* About
stsops is a simple alternative to tools like Puppet, Ansible, and Chef.  It is
inspired by [[http://fuckingshellscripts.org/][Fucking Shell Scripts]] but offers a more robust system.

stsops is meant to have simple semantics.  However, to achieve this it puts a
significant amount of the complexity on authors of actions.

stsops is not meant to be a complete system but to rather be an important piece
of a larger system.
* Why does this exist?
If Puppet, Ansible, Chef, and fss still exist, why create stsops at all?  Part
of it is an experiment.  But another part is that those tools are complicated
and require learning new languages and it can be challenging to get them to work
in a complicated system.
* Terminology
** Host
Something that can be logged into and scripts executed on.
** Action
A script that is executed on a host.  An action can be in any language and any
format however it must be executable.
** Assets
These are files that are uploaded to a host.  Assets can also be templates which
are applied on the machine which running stsops and uploaded like any other
asset.
** Assemblage
The collection of actions, assets, and other configuration to apply to a
collection of hosts.
** Templates
A template is an asset and variables which are applied to the asset and uploaded.
** Variable
A value that can differ per execution of an assemblage.  These are stored on the
file system where the filename is the variable name and the content of the file
is the value of the variable.
** Failure strategy
How the system should handle a failure when apply an assemblage to a host.
** Host Generator
A program which generates a list of hosts.  Each host is on a new line.  Empty
lines are filtered out.
** Config
A configuration for the stsops client.
* Directory layout
stsops assumes the directory it runs in is laid out in a particular way.  At the
top level are directories named: ~assemblages~, ~actions~, ~assets~, and
~configs~.

An example directory:

#+BEGIN_EXAMPLE
.
 /actions
         /config-nginx
         /install-nginx
         /restart-nginx
 /assemblages
             /frontend-server.toml
 /assets
        /nginx.conf
 /config
        /default.toml
#+END_EXAMPLE
* Assemblages
An assemblage combines hosts, actions, and assets.

An example assemblage:

#+BEGIN_SRC toml
# Hosts are not just a list of hosts but have a generator, which is a program
# that outputs a list of hostnames.
[hosts]
generator = ["cat", "/foo/nginx.hosts"]

# These variables will be in the nginx namespace.
# Note, variables are stringly typed.
[variables.nginx]
server-name = "foo.com"
port = "80"

# Upload these assets
[assets]
upload = ["nginx.conf"]

# These actions are run in order
[actions]
run = ["install-nginx",
       "config-nginx",
       "restart-nginx"]
#+END_SRC

An example assemblage using a template:

#+BEGIN_SRC toml
[hosts]
generator = ["cat", "/foo/nginx.hosts"]

[assets.templates.nginx]
# The asset to use as a template.
asset = "nginx.conf.tmpl"
# By default the generated asset has the same name as
# the input asset.  That can be overriden with output_name.
output_name = "nginx.conf"

# All temlates require a .kv section.
# In this case the "server" key is a list of servers.
[[assets.template.nginx.kv.server]]
name = "foo.com"
port = 80

[[assets.template.nginx.kv.server]]
name = "bar.com"
port = 81

# These actions are run in order
[actions]
run = ["install-nginx",
       "config-nginx",
       "restart-nginx"]
#+END_SRC
* Actions
Actions are programs which are executed on a host.  They are passed one
parameter on the command line which is the base directory for the variables.
* Variables
Variables are a mapping of string to string.  They are passed between scripts
using the file system.  All variables can be read and written by any action.  It
is suggested that actions put their variables in a sub-directory that is
meaningfully named.
** Pre-defined variables
*** stsops.hostname
The name stsops believes this host is named.
*** stsops.hosts
List of hosts that stsops is executing over.  This is the same output, including
order, as the hosts generator.
*** stsops.assets
The path on the system for where the assets that have been uploaded.
* Config
A config describes configuration for the stsops client.

An example config:

#+BEGIN_SRC
[connection.ssh]
command = "ssh"
options = ["-oUserKnownHostsFile=/dev/null", "-oStrictHostKeyChecking=no"]

[connection.scp]
command = "scp"
options = ["-oUserKnownHostsFile=/dev/null", "-oStrictHostKeyChecking=no"]

[strategy.failure]
name = "all-for-one"
#+END_SRC
