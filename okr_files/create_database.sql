PRAGMA foreign_keys = ON;

CREATE TABLE orgs(
       id INTEGER PRIMARY KEY AUTOINCREMENT,
       name TEXT NOT NULL);

CREATE TABLE users(
       email TEXT PRIMARY KEY,
       name TEXT NOT NULL,
       passwd TEXT NOT NULL);

CREATE TABLE sessions(
       id TEXT PRIMARY KEY,
       user_id TEXT,
       FOREIGN KEY(user_id) REFERENCES users(email));

CREATE TABLE user_orgs(
       user_id TEXT,
       org_id INTEGER,
       FOREIGN KEY(user_id) REFERENCES users(email),
       FOREIGN KEY(org_id) REFERENCES orgs(id),
       PRIMARY KEY (user_id, org_id));

CREATE TABLE objectives(
       id INTEGER PRIMARY KEY AUTOINCREMENT,
       org_id INTEGER,
       desc TEXT NOT NULL,
       time_type TEXT NOT NULL,
       end_date TEXT NOT NULL,
       FOREIGN KEY(org_id) REFERENCES orgs(id));

CREATE TABLE key_results(
       id INTEGER PRIMARY KEY AUTOINCREMENT,
       objective_id INTEGER NOT NULL,
       desc TEXT NOT NULL,
       score REAL,
       FOREIGN KEY(objective_id) REFERENCES objectives(id));

INSERT INTO orgs(name) VALUES('Test Org');
INSERT INTO orgs(name) VALUES('Test User');
INSERT INTO users VALUES('test@test.com', 'Test User', '');
INSERT INTO user_orgs values('test@test.com', 1);
INSERT INTO user_orgs values('test@test.com', 2);

INSERT INTO objectives(org_id, desc, time_type, end_date)
       VALUES(1, 'Build OKR web app', 'quarterly', date('now','+3 month'));

INSERT INTO objectives(org_id, desc, time_type, end_date)
       VALUES(1, 'Do other thing', 'quarterly', date('now','+3 month'));

INSERT INTO objectives(org_id, desc, time_type, end_date)
       VALUES(1, 'Do yet another thing', 'quarterly', date('now','+3 month'));

INSERT INTO key_results(objective_id, desc) VALUES(1, 'Make web app');
INSERT INTO key_results(objective_id, desc) VALUES(1, 'Make other thing');
INSERT INTO key_results(objective_id, desc) VALUES(1, 'Profit');

INSERT INTO key_results(objective_id, desc, score) VALUES(2, 'Make web app', 0.5);
INSERT INTO key_results(objective_id, desc, score) VALUES(2, 'Make other thing', 0.9);
INSERT INTO key_results(objective_id, desc, score) VALUES(2, 'Profit', 0.7);

INSERT INTO key_results(objective_id, desc, score) VALUES(3, 'Make web app', 0.5);
INSERT INTO key_results(objective_id, desc, score) VALUES(3, 'Make other thing', 0.4);
INSERT INTO key_results(objective_id, desc, score) VALUES(3, 'Profit', 0.2);
