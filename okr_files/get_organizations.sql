SELECT orgs.id, orgs.name FROM orgs
       INNER JOIN user_orgs ON orgs.id = user_orgs.org_id
       WHERE user_id = ?
       ORDER BY orgs.id DESC;
