SELECT objectives.id, objectives.desc, objectives.time_type FROM objectives
       INNER JOIN orgs ON orgs.id = objectives.org_id
       INNER JOIN user_orgs ON orgs.id = user_orgs.org_id
       WHERE user_orgs.user_id = ? AND end_date > date('now')
       ORDER BY objectives.id;
