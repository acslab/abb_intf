DELETE FROM key_results WHERE
       key_results.objective_id = ? AND key_results.id = ?
       AND EXISTS
       (SELECT * FROM objectives
        INNER JOIN orgs ON orgs.id = objectives.org_id
        INNER JOIN user_orgs ON orgs.id = user_orgs.org_id
        WHERE objectives.id = key_results.objective_id AND user_orgs.user_id = ?);
