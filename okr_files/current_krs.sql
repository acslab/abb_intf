SELECT key_results.id, objectives.id, key_results.desc, key_results.score FROM key_results
       INNER JOIN objectives ON objectives.id = key_results.objective_id
       INNER JOIN orgs ON orgs.id = objectives.org_id
       INNER JOIN user_orgs ON orgs.id = user_orgs.org_id
       WHERE user_orgs.user_id = ? AND objectives.end_date > date('now')
       ORDER BY key_results.id;
