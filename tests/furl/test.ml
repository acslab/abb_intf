let string = Tyre.regex Re.(rep1 @@ compl [char '/'])

let hello_name () =
  Furl.(rel / "hello" /% string)

let goodbye_name () =
  Furl.(rel / "goodbye" /% string)

let handle_hello_name = Printf.sprintf "Hello %s"

let handle_goodbye_name = Printf.sprintf "Goodbye %s"

let router =
  Furl.(match_url
          ~default:(fun _ -> failwith "This is not a valid path.")
          [ hello_name () --> handle_hello_name
          ; goodbye_name () --> handle_goodbye_name
          ])

let route_hello =
  Oth.test
    ~desc:"Route to the hello path"
    ~name:"Route hello"
    (fun _ ->
       let uri = Uri.of_string "http://test.com/hello/there" in
       let resp = router uri in
       assert (resp = "Hello there"))

let route_goodbye =
  Oth.test
    ~desc:"Route to the goodbye path"
    ~name:"Route goodbye"
    (fun _ ->
       let uri = Uri.of_string "http://test.com/goodbye/you" in
       let resp = router uri in
       assert (resp = "Goodbye you"))

let test =
  Oth.parallel
    [ route_hello
    ; route_goodbye
    ]

let () =
  Random.self_init ();
  Oth.run test
