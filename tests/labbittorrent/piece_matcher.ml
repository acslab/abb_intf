module List = ListLabels

module Piece_matcher = Labbittorrent_piece_matcher

let float_bound f s = Random.State.float s f

let non_empty_list length_gen =
  QCheck.Gen.(
    (* Make sure the list is empty and the first file is at least 1 byte *)
    let f frst l = (frst + 1)::l in
    return f <*> length_gen <*> list length_gen)

let file_offset_gen length_gen =
  QCheck.Gen.(
    let rec f id len = function
      | [] -> [(Piece_matcher.Offset.of_int len, id)]
      | x::xs -> (Piece_matcher.Offset.of_int len, id)::f (id + 1) (len + x) xs
    in
    return (f 0 0) <*> non_empty_list length_gen)

let big_filesize_gen = QCheck.Gen.(return (fun n -> Int32.to_int (Int32.abs n)) <*> ui32)

let offset_pp o =
  string_of_int (Piece_matcher.Offset.to_int o)

let offset_between_files f_offset1 f_offset2 offset =
  Piece_matcher.Offset.(to_int f_offset1 <= offset && offset < to_int f_offset2)

let rec simple_matcher offset len = function
  | [] ->
    []
  | [(f_offset, id)] when Piece_matcher.Offset.to_int f_offset <= offset ->
    let open Piece_matcher in
    [(id, Offset.of_int (offset - Offset.to_int f_offset), Length.of_int len)]
  | (f_offset1, id1)::(f_offset2, id2)::xs when offset_between_files f_offset1 f_offset2 offset ->
    let open Piece_matcher in
    let f_offset1' = Offset.to_int f_offset1 in
    let f_offset2' = Offset.to_int f_offset2 in
    if offset + len >= f_offset2' then
      let mtch = (id1, Offset.of_int (offset - f_offset1'), Length.of_int (f_offset2' - offset)) in
      mtch::simple_matcher f_offset2' (len - (f_offset2' - offset)) ((f_offset2, id2)::xs)
    else
      let mtch = (id1, Offset.of_int (offset - f_offset1'), Length.of_int len) in
      [mtch]
  | _::xs ->
    simple_matcher offset len xs

let simple_matcher_test1 =
  Oth.test
    ~name:"Simple Matcher Test #1"
    (fun _ ->
       let file_offsets = [(0, 0); (10, 1)] in
       let file_offsets' =
         List.map
           ~f:(fun (o, id) -> (Piece_matcher.Offset.of_int o, id))
           file_offsets
       in
       let offset = 0 in
       let len = 9 in
       match simple_matcher offset len file_offsets' with
         | [ (0, offset', len') ] ->
           assert (Piece_matcher.Offset.to_int offset' = offset);
           assert (Piece_matcher.Length.to_int len' = len)
         | _ ->
           assert false)

let simple_matcher_test2 =
  Oth.test
    ~name:"Simple Matcher Test #2"
    (fun _ ->
       let file_offsets = [(0, 0); (10, 1)] in
       let file_offsets' =
         List.map
           ~f:(fun (o, id) -> (Piece_matcher.Offset.of_int o, id))
           file_offsets
       in
       let offset = 0 in
       let len = 15 in
       match simple_matcher offset len file_offsets' with
         | [ (0, offset1, len1); (1, offset2, len2) ] ->
           assert (Piece_matcher.Offset.to_int offset1 = offset);
           assert (Piece_matcher.Length.to_int len1 = 10);
           assert (Piece_matcher.Offset.to_int offset2 = 0);
           assert (Piece_matcher.Length.to_int len2 = 5)
         | _ ->
           assert false)

let simple_matcher_test3 =
  Oth.test
    ~name:"Piece Matcher Test #3"
    (fun _ ->
       let file_offsets = [(0, 0); (274589440, 1); (1883804787, 2); (2896241042, 3)] in
       let file_offsets' =
         List.map
           ~f:(fun (o, id) -> (Piece_matcher.Offset.of_int o, id))
           file_offsets
       in
       let offset = 1050402102 in
       let len = 2133332188 in
       match simple_matcher offset len file_offsets' with
         | [ (1, offset1, len1); (2, offset2, len2); (3, offset3, len3) ] ->
           assert (Piece_matcher.Offset.to_int offset1 = (offset - 274589440));
           assert (Piece_matcher.Length.to_int len1 = (1883804787 - offset));
           assert (Piece_matcher.Offset.to_int offset2 = 0);
           assert (Piece_matcher.Length.to_int len2 = (2896241042 - 1883804787));
           assert (Piece_matcher.Offset.to_int offset3 = 0);
           assert (Piece_matcher.Length.to_int len3 = (offset + len - 2896241042))
         | _ ->
           assert false)

let simple_matcher_test4 =
  Oth.test
    ~name:"Simple Matcher Test #4"
    (fun _ ->
       let file_offsets = [(0, 0); (10, 1)] in
       let file_offsets' =
         List.map
           ~f:(fun (o, id) -> (Piece_matcher.Offset.of_int o, id))
           file_offsets
       in
       let offset = 2 in
       let len = 7 in
       match simple_matcher offset len file_offsets' with
         | [ (0, offset', len') ] ->
           assert (Piece_matcher.Offset.to_int offset' = offset);
           assert (Piece_matcher.Length.to_int len' = len)
         | _ ->
           assert false)

let piece_matcher_test1 =
  Oth.test
    ~name:"Piece Matcher Test #1"
    (fun _ ->
       let file_offsets = [(0, 0); (10, 1)] in
       let file_offsets' =
         Piece_matcher.create
           (List.map
              ~f:(fun (o, id) -> (Piece_matcher.Offset.of_int o, id))
              file_offsets)
       in
       let offset = 0 in
       let len = 9 in
       match Piece_matcher.(mtch (Offset.of_int offset, Length.of_int len) file_offsets') with
         | [ (0, offset', len') ] ->
           assert (Piece_matcher.Offset.to_int offset' = offset);
           assert (Piece_matcher.Length.to_int len' = len)
         | _ ->
           assert false)

let piece_matcher_test2 =
  Oth.test
    ~name:"Piece Matcher Test #2"
    (fun _ ->
       let file_offsets = [(0, 0); (10, 1)] in
       let file_offsets' =
         Piece_matcher.create
           (List.map
              ~f:(fun (o, id) -> (Piece_matcher.Offset.of_int o, id))
              file_offsets)
       in
       let offset = 0 in
       let len = 15 in
       match Piece_matcher.(mtch (Offset.of_int offset, Length.of_int len) file_offsets') with
         | [ (0, offset1, len1); (1, offset2, len2) ] ->
           assert (Piece_matcher.Offset.to_int offset1 = offset);
           assert (Piece_matcher.Length.to_int len1 = 10);
           assert (Piece_matcher.Offset.to_int offset2 = 0);
           assert (Piece_matcher.Length.to_int len2 = 5)
         | _ ->
           assert false)

let piece_matcher_test3 =
  Oth.test
    ~name:"Piece Matcher Test #3"
    (fun _ ->
       let file_offsets = [(0, 0); (274589440, 1); (1883804787, 2); (2896241042, 3)] in
       let file_offsets' =
         Piece_matcher.create
           (List.map
              ~f:(fun (o, id) -> (Piece_matcher.Offset.of_int o, id))
              file_offsets)
       in
       let offset = 1050402102 in
       let len = 2133332188 in
       match Piece_matcher.(mtch (Offset.of_int offset, Length.of_int len) file_offsets') with
         | [ (1, offset1, len1); (2, offset2, len2); (3, offset3, len3) ] ->
           assert (Piece_matcher.Offset.to_int offset1 = (offset - 274589440));
           assert (Piece_matcher.Length.to_int len1 = (1883804787 - offset));
           assert (Piece_matcher.Offset.to_int offset2 = 0);
           assert (Piece_matcher.Length.to_int len2 = (2896241042 - 1883804787));
           assert (Piece_matcher.Offset.to_int offset3 = 0);
           assert (Piece_matcher.Length.to_int len3 = (offset + len - 2896241042))
         | _ ->
           assert false)

let piece_matcher_prop =
  QCheck.Test.make
    ~count:1000
    ~max_gen:2000
    ~name:"piece_matcher_prop"
    (QCheck.make
       ~print:QCheck.Print.(pair (list (pair offset_pp int)) (pair float float))
       QCheck.Gen.(
         pair
           (file_offset_gen big_filesize_gen)
           (pair (float_bound 1.0) (float_bound 1.0))))
    (fun (file_offsets, (split_point, len_point)) ->
       let matcher = Piece_matcher.create file_offsets in
       let length =
         List.fold_left
           ~f:(fun acc (offset, _) -> acc + Piece_matcher.Offset.to_int offset)
           ~init:0
           file_offsets
       in
       let offset = int_of_float (float length *. split_point) in
       let len = int_of_float (float (length - offset) *. len_point) + 1 in
       let res = Piece_matcher.(mtch (Offset.of_int offset, Length.of_int len) matcher) in
       res = simple_matcher offset len file_offsets)

let piece_matcher_prop_test =
  Oth.test
    ~name:"Piece Match er Prop Test"
    (fun _ ->
       QCheck.Test.check_exn
         ~rand:(Random.State.make_self_init ())
         piece_matcher_prop)

let () =
  Random.self_init ();
  Oth.(
    run
      (parallel [ piece_matcher_prop_test
                ; simple_matcher_test1
                ; simple_matcher_test2
                ; simple_matcher_test3
                ; simple_matcher_test4
                ; piece_matcher_test1
                ; piece_matcher_test2
                ; piece_matcher_test3
                ]))

