module List = ListLabels
module String_map = Map.Make(String)

module Bencode = Labbittorrent_bencode

let string_test =
  Oth.test
    ~desc:"Test string decoding"
    ~name:"String test"
    (fun _ ->
      let b = Bytes.of_string "3:bye" in
      let t =
        Bencode.of_bytes b
        |> snd
        |> Bencode.to_view
      in
      assert ((Bencode.View.String "bye") = t);
      assert (Bytes.equal (Bencode.to_bytes (Bencode.of_view t)) b))

let list_test =
  Oth.test
    ~desc:"Test list decoding"
    ~name:"List test"
    (fun _ ->
      let b = Bytes.of_string "l3:byee" in
      let t =
        Bencode.of_bytes b
        |> snd
        |> Bencode.to_view
      in
      assert ((Bencode.View.List [Bencode.View.String "bye"]) = t);
      assert (Bytes.equal (Bencode.to_bytes (Bencode.of_view t)) b))

let int_test =
  Oth.test
    ~desc:"Test int decoding"
    ~name:"Int test"
    (fun _ ->
      let b = Bytes.of_string "i123e" in
      let t =
        Bencode.of_bytes b
        |> snd
        |> Bencode.to_view
      in
      (match t with
        | Bencode.View.Int n ->
          assert Num.(n =/ num_of_int 123)
        | _ ->
          assert false);
      assert (Bytes.equal (Bencode.to_bytes (Bencode.of_view t)) b))

let dict_test =
  Oth.test
    ~desc:"Test dictionary decoding"
    ~name:"Dict test"
    (fun _ ->
      let b = Bytes.of_string "d3:bye2:hi2:hi3:byee" in
      let t =
        Bencode.of_bytes b
        |> snd
        |> Bencode.to_view
      in
      (match t with
        | Bencode.View.Dict d -> begin
          assert (String_map.find "hi" d = Bencode.View.String "bye");
          assert (String_map.find "bye" d = Bencode.View.String "hi")
        end
        | _ ->
          assert false);
      assert (Bytes.equal (Bencode.to_bytes (Bencode.of_view t)) b))

let dict_key_order_test =
  Oth.test
    ~desc:"Test dictionary encoding, keys must be sorted"
    ~name:"Dict encoding test"
    (fun _ ->
      let d =
        String_map.empty
        |> String_map.add "hi" (Bencode.View.String "bye")
        |> String_map.add "bye" (Bencode.View.String "hi")
        |> fun d -> Bencode.View.Dict d
        |> Bencode.of_view
      in
      let b = Bytes.of_string "d3:bye2:hi2:hi3:byee" in
      let encoded = Bencode.to_bytes d in
      assert (Bytes.equal b encoded))

let bencode_int = QCheck.Gen.(map (fun i -> Bencode.View.(Int (Num.num_of_int i))) int)

let bencode_str = QCheck.Gen.(map (fun s -> Bencode.View.(String s)) string)

let make_list l = Bencode.View.(List l)

let make_dict pairs =
  let m =
    List.fold_left
      ~f:(fun acc (k, v) -> String_map.add k v acc)
      ~init:String_map.empty
      pairs
  in
  Bencode.View.(Dict m)

let bencode_gen =
  QCheck.Gen.(
    sized @@ fix
      (fun self n ->
        (* Limit the depth to 4 *)
        match min n 4  with
          | 0 ->
            oneof [bencode_int; bencode_str]
          | n ->
            frequency
              [ (1, oneof [bencode_int; bencode_str])
              ; (2, map make_list (list_size (int_range 0 10) (self (n - 1))))
              ; (3, map make_dict (list_size (int_range 0 10) (pair string (self (n -1)))))
              ]))

let bencode_prop =
  QCheck.Test.make
    ~count:100
    ~max_gen:500
    ~name:"bencode_prop"
    (QCheck.make bencode_gen)
    (fun view ->
      let t1 = Bencode.of_view view in
      let b1 = Bencode.to_bytes t1 in
      let t2 = snd (Bencode.of_bytes b1) in
      let b2 = Bencode.to_bytes t2 in
      b1 = b2)

let bencode_prop_test =
  Oth.test
    ~name:"Bencode Prop Test"
    (fun _ ->
      QCheck.Test.check_exn
        ~rand:(Random.State.make_self_init ())
        bencode_prop)

let () =
  Random.self_init ();
  Oth.(
    run
      (parallel [ string_test
                ; list_test
                ; int_test
                ; dict_test
                ; dict_key_order_test
                ; bencode_prop_test
                ]))
