module List = ListLabels

module Peer_protocol = Labbittorrent_peer_protocol

let sint32 =
  QCheck.Gen.(
    let f n = function
      | true -> Int32.to_int n
      | false -> -1 * Int32.to_int n
    in
    return f <*> ui32 <*> bool)

let request_gen =
  QCheck.Gen.(
    let f index start length = Peer_protocol.Frame.Request.({ index; start; length }) in
    return f <*> sint32 <*> sint32 <*> sint32)

let float_bound f s = Random.State.float s f

let piece_gen =
  QCheck.Gen.(
    let f r p =
      let p = Bytes.of_string p in
      let module Req = Peer_protocol.Frame.Request in
      let r = { r with Req.length = Bytes.length p } in
      (r, p)
    in
    return f <*> request_gen <*> string)

let frame_gen =
  QCheck.Gen.(
    frequency
      [ (1, return `Keepalive)
      ; (1, return `Choke)
      ; (1, return `Unchoke)
      ; (1, return `Interested)
      ; (1, return `Uninterested)
      ; (1, map (fun n -> `Have n) sint32)
      ; (2, map (fun bf -> `Bitfield (Bytes.of_string bf)) string)
      ; (1, return `Have_all)
      ; (1, return `Have_none)
      ; (2, map (fun r -> `Request r) request_gen)
      ; (2, map (fun p -> `Piece p) piece_gen)
      ; (2, map (fun r  -> `Cancel r) request_gen)
      ; (2, map (fun n -> `Suggest_piece n) sint32)
      ; (2, map (fun r -> `Reject_request r) request_gen)
      ; (2, map (fun n -> `Allowed_fast n) sint32)
      ])

let rec print_frame : Peer_protocol.Frame.t -> string = function
  | `Keepalive -> "Keepalive"
  | `Choke -> "Choke"
  | `Unchoke -> "Unchoke"
  | `Interested -> "Interested"
  | `Uninterested -> "Uninterested"
  | `Have n -> Printf.sprintf "Have %d" n
  | `Bitfield bf -> Printf.sprintf "Bitfield %s" (String.escaped (Bytes.to_string bf))
  | `Have_all -> "Have_all"
  | `Have_none -> "Have_none"
  | `Request r ->
    let open Peer_protocol.Frame.Request in
    Printf.sprintf "Request index=%d start=%d length=%d" r.index r.start r.length
  | `Piece (r, p) ->
    let open Peer_protocol.Frame.Request in
    Printf.sprintf
      "Piece index=%d start=%d length=%d piece=%s"
      r.index
      r.start
      r.length
      (String.escaped (Bytes.to_string p))
  | `Cancel r ->
    let open Peer_protocol.Frame.Request in
    Printf.sprintf "Cancel index=%d start=%d length=%d" r.index r.start r.length
  | `Suggest_piece n -> Printf.sprintf "Suggest_piece %d" n
  | `Reject_request r ->
    let open Peer_protocol.Frame.Request in
    Printf.sprintf "Reject_request index=%d start=%d length=%d" r.index r.start r.length
  | `Allowed_fast n -> Printf.sprintf "Allowed_fast %d" n

let encode_decode_prop =
  QCheck.Test.make
    ~count:1000
    ~max_gen:2000
    ~name:"encode_decode_prop"
    (QCheck.make ~print:print_frame frame_gen)
    (fun frame ->
       let buf = Buffer.create 0x0d in
       Peer_protocol.Writer.encode buf frame;
       let bytes = Buffer.to_bytes buf in
       let decoded =
         Peer_protocol.Reader.decode_connected
           ~pos:0
           ~len:(Bytes.length bytes)
           bytes
       in
       match decoded with
         | Ok ([ frame' ], _) when frame' = frame ->
           true
         | _ ->
           false)

let encode_decode_multi_frames_prop =
  QCheck.Test.make
    ~count:1000
    ~max_gen:2000
    ~name:"encode_decode_multi_frames_prop"
    (QCheck.make ~print:(QCheck.Print.list print_frame) (QCheck.Gen.list frame_gen))
    (fun frames ->
       let buf = Buffer.create 0x0d in
       List.iter
         ~f:(fun frame -> Peer_protocol.Writer.encode buf frame)
         frames;
       let bytes = Buffer.to_bytes buf in
       let decoded =
         Peer_protocol.Reader.decode_connected
           ~pos:0
           ~len:(Bytes.length bytes)
           bytes
       in
       match decoded with
         | Ok (fs, _) ->
           [] = List.filter ~f:(not) (List.map2 ~f:(=) frames fs)
         | _ ->
           false)

let encode_decode_multi_frames_split_prop =
  QCheck.Test.make
    ~count:1000
    ~max_gen:2000
    ~name:"encode_decode_multi_frames_split_prop"
    (QCheck.make
       ~print:QCheck.Print.(pair (list print_frame) float)
       QCheck.Gen.(pair (list frame_gen) (float_bound 1.0)))
    (fun (frames, split) ->
       let buf = Buffer.create 0x0d in
       List.iter
         ~f:(fun frame -> Peer_protocol.Writer.encode buf frame)
         frames;
       let bytes = Buffer.to_bytes buf in
       let split_pos = int_of_float (split *. float (Bytes.length bytes)) in
       let decoded1 =
         Peer_protocol.Reader.decode_connected
           ~pos:0
           ~len:split_pos
           bytes
       in
       match decoded1 with
         | Ok (fs1, pos) ->
           let decoded2 =
             Peer_protocol.Reader.decode_connected
               ~pos:pos
               ~len:(Bytes.length bytes - pos)
               bytes
           in
           begin match decoded2 with
             | Ok (fs2, _) ->
               [] = List.filter ~f:(not) (List.map2 ~f:(=) frames (fs1 @ fs2))
             | _ ->
               assert false
           end
         | _ ->
           false)

let encode_decode_prop_test =
  Oth.test
    ~name:"Encode Decode Prop Test"
    (fun _ ->
       QCheck.Test.check_exn
         ~rand:(Random.State.make_self_init ())
         encode_decode_prop)

let encode_decode_multi_frames_prop_test =
  Oth.test
    ~name:"Encode Decode Multiple Frames Prop Test"
    (fun _ ->
       QCheck.Test.check_exn
         ~rand:(Random.State.make_self_init ())
         encode_decode_multi_frames_prop)

let encode_decode_multi_frames_split_prop_test =
  Oth.test
    ~name:"Encode Decode Multiple Frames With Split Prop Test"
    (fun _ ->
       QCheck.Test.check_exn
         ~rand:(Random.State.make_self_init ())
         encode_decode_multi_frames_split_prop)

let () =
  Random.self_init ();
  Oth.(
    run
      (parallel [ encode_decode_prop_test
                ; encode_decode_multi_frames_prop_test
                ; encode_decode_multi_frames_split_prop_test
                ]))
