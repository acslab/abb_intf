module Abb = Abb_scheduler_select
module Fut_comb = Abb_future_combinators.Make(Abb.Future)
module Http = Cohttp_abb.Make(Abb)

let tls_config =
  let cfg = Otls.Tls_config.create () in
  Otls.Tls_config.insecure_noverifycert cfg;
  cfg

let run_client () =
  let open Fut_comb.Infix_result_monad in
  let url = Sys.argv.(1) in
  let uri = Uri.of_string url in
  Http.Client.call ~tls_config `GET uri
  >>| fun (resp, body) ->
  Printf.printf "Status: %s\n" (Cohttp.Code.string_of_status resp.Http.Response.status);
  print_endline body


let main () =
  match Abb.Scheduler.run (Abb.Scheduler.create ()) run_client with
    | `Det (Ok _) ->
      ()
    | `Det (Error err) ->
      Printf.eprintf "Error: %s" (Cohttp_abb.show_request_err err);
      failwith "Error"
    | `Aborted ->
      failwith "Aborted"
    | `Exn (exn, bt_opt) ->
      Printf.eprintf "Exn = %s\n" (Printexc.to_string exn);
      CCOpt.iter
        (fun bt -> Printf.eprintf "%s\n" (Printexc.raw_backtrace_to_string bt))
        bt_opt;
      raise exn

let () = main ()
