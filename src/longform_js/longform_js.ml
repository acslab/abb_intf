module Search_responce = Longform_data.Search_responce

let search_responce_of_string s = Search_responce.of_yojson (Yojson.Safe.from_string s)

let alert msg = Js.Unsafe.(fun_call (js_expr "alert") [| inject (Js.string msg) |])

let set_title req title_elm =
  title_elm##.value := req##.responseText;
  Js._true

let fetch_title () =
  let url = Js.Opt.to_option (Dom_html.(CoerceTo.input (getElementById "article_url"))) in
  let title = Js.Opt.to_option (Dom_html.(CoerceTo.input (getElementById "article_title"))) in
  match (url, title) with
    | (Some url_elm, Some title_elm) ->
      let url = url_elm##.value in
      let req = XmlHttpRequest.create () in
      req##.onload := Dom.handler (fun _ -> set_title req title_elm);
      req##_open (Js.string "POST") (Js.string "/v0/fetch-title") Js._true;
      req##send (Js.some url)
    | _ ->
      assert false

let exec_search query f =
  let req = XmlHttpRequest.create () in
  req##.onload := Dom.handler (fun _ -> f req);
  req##_open (Js.string "POST") (Js.string "/v0/search") Js._true;
  let data =
    Js.(string
          (to_string (encodeURIComponent(string "query"))
           ^ "=" ^
           (to_string (encodeURIComponent(query)))))
  in
  req##send (Js.some data)

let populate_results results_id results_elm req =
  match search_responce_of_string (Js.to_string req##.responseText) with
    | Ok result ->
      let ul = Dom_html.createUl Dom_html.document in
      ul##.id := results_id;
      ListLabels.iter
        ~f:(fun article ->
            let li = Dom_html.createLi Dom_html.document in
            let href = Dom_html.createA Dom_html.document in
            href##.innerHTML := Js.string article.Search_responce.title;
            href##.href := Js.string (Printf.sprintf "add/%d" article.Search_responce.id);
            Dom.appendChild li href;
            Dom.appendChild ul li)
        result.Search_responce.data;
      (match Js.Opt.to_option results_elm##.firstChild with
        | Some first_child ->
          Dom.replaceChild results_elm ul first_child
        | None ->
          assert false);
      Js._true
    | Error _ ->
       assert false

let search query_id results_id =
  let query =
    query_id
    |> Js.to_string
    |> Dom_html.getElementById
    |> Dom_html.CoerceTo.input
    |> Js.Opt.to_option
  in
  let results =
    results_id
    |> Js.to_string
    |> Dom_html.getElementById
    |> Dom_html.CoerceTo.div
    |> Js.Opt.to_option
  in
  match (query, results) with
    | (Some query, Some results) ->
      exec_search
        query##.value
        (fun req -> populate_results results_id results req)
    | _ ->
      assert false

let add_tag () =
  let new_tag = Js.Opt.to_option (Dom_html.(CoerceTo.input (getElementById "new_tag"))) in
  let tag_list = Js.Opt.to_option (Dom_html.(CoerceTo.ul (getElementById "tag_list"))) in
  match (new_tag, tag_list) with
    | (Some new_tag, Some tag_list) ->
      let new_tag_value = Js.to_string new_tag##.value in
      let id = "tag_list_" ^ new_tag_value in
      let li = Dom_html.createLi Dom_html.document in
      li##.className := Js.string "tag_list";
      let s =
        Printf.sprintf
          "%s<input name=\"article_tags\" class=\"input_hidden\" value=\"%s\" /><button class=\"remove_button\" type=\"button\" onclick=\"removeTag('%s')\">&#x274C;</button>"
          new_tag_value
          new_tag_value
          new_tag_value
      in
      li##.id := Js.string id;
      li##.innerHTML := Js.string s;
      new_tag##.value := Js.string "";
      Dom.appendChild tag_list li
    | _ ->
      assert false

let remove_tag tag_value =
  let id = "tag_list_" ^ (Js.to_string tag_value) in
  let tag = Js.Opt.to_option (Dom_html.(CoerceTo.li (getElementById id))) in
  let tag_list = Js.Opt.to_option (Dom_html.(CoerceTo.ul (getElementById "tag_list"))) in
  match (tag, tag_list) with
    | (Some tag, Some tag_list) ->
      Dom.removeChild tag_list tag
    | _ ->
      assert false

(* Code to run when the submit page has been loaded *)
let submit_onload () =
  let keydown event =
    match event##.keyCode with
      | 13 ->
        let add_tag = Js.Opt.to_option (Dom_html.(CoerceTo.button (getElementById "add_tag"))) in
        (match add_tag with
          | Some add_tag ->
            add_tag##click;
            Js._false
          | None ->
            assert false)
      | _ ->
        Js._true
  in
  let tag = Js.Opt.to_option (Dom_html.(CoerceTo.input (getElementById "new_tag"))) in
  match tag with
    | Some tag ->
      tag##.onkeydown := Dom_html.handler keydown;
      Js._true
    | None ->
      assert false

let add_related_onload () =
  let keydown event =
    match event##.keyCode with
      | 13 ->
        let search = Js.Opt.to_option (Dom_html.(CoerceTo.button (getElementById "search"))) in
        (match search with
          | Some search ->
            search##click;
            Js._false
          | None ->
            assert false)
      | _ ->
        Js._true
  in
  let query = Js.Opt.to_option (Dom_html.(CoerceTo.input (getElementById "query"))) in
  match query with
    | Some query ->
      query##.onkeydown := Dom_html.handler keydown;
      Js._true
    | None ->
      assert false

let _ =
  Js.export_all
    (object%js
      method fetchTitle = fetch_title ()
      method addTag = add_tag ()
      method removeTag (id : Js.js_string Js.t) = remove_tag id
      method submitOnload = submit_onload ()
      method addRelatedOnload = add_related_onload ()
      method search (query_id : Js.js_string Js.t) (results_id : Js.js_string Js.t) =
        search query_id results_id
    end)
