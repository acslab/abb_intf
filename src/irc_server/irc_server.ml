module Cmdline = struct
  module C = Cmdliner

  let port =
    let doc = "Port to run server on." in
    C.Arg.(value & opt int 6667 & info ["p"; "port"] ~doc)

  let run_cmd f =
    let doc = "Run IRC server." in
    let exits = C.Term.default_exits in
    C.Term.(const f $ port, info "run" ~doc ~exits)

  let default_cmd =
    let doc = "A simple IRC server backed by keybase chat." in
    let exits = C.Term.default_exits in
    C.Term.(ret (const (fun _ -> (`Help (`Pager, None))) $ port),
            info "server" ~doc ~exits)
end

let reporter ppf =
  let report src level ~over k msgf =
    let k _ = over (); k () in
    let with_stamp h tags k ppf fmt =
      (* TODO: Make this use the proper Abb time *)
      let time = Unix.gettimeofday () in
      let time_str = ISO8601.Permissive.string_of_datetime time in
      Format.kfprintf
        k
        ppf
        ("[%s]%a @[" ^^ fmt ^^ "@]@.")
        time_str
        Logs.pp_header
        (level, h)
    in
    msgf @@ fun ?header ?tags fmt -> with_stamp header tags k ppf fmt
  in
  { Logs.report = report }

module Make (Abb : Abb_intf.S) = struct
  module Channel = Abb_channel.Make(Abb.Future)
  module Fut_comb = Abb_future_combinators.Make(Abb.Future)
  module Irc_srv = Irc_server_server.Make(Abb)
  module Tcp_server = Abb_tcp_server.Make(Abb)

  type handler = (Abb.Socket.tcp Abb.Socket.t -> unit Abb.Future.t)

  module Config = struct
    module View = struct
      type t = { on_handler_exn : [ `Ignore | `Error ]
               ; port : int
               ; handler : handler
               }
    end

    type t = View.t

    type err = [ `Invalid_port ]

    let of_view = function
      | { View.port; _ } when port <= 0 -> Error `Invalid_port
      | t -> Ok t

    let on_handler_exn t = t.View.on_handler_exn
    let port t = t.View.port
    let handler t = t.View.handler
  end

  let run_irc_server conn =
    let run_server () =
      let open Fut_comb.Infix_result_monad in
      Irc_srv.run conn
      >>= fun (chan_r, chan_w) ->
      failwith "nyi"
    in
    Fut_comb.ignore (run_server ())

  let rec run_handler config conn =
    let open Abb.Future.Infix_monad in
    Abb.Future.await
      (Fut_comb.with_finally
         (fun () -> Fut_comb.ignore (Config.handler config conn))
         ~finally:(fun () ->
             Logs.info (fun m -> m "Connection closed");
             Fut_comb.ignore (Abb.Socket.close conn)))
    >>| function
    | `Det () ->
      Logs.info (fun m -> m "Client disconnected")
    | `Exn (exn, bt_opt) ->
      Logs.err (fun m -> m "Exception: %s" (Printexc.to_string exn));
      CCOpt.iter
        (fun bt -> Logs.err (fun m -> m "Backtrace: %s" (Printexc.raw_backtrace_to_string bt)))
        bt_opt
    | `Aborted ->
      ()

  let rec accept_loop t config =
    let open Abb.Future.Infix_monad in
    Channel.recv t
    >>= function
    | `Ok conn ->
      Abb.Future.fork (run_handler config conn)
      >>= fun () ->
      accept_loop t config
    | `Closed ->
      assert false

  let irc_server config =
    let open Fut_comb.Infix_result_monad in
    let addr =
      Abb_intf.Socket.Sockaddr.(Inet { addr = Unix.inet_addr_loopback; port = Config.port config })
    in
    Tcp_server.run addr
    >>= fun t ->
    accept_loop t config

  let run_irc_server port =
    Logs.set_reporter (reporter Format.std_formatter);
    Logs.set_level ~all:true (Some Logs.Debug);
    Random.self_init ();
    match
      Config.of_view Config.View.({ port = port
                                  ; handler = run_irc_server
                                  ; on_handler_exn = `Ignore
                                  })
    with
      | Ok config ->
        Logs.info (fun m -> m "Started");
        ignore (Abb.Scheduler.run (Abb.Scheduler.create ()) (fun () -> irc_server config))
      | Error _ ->
        Logs.err (fun m -> m "Unable to make config")

    let cmds = Cmdline.([run_cmd run_irc_server])

  let main () = Cmdliner.Term.(exit @@ eval_choice Cmdline.default_cmd cmds)
end

module Irc = Make(Abb_scheduler_select)

let () = Irc.main ()
