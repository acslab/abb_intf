module Make (Abb: Abb_intf.S) : sig
  type 'a r = (Abb_channel.Make(Abb.Future).reader, 'a) Abb_channel.Make(Abb.Future).t
  type 'a w = (Abb_channel.Make(Abb.Future).writer, 'a) Abb_channel.Make(Abb.Future).t
  type err = [ `Error ]

  val run :
    Abb.Socket.tcp Abb.Socket.t ->
    ((Irc_server_msg.t r * Irc_server_msg.t w), [> err ]) result Abb.Future.t
end
