module Im = Irc_server_msg

let welcome server nick =
  [ Im.Raw Im.Raw.({ prefix = server
                   ; cmd = "001"
                   ; params = [nick]
                   ; trailing = Printf.sprintf "Welcome to the Internet Relay Chat Network %s" nick
                   })
  ; Im.Raw Im.Raw.({ prefix = server
                   ; cmd = "002"
                   ; params = [nick]
                   ; trailing = Printf.sprintf "Your host is %s" server
                   })
  ; Im.Raw Im.Raw.({ prefix = server
                   ; cmd = "003"
                   ; params = [nick]
                   ; trailing = "Server was created yesterday"
                   })
  ; Im.Raw Im.Raw.({ prefix = server
                   ; cmd = "004"
                   ; params = [nick]
                   ; trailing = Printf.sprintf "%s 1.0.0 DOQRSZaghilopswz CFILMPQSbcefgijklmnopqrstvz bkloveqjfI" server
                   })
  ]


module Make (Abb: Abb_intf.S) = struct
  module Buffered = Abb_io_buffered.Make(Abb.Future)
  module Buffered_of = Abb_io_buffered.Of(Abb)
  module Channel = Abb_channel.Make(Abb.Future)
  module Channel_queue = Abb_channel_queue.Make(Abb.Future)
  module Fut_comb = Abb_future_combinators.Make(Abb.Future)

  type 'a r = (Channel.reader, 'a) Channel.t
  type 'a w = (Channel.writer, 'a) Channel.t

  type err = [ `Error ]

  module State = struct
    type 'a t = { server : string
                ; sock_r : Buffered.reader Buffered.t
                ; sock_w : Buffered.writer Buffered.t
                ; chan_r : (Channel.reader, Im.t) Channel.t
                ; chan_w : (Channel.writer, Im.t) Channel.t
                ; client_chan_w : (Channel.writer, Im.t) Channel.t
                ; conn : 'a
                }

    type connected = { nick : string }
  end

  let write_line w line =
    let buf =
      Abb_intf.Write_buf.({ buf = Bytes.of_string line
                          ; pos = 0
                          ; len = String.length line
                          })
    in
    let open Fut_comb.Infix_result_monad in
    Buffered.write w ~bufs:[buf]
    >>= fun _ ->
    Buffered.flushed w

  let read_irc_msg sock_r =
    let open Fut_comb.Infix_result_monad in
    Logs.debug (fun m -> m "Waiting to read");
    Buffered.read_line sock_r
    >>= fun line ->
    Logs.debug (fun m -> m "Read line: %s" line);
    match Im.Parse.of_string line with
      | Some msg ->
        Abb.Future.return (Ok msg)
      | _ ->
        assert false

  let rec ping_loop state =
    let open Abb.Future.Infix_monad in
    Abb.Sys.sleep 30.0
    >>= fun () ->
    Logs.debug (fun m -> m "Sending ping");
    Channel.send state.State.client_chan_w (Im.Ping state.State.server)
    >>= function
    | `Ok () -> ping_loop state
    | `Closed -> Abb.Future.return ()

  let read_irc_msg_fut sock_r =
    let open Abb.Future.Infix_monad in
    read_irc_msg sock_r >>| fun m -> `Irc m

  let read_client_msg_fut chan_r =
    let open Abb.Future.Infix_monad in
    Channel.recv chan_r >>| fun r -> `Read r

  let rec loop irc_fut client_fut state =
    let open Abb.Future.Infix_monad in
    Fut_comb.first irc_fut client_fut
    >>= function
    | (`Irc (Ok (Im.Ping _)), client_fut) ->
      write_line state.State.sock_w Im.(Writer.to_string (Pong state.State.server))
      >>= fun _ ->
      loop
        (read_irc_msg_fut state.State.sock_r)
        client_fut
        state
    | (`Irc (Ok (Im.Pong _)), client_fut) ->
      loop
        (read_irc_msg_fut state.State.sock_r)
        client_fut
        state
    | (`Irc (Ok msg), client_fut) ->
      begin Channel.send state.State.chan_w msg
        >>= function
        | `Ok () ->
          loop
            (read_irc_msg_fut state.State.sock_r)
            client_fut
            state
        | `Closed ->
          Logs.debug (fun m -> m "Channel closed");
          Abb.Future.return (Ok ())
      end
    | (`Read (`Ok msg), irc_fut) ->
      let open Fut_comb.Infix_result_monad in
      write_line state.State.sock_w (Im.Writer.to_string msg)
      >>= fun () ->
      loop
        irc_fut
        (read_client_msg_fut state.State.chan_r)
        state
    | _ ->
      Logs.debug (fun m -> m "Loop done");
      Channel.close state.State.chan_w
      >>= fun () ->
      Abb.Future.return (Ok ())

  let connection_registration_user nick state =
    let open Fut_comb.Infix_result_monad in
    read_irc_msg state.State.sock_r
    >>= function
    | Im.User _ ->
      Logs.info (fun m -> m "User %s connected" nick);
      Fut_comb.to_result
        (Fut_comb.List.iter
           ~f:(fun msg -> Fut_comb.ignore (write_line state.State.sock_w (Im.Writer.to_string msg)))
           (welcome state.State.server nick))
      >>= fun () ->
      Fut_comb.to_result (Abb.Future.fork (ping_loop state))
      >>= fun () ->
      loop
        (read_irc_msg_fut state.State.sock_r)
        (read_client_msg_fut state.State.chan_r)
        { state with State.conn = State.({nick}) }
    | _ ->
      failwith "nyi"

  let rec connection_registration_nick (state : unit State.t) =
    let open Fut_comb.Infix_result_monad in
    read_irc_msg state.State.sock_r
    >>= function
    | Im.Nick nick ->
      connection_registration_user nick state
    | _ ->
      connection_registration_nick state

  let run conn =
    let open Abb.Future.Infix_monad in
    Logs.info (fun m -> m "Irc Server Started");
    let (sock_r, sock_w) = Buffered_of.of_tcp_socket conn in
    (* Two groups of channels, one is to publish events to the client and the
       other is for the client to publish events. *)
    Channel_queue.T.create ()
    >>= fun chan_queue ->
    let (client_chan_r, chan_w) = Channel_queue.to_abb_channel chan_queue in
    Channel_queue.T.create ()
    >>= fun client_chan_queue ->
    let (chan_r, client_chan_w) = Channel_queue.to_abb_channel chan_queue in
    let state = State.({ server = "localhost"
                       ; sock_r
                       ; sock_w
                       ; chan_r
                       ; chan_w
                       ; client_chan_w
                       ; conn = ()
                       })
    in
    write_line state.State.sock_w (Printf.sprintf ":%s NOTICE * :Hello\r\n" state.State.server)
    >>= fun _ ->
    Abb.Future.fork (connection_registration_nick state)
    >>= fun () ->
    Abb.Future.return (Ok (client_chan_r, client_chan_w))
end
