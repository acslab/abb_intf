module Privmsg = struct
  type t = { src : string
           ; dst : string
           ; msg : string
           }
end

module User = struct
  type t = { user : string
           ; mode : string
           ; realname : string
           }
end

module Raw = struct
  type t = { prefix : string
           ; cmd : string
           ; params : string list
           ; trailing : string
           }
end

type t =
  | Privmsg of Privmsg.t
  | Nick of string
  | User of User.t
  | Ping of string
  | Pong of string
  | Raw of Raw.t

module Parse = struct
  exception Parse_exception

  let make_irc_msg prefix cmd params trailing =
    match (cmd, params) with
      | ("NICK", [nick]) -> Nick nick
      | ("PRIVMSG", [dst]) -> Privmsg Privmsg.({ src = prefix; dst = dst; msg = trailing })
      | ("USER", [user; mode; _]) -> User User.({ user; mode; realname = trailing })
      | _ -> Raw Raw.({ prefix; cmd; params; trailing })

  let parse_prefix str =
    match CCString.Split.left ~by:" " str with
      | Some (prefix, r) when String.length prefix > 0 && prefix.[0] = ':' ->
        let prefix = CCString.Sub.(copy (make prefix 1 ~len:(String.length prefix - 1))) in
        (prefix, r)
      | _ ->
        ("", str)

  let parse_cmd str =
    match CCString.Split.left ~by:" " str with
      | Some (cmd, r) -> (cmd, r)
      | _ -> raise Parse_exception

  let rec parse_params str =
    match CCString.Split.left ~by:" " str with
      | Some (param, trailing) when String.length param > 0 && param.[0] <> ':' ->
        let (param', trailing') = parse_params trailing in
        (param::param', trailing')
      | None when String.length str > 0 && str.[0] <> ':' ->
        ([str], "")
      | _ ->
        ([], str)

  let parse_trailing = function
    | str when String.length str > 0 && str.[0] = ':' ->
      CCString.Sub.(copy (make str 1 ~len:(String.length str - 1)))
    | str ->
      str

  let of_string str =
    let (prefix, r) = parse_prefix str in
    let (cmd, r) = parse_cmd r in
    let (params, r) = parse_params r in
    let trailing = parse_trailing r in
    try
      Some (make_irc_msg prefix cmd params trailing)
    with
      | _ -> None
end

module Writer = struct
  let of_raw b {Raw.prefix; cmd; params; trailing} =
    if String.length prefix > 0 then begin
      Buffer.add_string b (":" ^ prefix);
      Buffer.add_char b ' ';
    end;

    Buffer.add_string b cmd;
    Buffer.add_char b ' ';
    Buffer.add_string b (CCString.concat " " params);

    if String.length trailing > 0 then begin
      Buffer.add_char b ' ';
      Buffer.add_string b (":" ^ trailing);
    end;

    Buffer.add_string b "\r\n"

  let to_string msg =
    let b = Buffer.create 512 in
    let () =
      match msg with
        | Ping server -> of_raw b (Raw.{prefix = ""; cmd = "PING"; params = []; trailing = server})
        | Pong server -> of_raw b (Raw.{prefix = ""; cmd = "PONG"; params = [server]; trailing = ""})
        | Raw raw -> of_raw b raw
        | _ -> ()
    in
    Buffer.contents b
end
