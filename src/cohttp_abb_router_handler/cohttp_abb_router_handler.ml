module Make (Abb : Abb_intf.S with type Native.t = Unix.file_descr) = struct
  module Http = Cohttp_abb.Make(Abb)

  let handler router req r w =
    let uri = Http.Request.uri req in
    let handler = router uri in
    handler req r w
end
