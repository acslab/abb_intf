module Make (Abb : Abb_intf.S with type Native.t = Unix.file_descr) : sig
  val handler :
    (Uri.t -> Cohttp_abb.Make(Abb).Server.handler) ->
    Cohttp_abb.Make(Abb).Server.handler
end
