module List = ListLabels

module Abb = Abb_scheduler_kqueue
module Channel = Abb_channel.Make(Abb.Future)
module Fut_comb = Abb_future_combinators.Make(Abb.Future)
module Buffered = Abb_io_buffered.Make(Abb.Future)
module Buffered_of = Abb_io_buffered.Of(Abb)
module Http = Cohttp_abb.Make(Abb)
module Time = Abb_time.Make(Abb.Future)(Abb.Sys)

module Cmdline = struct
  module C = Cmdliner

  let check_cmd check =
    let concurrency =
      let doc = "Number of concurrent requests." in
      C.Arg.(value & opt int 10 & info ["c"; "concurrency"] ~docv:"NUM" ~doc)
  in
  let doc = "check URLs if they respond with 200" in
  C.Term.(const check $ concurrency, info "check" ~doc)

  let default_cmd =
    let doc = "Check URLs if they are up" in
    C.Term.(ret (const (`Help (`Pager, None))),
            info "help" ~doc)
end

let headers = Cohttp.Header.of_list [("user-agent", "MimirOps Uptime Checker/1.0")]

let sort_by_family addrinfos =
  let module Addrinfo = Abb_intf.Socket.Addrinfo in
  let module Domain = Abb_intf.Socket.Domain in
  List.sort
    ~cmp:(fun {Addrinfo.family = x; _} {Addrinfo.family = y; _} ->
        match (x, y) with
          | (Domain.Inet4, Domain.Inet4)
          | (Domain.Inet6, Domain.Inet6)
          | (Domain.Unix, Domain.Unix) -> 0
          | (_, Domain.Inet4) -> 1
          | (Domain.Inet4, _)
          | (Domain.Inet6, _) -> -1
          | (Domain.Unix, _) -> 1)
    addrinfos

let log_resp id url duration resp =
  let code = resp.Http.Response.status in
  Logs.info
    (fun m ->
       m "CHECK: %s %s %d %0.2f"
         id
         url
         (Cohttp.Code.code_of_status code)
         (Abb_time.Span.to_sec duration))

let resolve_host host =
  let open Fut_comb.Infix_result_monad in
  Abb.Socket.getaddrinfo (Abb_intf.Socket.Addrinfo_query.Host host)
  >>= fun addrinfos ->
  Abb.Future.return (Ok (List.hd (sort_by_family addrinfos)))

let call_with_timeout ~timeout addrinfo uri =
  let open Abb.Future.Infix_monad in
  let connect uri =
    match Uri.scheme uri with
      | Some "http" ->
        Http.Client.connect_http addrinfo uri
      | Some "https" ->
        let conf = Otls.Tls_config.create () in
        ignore (Otls.Tls_config.insecure_noverifycert conf);
        ignore (Otls.Tls_config.set_ciphers conf (Otls.Tls_ciphers.Insecure));
        Http.Client.connect_https addrinfo conf uri
      | Some any ->
        failwith (Printf.sprintf "Unknown scheme: %s" any)
      | None ->
        failwith (Printf.sprintf "Missing scheme: %s" (Uri.to_string uri))
  in
  let timeout_fut = Abb.Sys.sleep timeout in
  let call_fut =
    connect uri
    >>= function
    | Ok (ic, oc) ->
      let req =
        Http.Request.make_for_client ~chunked:false ~body_length:Int64.zero `HEAD uri
      in
      Http.Client.do_request req ic oc
      >>= fun (resp, ic) ->
      Fut_comb.ignore (Buffered.close ic)
      >>= fun () ->
      Abb.Future.return resp
    | Error err ->
      Printf.eprintf "Error: %s\n" (Cohttp_abb.show_connect_https_err err);
      assert false
  in
  Fut_comb.first
    (timeout_fut >>| fun () -> `Timedout)
    (call_fut >>| fun v -> `Ok v)
  >>= fun (ret, other_fut) ->
  Abb.Future.abort other_fut
  >>| fun () ->
  ret

let check_url id uri =
  let open Abb.Future.Infix_monad in
  Fut_comb.ignore
    (Abb.Future.await
       (Fut_comb.on_failure
          (fun () ->
             resolve_host (CCOpt.get_exn (Uri.host uri))
             >>= function
             | Ok addrinfo ->
               Time.Mono.now ()
               >>= fun start_time ->
               call_with_timeout ~timeout:10.0 addrinfo uri
               >>= begin function
                 | `Ok resp ->
                   Time.Mono.now ()
                   >>| fun end_time ->
                   log_resp id (Uri.to_string uri) (Time.Mono.diff start_time end_time) resp
                 | `Timedout ->
                   Logs.err (fun m -> m "CHECK: %s %s TIMEDOUT" id (Uri.to_string uri));
                   Abb.Future.return ()
               end
             | Error (`Unexpected exn) ->
               Logs.err (fun m -> m "CHECK: %s %s FAILED" id (Uri.to_string uri));
               Logs.err (fun m -> m "Exception: %s" (Printexc.to_string exn));
               Logs.err (fun m -> m "Backtrace: %s" (Printexc.get_backtrace ()));
               Fut_comb.unit)
          ~failure:(fun () ->
              Logs.err (fun m -> m "CHECK: %s %s FAILED" id (Uri.to_string uri));
              Fut_comb.unit))
     >>| function
     | `Det () ->
       ()
     | `Exn (exn, bt_opt) ->
       Logs.err (fun m -> m "Exception: %s" (Printexc.to_string exn));
       CCOpt.iter
         (fun bt -> Logs.err (fun m -> m "Backtrace: %s" (Printexc.raw_backtrace_to_string bt)))
         bt_opt;
       ()
     | `Aborted ->
       ())


let rec check_clients sched r max = function
  | l when List.length l < max ->
    let open Abb.Future.Infix_monad in
    Buffered.read_line r
    >>= begin function
      | Ok line ->
        begin match CCString.Split.right ~by:"\t" line with
          | Some (id, uri_s) ->
            let id = String.trim id in
            let uri = Uri.of_string uri_s in
            check_clients sched r max (check_url id uri::l)
          | None ->
            assert false
        end
      | Error (`Unexpected End_of_file) ->
        Fut_comb.ignore (Fut_comb.all l)
      | Error _ ->
        assert false
    end
  | l ->
    let open Abb.Future.Infix_monad in
    Fut_comb.firstl l
    >>= fun (_, ls) ->
    Logs.debug
      (fun m ->
         if Random.int 5 = 0 then begin
           let durations = Abb.Scheduler.exec_duration sched in
           let sum = Array.fold_left (+.) 0.0 durations in
           let avg = sum /. (float (Array.length durations)) in
           m "Avg = %0.2f" avg
         end);
    check_clients sched r max ls

let run_client sched concurrency r = check_clients sched r concurrency []

let check_urls concurrency =
  let (r, _) = Buffered_of.of_file Abb.File.stdin in
  let sched = Abb.Scheduler.create () in
  match Abb.Scheduler.run sched (fun () -> run_client sched concurrency r) with
    | `Det () ->
      Logs.info (fun m -> m "Finished")
    | `Exn (exn, bt_opt) ->
      Printf.eprintf "%s\n" (Printexc.to_string exn);
      CCOpt.iter
        (fun bt -> Printf.eprintf "%s\n" (Printexc.raw_backtrace_to_string bt))
        bt_opt;
      exit 1
    | `Aborted ->
      assert false

let cmds = [Cmdline.check_cmd check_urls]

let reporter ppf =
  let report src level ~over k msgf =
    let k _ = over (); k () in
    let with_stamp h tags k ppf fmt =
      (* TODO: Make this use the proper Abb time *)
      let time = Unix.gettimeofday () in
      let time_str = ISO8601.Permissive.string_of_datetime time in
      Format.kfprintf
        k
        ppf
        ("[%s]%a @[" ^^ fmt ^^ "@]@.")
        time_str
        Logs.pp_header
        (level, h)
    in
    msgf @@ fun ?header ?tags fmt -> with_stamp header tags k ppf fmt
  in
  { Logs.report = report }

let main () =
  Logs.set_reporter (reporter Format.std_formatter);
  Logs.set_level ~all:true (Some Logs.Debug);
  Random.self_init ();
  Logs.info (fun m -> m "Started");
  match Cmdliner.Term.eval_choice Cmdline.default_cmd cmds with
    | `Error _ -> exit 1
    | _ -> exit 0

let () = main ()
