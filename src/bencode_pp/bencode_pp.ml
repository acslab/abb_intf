module List = ListLabels
module String_map = Map.Make(String)
module View = Labbittorrent_bencode.View

let rec read_in_channel in_chan b =
  let buf = Bytes.create 1024 in
  match input in_chan buf 0 1024 with
    | 0 ->
      b
    | n ->
      read_in_channel in_chan (Bytes.cat b (Bytes.sub buf 0 n))

let rec pp = function
  | View.Dict d -> begin
    Format.printf "@[<v>{@[<v 2>@ ";
    String_map.iter
      (fun k v ->
        Format.printf "@[<v 2>\"%s\":@ " (String.escaped k);
        pp v;
        Format.printf "@],@ ")
      d;
    Format.printf "@]@ }@]@ ";
  end
  | View.List l -> begin
    Format.printf "@[<v 2>[@ ";
    List.iter
      ~f:(fun v -> pp v; Format.printf ",@ ")
      l;
    Format.printf "]@]@ "
  end
  | View.Int i ->
    Format.printf "%s" (Num.string_of_num i)
  | View.String s ->
    Format.printf "\"%s\"" (String.escaped s)

let main () =
  let b = read_in_channel stdin (Bytes.create 0) in
  let t = Labbittorrent_bencode.of_bytes b |> snd in
  pp (Labbittorrent_bencode.to_view t)

let () = main ()
