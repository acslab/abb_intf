module List = ListLabels
module String_map = Map.Make(String)
module Uri_set = Set.Make(Uri)

module Bencode = Labbittorrent_bencode

module Abb = Abb_scheduler_select

module Fut_comb = Abb_future_combinators.Make(Abb.Future)

module Tracker_client = Labbittorrent_tracker_client.Make(Abb.Future)
module Tracker_client_udp = Labbittorrent_tracker_client_udp.Make(Abb)
module Tracker_client_http = Labbittorrent_tracker_client_http.Make(Abb)

module Torrent = Labbittorrent_torrent

module Peer_set = Labbittorrent_peer_set

let rec read_in_channel in_chan b =
  let buf = Bytes.create 1024 in
  match input in_chan buf 0 1024 with
    | 0 ->
      b
    | n ->
      read_in_channel in_chan (Bytes.cat b (Bytes.sub buf 0 n))

let compute_peer_id () =
  let b = Bytes.create 20 in
  let name = "ABB1-0-" in
  Bytes.blit_string name 0 b 0 (String.length name);
  for i = String.length name to 20 - 1 do
    Bytes.set b i (Char.chr (Random.int 256))
  done;
  Bytes.to_string b

let create_udp_tracker client_s url =
  let open Abb.Future.Infix_monad in
  let host =
    match Uri.host url with
      | Some host -> host
      | None -> assert false
  in
  let port =
    match Uri.port url with
      | Some port -> port
      | None -> assert false
  in
  Abb.Socket.getaddrinfo (Abb_intf.Socket.Addrinfo_query.Host host)
  >>= function
  | Ok addrinfos ->
    let sockaddrs =
      List.map
        ~f:(fun addrinfo ->
            match addrinfo.Abb_intf.Socket.Addrinfo.addr with
              | Abb_intf.Socket.Sockaddr.Inet inet ->
                Abb_intf.Socket.Sockaddr.(Inet { inet with port = port })
              | _ ->
                assert false)
        addrinfos
    in
    let init_args =
      Tracker_client.({ tracker = List.hd sockaddrs
                      ; name = host
                      ; sleep = Abb.Sys.sleep
                      ; now = Abb.Sys.time
                      })
    in
    Tracker_client.create init_args client_s Tracker_client_udp.announce
  | Error _ ->
    assert false

let create_http_tracker client_s url =
  let init_args =
    Tracker_client.({ tracker = url
                    ; name = Uri.host_with_default ~default:"unknown" url
                    ; sleep = Abb.Sys.sleep
                    ; now = Abb.Sys.time
                    })
  in
  Tracker_client.create init_args client_s Tracker_client_http.announce

let create_tracker client_s url =
  match Uri.scheme url with
    | Some "udp" ->
      create_udp_tracker client_s url
    | Some "http" ->
      create_http_tracker client_s url
    | _ ->
      assert false

let run_client torrent client_s =
  let open Abb.Future.Infix_monad in
  let urls = Uri_set.elements (Torrent.announce_urls torrent) in
  Fut_comb.List.map
    ~f:(create_tracker client_s)
    urls
  >>= fun trackers ->
  Fut_comb.List.map
    ~f:(fun tc ->
        Tracker_client.get_peers tc
        >>| function
        | `Ok v -> v
        | `Closed -> assert false)
    trackers
  >>= fun peers ->
  let all_peers = List.fold_left ~f:Peer_set.union ~init:Peer_set.empty peers in
  Logs.debug (fun m -> m "Peers: %d%!" (Peer_set.cardinal all_peers));
  Peer_set.iter
    (fun (addr, port) ->
       Logs.debug (fun m -> m "Peer: %s Port: %d" (Unix.string_of_inet_addr addr) port))
    all_peers;
  Fut_comb.List.map
    ~f:(fun tc ->
        Tracker_client.get_tracker_state tc
        >>| function
        | `Closed -> assert false
        | `Ok fut -> fut)
      trackers
  >>| fun states ->
  List.iter2
    ~f:(fun url state ->
        match state with
          | `Ok -> Logs.debug (fun m -> m "%s\tOk" (Uri.to_string url))
          | `Failed sec -> Logs.err (fun m -> m "%s\tFailed: %d" (Uri.to_string url) sec))
    urls
    states

let main () =
  Logs.set_reporter (Logs_fmt.reporter ());
  Logs.set_level ~all:true (Some Logs.Debug);
  Random.self_init ();
  let peer_id = compute_peer_id () in
  Logs.info (fun m -> m "Peer Id: %s" (String.escaped peer_id));
  let b = read_in_channel stdin (Bytes.create 0) in
  let torrent = Torrent.of_bencode (snd (Bencode.of_bytes b)) in
  let length =
    torrent
    |> Torrent.files
    |> List.map ~f:Torrent.File.length
    |> List.map ~f:Int64.to_int
    |> List.fold_left ~init:0 ~f:(+)
  in
  let client_s = Tracker_client.({ info_hash = Torrent.info_hash torrent
                                 ; peer_id = peer_id
                                 ; ip = None
                                 ; port = 0
                                 ; uploaded = 0
                                 ; downloaded = 0
                                 ; left = length
                                 })
  in
  ignore (Abb.Scheduler.run (Abb.Scheduler.create ()) (fun () -> run_client torrent client_s))

let () = main ()
