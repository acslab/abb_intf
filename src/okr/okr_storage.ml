module String_map = CCMap.Make(String)

module Channel = Abb_channel.Make(Abb.Future)
module Fut_comb = Abb_future_combinators.Make(Abb.Future)
module Serializer = Abb_service_serializer.Make(Abb.Future)

type t = { serializer : Serializer.t
         ; db : Sqlite3.db
         }

type err = [ `Sql_error of Sqlite3.Rc.t | `Closed ]

module Sql = struct
  let foreign_key_pragma = "PRAGMA foreign_keys = ON;"

  let create_database = CCOpt.get_exn (Okr_files.read "create_database.sql")
  let current_objectives = CCOpt.get_exn (Okr_files.read "current_objectives.sql")
  let current_krs = CCOpt.get_exn (Okr_files.read "current_krs.sql")
  let delete_kr = CCOpt.get_exn (Okr_files.read "delete_kr.sql")
  let add_objective = CCOpt.get_exn (Okr_files.read "add_objective.sql")
  let get_organizations = CCOpt.get_exn (Okr_files.read "get_organizations.sql")
  let add_kr = CCOpt.get_exn (Okr_files.read "add_kr.sql")
  let add_user = CCOpt.get_exn (Okr_files.read "add_user.sql")
  let add_session = CCOpt.get_exn (Okr_files.read "add_session.sql")
  let load_session = CCOpt.get_exn (Okr_files.read "load_session.sql")

  let int = function
    | Sqlite3.Data.INT i -> i
    | _ -> assert false

  let float = function
    | Sqlite3.Data.FLOAT f -> f
    | _ -> assert false

  let float_opt = function
    | Sqlite3.Data.NULL -> None
    | Sqlite3.Data.FLOAT f -> Some f
    | _ -> assert false

  let text = function
    | Sqlite3.Data.TEXT t -> t
    | _ -> assert false

  let column stmt idx f = f (Sqlite3.column stmt idx)
end

let exec db sql =
  let open Sqlite3.Rc in
  match Sqlite3.exec db sql with
    | OK
    | DONE->
      Ok ()
    | ERROR
    | INTERNAL
    | PERM
    | ABORT
    | BUSY
    | LOCKED
    | NOMEM
    | READONLY
    | INTERRUPT
    | IOERR
    | CORRUPT
    | NOTFOUND
    | FULL
    | CANTOPEN
    | PROTOCOL
    | EMPTY
    | SCHEMA
    | TOOBIG
    | CONSTRAINT
    | MISMATCH
    | MISUSE
    | NOFLS
    | AUTH
    | FORMAT
    | RANGE
    | NOTADB
    | ROW
    | UNKNOWN _ as err ->
      Error (`Sql_error err)

let step stmt =
  let open Sqlite3.Rc in
  match Sqlite3.step stmt with
    | OK
    | DONE
    | ROW as ret ->
      Ok ret
    | ERROR
    | INTERNAL
    | PERM
    | ABORT
    | BUSY
    | LOCKED
    | NOMEM
    | READONLY
    | INTERRUPT
    | IOERR
    | CORRUPT
    | NOTFOUND
    | FULL
    | CANTOPEN
    | PROTOCOL
    | EMPTY
    | SCHEMA
    | TOOBIG
    | CONSTRAINT
    | MISMATCH
    | MISUSE
    | NOFLS
    | AUTH
    | FORMAT
    | RANGE
    | NOTADB
    | UNKNOWN _ as err ->
      Error (`Sql_error err)

let exec_step stmt =
  let open CCResult.Infix in
  step stmt
  >>= fun _ ->
  Ok ()

let run serializer f =
  let open Abb.Future.Infix_monad in
  Serializer.run
    serializer
    ~f:(fun () -> Abb.Future.return (f ()))
  >>= function
  | `Ok res -> Abb.Future.return res
  | `Closed -> Abb.Future.return (Error `Closed)

let map ~f stmt =
  let rec map' f stmt =
    match step stmt with
      | Ok Sqlite3.Rc.ROW ->
        let v = f stmt in
        v::map' f stmt
      | Ok Sqlite3.Rc.OK
      | Ok Sqlite3.Rc.DONE ->
        []
      | _ ->
        assert false
  in
  Ok (map' f stmt)

let rec fold ~f ~init stmt =
  match step stmt with
    | Ok Sqlite3.Rc.ROW ->
      let init = f init stmt in
      fold ~f ~init stmt
    | Ok Sqlite3.Rc.OK
    | Ok Sqlite3.Rc.DONE ->
      Ok init
    | Ok _ ->
      assert false
    | Error _ as err ->
      err

let maybe_bind stmt = function
  | Some bindings ->
    let count = ref 0 in
    ListLabels.iter
      ~f:(fun b ->
          count := !count + 1;
          ignore (Sqlite3.bind stmt !count b))
      bindings
  | None ->
    ()

let with_stmt ?bind stmt ~f =
  maybe_bind stmt bind;
  let res = f stmt in
  ignore (Sqlite3.finalize stmt);
  res

let with_sql ?bind db sql ~f =
  let stmt = Sqlite3.prepare db sql in
  with_stmt ?bind stmt ~f

let create db_path =
  let db_fname = Filename.concat db_path "okr.db" in
  let open Fut_comb.Infix_result_monad in
  Fut_comb.to_result (Serializer.create ())
  >>= fun serializer ->
  let db = Sqlite3.db_open db_fname in
  Abb.Future.return (exec db Sql.foreign_key_pragma)
  >>= fun () ->
  Abb.Future.return (Ok { serializer; db })

let destroy t =
  let open Abb.Future.Infix_monad in
  ignore (Sqlite3.db_close t.db);
  Channel.close t.serializer
  >>= fun () ->
  Abb.Future.return (Ok ())

let create_tables t =
  Abb.Future.return (exec t.db Sql.create_database)

let add_user t ~email ~name ~passwd =
  let email = Okr_user.to_string email in
  let passwd = Scrypt.encrypt_exn passwd "key" in
  run
    t.serializer
    (fun () ->
       with_sql
         ~bind:Sqlite3.Data.([TEXT email; TEXT name; TEXT passwd])
         t.db
         Sql.add_user
         ~f:exec_step)

let create_session t user_id =
  let open Fut_comb.Infix_result_monad in
  let id = Uuidm.to_string (Uuidm.create `V4) in
  let user_id = Okr_user.to_string user_id in
  run
    t.serializer
    (fun () ->
       with_sql
         ~bind:Sqlite3.Data.([TEXT id; TEXT user_id])
         t.db
         Sql.add_session
         ~f:exec_step)
  >>| fun () ->
  id

let load_session t ~id = failwith "nyi"

let verify_user_password t user = failwith "nyi"

let org_of_stmt stmt =
  Okr_data.Org.({ id = Int64.to_string Sql.(column stmt 0 int)
                ; name = Sql.(column stmt 1 text)
                })

let organizations t user_id =
  let user_id = Okr_user.to_string user_id in
  run
    t.serializer
    (fun () ->
       with_sql
         ~bind:Sqlite3.Data.([TEXT user_id])
         t.db
         Sql.get_organizations
         ~f:(map ~f:org_of_stmt))

let okr_of_stmt okr_map stmt =
  let id = Int64.to_string Sql.(column stmt 0 int) in
  let desc = Sql.(column stmt 1 text) in
  let time_type = Sql.(column stmt 2 text) in
  let okr = Okr_data.Okr.({ id; desc; time_type; krs = [] }) in
  String_map.add id okr okr_map

let add_kr_to_okr okr_map stmt =
  let kr_id = Int64.to_string Sql.(column stmt 0 int) in
  let obj_id = Int64.to_string Sql.(column stmt 1 int) in
  let desc = Sql.(column stmt 2 text) in
  let score = Sql.(column stmt 3 float_opt) in
  let kr = Okr_data.Kr.({ id = kr_id; desc; score }) in
  match String_map.get obj_id okr_map with
    | Some okr ->
      let okr = Okr_data.Okr.({ okr with krs = kr::okr.krs }) in
      String_map.add obj_id okr okr_map
    | None ->
      (* TODO: Handle changes over time *)
      failwith "nyi"

let current_okrs t user_id =
  let user_id = Okr_user.to_string user_id in
  run
    t.serializer
    (fun () ->
       let open CCResult.Infix in
       with_sql
         ~bind:[Sqlite3.Data.TEXT user_id]
         t.db
         Sql.current_objectives
         ~f:(fold ~f:okr_of_stmt ~init:String_map.empty)
       >>= fun okr_map ->
       with_sql
         ~bind:[Sqlite3.Data.TEXT user_id]
         t.db
         Sql.current_krs
         ~f:(fold ~f:add_kr_to_okr ~init:okr_map)
       >>= fun okr_map ->
       let okrs =
         CCListLabels.map
           ~f:(fun (_, okr) -> Okr_data.Okr.({ okr with krs = CCList.rev okr.krs }))
           (List.rev (String_map.to_list okr_map))
       in
       Ok okrs)

let create_objective t org_id objective time_type =
  let org_id = Int64.of_string (Okr_org.to_string org_id) in
  run
    t.serializer
    (fun () ->
       with_sql
         ~bind:Sqlite3.Data.([INT org_id; TEXT objective; TEXT time_type; TEXT "2018-12-31"])
         t.db
         Sql.add_objective
         ~f:exec_step)

let create_kr t obj_id key_result =
  let obj_id = Int64.of_string (Okr_objective.to_string obj_id) in
  run
    t.serializer
    (fun () ->
       with_sql
         ~bind:Sqlite3.Data.([INT obj_id; TEXT key_result])
         t.db
         Sql.add_kr
         ~f:exec_step)

let delete_kr t user_id obj_id kr_id =
  let user_id = Okr_user.to_string user_id in
  let obj_id = Int64.of_string (Okr_objective.to_string obj_id) in
  let kr_id = Int64.of_string (Okr_kr.to_string kr_id) in
  run
    t.serializer
    (fun () ->
       with_sql
         ~bind:Sqlite3.Data.([INT obj_id; INT kr_id; TEXT user_id])
         t.db
         Sql.delete_kr
         ~f:exec_step)
