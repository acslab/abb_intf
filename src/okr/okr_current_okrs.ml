module Brtl = Brtl.Make(Abb)

module Storage = Okr_storage

let user = Okr_user.of_string "test@test.com"

let run storage ctx =
  let open Abb.Future.Infix_monad in
  Storage.current_okrs storage user
  >>| function
  | Ok okrs ->
    Brtl.(Ctx.set_response
            (Rspnc.create
               ~status:`OK
               (Yojson.Safe.to_string (Okr_data.Okrs.to_yojson okrs)))
            ctx)
  | Error _ ->
    Brtl.(Ctx.set_response
            (Rspnc.create ~status:`Internal_server_error "")
            ctx)
