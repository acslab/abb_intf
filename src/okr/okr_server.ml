module Brtl = Brtl.Make(Abb)

module Mw_log = Brtl_mw_log.Make(Abb)

module Account = Okr_account
module Assets = Okr_assets
module Current_okrs = Okr_current_okrs
module Login = Okr_login
module Okrs = Okr_okrs
module Session = Okr_session
module Storage = Okr_storage

let default_route ctx =
  Abb.Future.return Brtl.(Ctx.set_response (Rspnc.create ~status:`Not_found "") ctx)

let assets_rt () = Brtl.Rtng.Route.(rel / "assets" /% Path.string)
let current_okrs_rt () = Brtl.Rtng.Route.(rel / "v0" / "current_okrs")
let homepage_rt () = Brtl.Rtng.Route.rel
let okrs_delete_kr_rt () =
  Brtl.Rtng.Route.(rel / "v0" / "okr" /% Path.string / "delete" /% Path.string)
let okr_rt () = Brtl.Rtng.Route.(rel / "v0" / "okr")
let account_orgs_rt () = Brtl.Rtng.Route.(rel / "v0" / "account" / "organizations")
let okr_kr_rt () = Brtl.Rtng.Route.(rel / "v0" / "okr" /% Path.string / "kr")
let login_rt () = Brtl.Rtng.Route.(rel / "login")
let login_api_rt () = Brtl.Rtng.Route.(rel / "v0" / "login")

let assets = Assets.run
let current_okrs = Current_okrs.run
let homepage = Session.with_user (Uri.of_string "/login") (fun _ -> Assets.run "homepage.html")
let okrs_delete_kr = Okrs.delete_kr
let okr = Okrs.create_objective
let account_orgs = Account.organizations
let okr_kr = Okrs.create_kr
let login = Assets.run "login.html"
let login_api = Login.run

let rtng storage =
  Brtl.Rtng.create
    ~default:default_route
    Brtl.Rtng.Route.([ (`GET, homepage_rt () --> homepage)
                     ; (`GET, current_okrs_rt () --> current_okrs storage)
                     ; (`GET, assets_rt () --> assets)
                     ; (`POST, okrs_delete_kr_rt () --> okrs_delete_kr storage)
                     ; (`POST, okr_rt () --> okr storage)
                     ; (`GET, account_orgs_rt () --> account_orgs storage)
                     ; (`POST, okr_kr_rt () --> okr_kr storage)
                     ; (`GET, login_rt () --> login)
                     ; (`POST, login_api_rt () --> login_api storage)
                     ])

let run ~port db_path =
  let one_min = Duration.of_min 1 in
  let run () =
    let open Abb.Future.Infix_monad in
    Storage.create db_path
    >>= fun res ->
    let storage = CCResult.get_exn res in
    let cfg =
      Brtl.Cfg.create
        ~port
        ~read_header_timeout:(Some one_min)
        ~handler_timeout:(Some one_min)
    in
    let mw_log = Mw_log.create () in
    let mw_session = Session.create storage in
    let mw = Brtl.Mw.create [mw_log; mw_session] in
    Brtl.run cfg mw (rtng storage)
  in
  match Abb.Scheduler.run (Abb.Scheduler.create ()) run with
    | `Det () -> ()
    | _ -> assert false

