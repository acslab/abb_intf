type t

type err = [ `Sql_error of Sqlite3.Rc.t | `Closed ]

val create : string -> (t, [> err ]) result Abb.Future.t
val destroy : t -> (unit, [> err ]) result Abb.Future.t

val create_tables : t -> (unit, [> err ]) result Abb.Future.t

val add_user :
  t ->
  email:Okr_user.t ->
  name:string ->
  passwd:string ->
  (unit, [> err ]) result Abb.Future.t

val create_session : t -> Okr_user.t -> (string, [> err ]) result Abb.Future.t

val load_session : t -> id:string -> (Okr_user.t option, [> err ]) result Abb.Future.t

val verify_user_password : t -> Okr_user.t -> string -> (bool, [> err ]) result Abb.Future.t

val organizations : t -> Okr_user.t -> (Okr_data.Org.t list, [> err ]) result Abb.Future.t

val current_okrs : t -> Okr_user.t -> (Okr_data.Okr.t list, [> err ]) result Abb.Future.t

val create_objective : t -> Okr_org.t -> string -> string -> (unit, [> err ]) result Abb.Future.t

val create_kr : t -> Okr_objective.t -> string -> (unit, [> err ]) result Abb.Future.t

val delete_kr :
  t ->
  Okr_user.t ->
  Okr_objective.t ->
  Okr_kr.t ->
  (unit, [> err ]) result Abb.Future.t

