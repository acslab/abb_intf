module Brtl = Brtl.Make(Abb)

let login_user ctx storage username passwd =
  failwith "nyi"

let run storage ctx =
  let data = Uri.of_string ("?" ^ Brtl.Ctx.body ctx) in
  match
    CCOpt.(login_user ctx storage
           <$> Uri.get_query_param data "username"
           <*> Uri.get_query_param data "password")
  with
    | Some fut -> fut
    | None -> failwith "nyi"

