module Session : sig
  type t

  val create : Okr_user.t -> t
  val set_logged_in : Okr_user.t -> t -> t
end

val create : Okr_storage.t -> Brtl.Make(Abb).Mw.Mw.t
val get_session : ('a, 'b) Brtl.Make(Abb).Ctx.t -> Session.t option
val set_session : Session.t -> ('a, 'b) Brtl.Make(Abb).Ctx.t -> ('a, 'b) Brtl.Make(Abb).Ctx.t

(** Lookup the user session and run a function if the session exists.  Otherwise
   redirect to the URL. *)
val with_user :
  Uri.t ->
  (Okr_user.t -> Brtl.Make(Abb).Rtng.Handler.t) ->
  Brtl.Make(Abb).Rtng.Handler.t
