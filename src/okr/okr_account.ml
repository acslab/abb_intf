module Brtl = Brtl.Make(Abb)

module Storage = Okr_storage

let user = Okr_user.of_string "test@test.com"

let organizations storage ctx =
  let open Abb.Future.Infix_monad in
  Storage.organizations storage user
  >>| function
  | Ok orgs ->
    Brtl.(Ctx.set_response
            (Rspnc.create
               ~status:`OK
               (Yojson.Safe.to_string
                  (Okr_data.Orgs.to_yojson orgs)))
            ctx)
  | Error _ ->
    failwith "nyi"

