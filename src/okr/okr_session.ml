module Brtl = Brtl.Make(Abb)

module Mw_session = Brtl_mw_session.Make(Abb)

module Storage = Okr_storage

module Session = struct
  type t = { logged_in : Okr_user.t }

  let create user = { logged_in = user }

  let set_logged_in user t = { logged_in = user }
end

let key : Session.t Hmap.key = Hmap.Key.create ()

let cookie_name = "session"

let load storage id =
  let open Abb.Future.Infix_monad in
  Storage.load_session storage ~id:id
  >>= function
  | Ok (Some user) ->
    Abb.Future.return (Some (Session.create user))
  | _ ->
    Abb.Future.return None

let create_session storage Session.({logged_in = user}) =
  let open Abb.Future.Infix_monad in
  Storage.create_session storage user
  >>= function
  | Ok id ->
    Abb.Future.return id
  | Error _ ->
    assert false

let store storage id_opt session =
  match id_opt with
    | Some id ->
      Abb.Future.return id
    | None ->
      create_session storage session

let create storage =
  let config =
    Mw_session.Config.({ key
                       ; cookie_name
                       ; load = load storage
                       ; store = store storage
                       ; expiration = `Session
                       ; domain = None
                       ; path = None
                       })
  in
  Mw_session.create config

let get_session ctx = Mw_session.get_session_value key ctx

let set_session t ctx = Mw_session.set_session_value key t ctx

let with_user uri f ctx =
  match get_session ctx with
    | Some {Session.logged_in = user; _} ->
      f user ctx
    | _ ->
      let headers = Cohttp.Header.of_list [("location", Uri.to_string uri)] in
      Abb.Future.return Brtl.(Ctx.set_response (Rspnc.create ~headers ~status:`See_other "") ctx)
