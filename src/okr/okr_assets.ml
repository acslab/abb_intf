module Brtl = Brtl.Make(Abb)

let run asset ctx =
  match Okr_files.read asset with
    | Some v ->
      let mime_type = Magic_mime.lookup asset in
      let headers = Cohttp.Header.of_list [("content-type", mime_type)] in
      Abb.Future.return Brtl.(Ctx.set_response (Rspnc.create ~headers ~status:`OK v) ctx)
    | None ->
      Abb.Future.return Brtl.(Ctx.set_response (Rspnc.create ~status:`Not_found "") ctx)

