module Brtl = Brtl.Make(Abb)

module Storage = Okr_storage

let user = Okr_user.of_string "test@test.com"

let delete_kr storage obj_id kr_id ctx =
  let open Abb.Future.Infix_monad in
  let obj_id = Okr_objective.of_string obj_id in
  let kr_id = Okr_kr.of_string kr_id in
  Storage.delete_kr storage user obj_id kr_id
  >>| function
  | Ok () ->
    Brtl.(Ctx.set_response (Rspnc.create ~status:`OK "") ctx)
  | Error _ ->
    failwith "nyi"

let create_kr' ctx storage obj_id key_result =
  let open Abb.Future.Infix_monad in
  Storage.create_kr storage obj_id key_result
  >>| function
  | Ok () ->
    let headers = Cohttp.Header.of_list [("location", "/")] in
    Brtl.(Ctx.set_response (Rspnc.create ~headers ~status:`See_other "") ctx)
  | Error _ ->
    failwith "nyi"

let create_kr storage obj_id ctx =
  let obj_id = Okr_objective.of_string obj_id in
  let data = Uri.of_string ("?" ^ Brtl.Ctx.body ctx) in
  match
    CCOpt.(create_kr' ctx storage obj_id <$> Uri.get_query_param data "key_result")
  with
    | Some fut -> fut
    | None -> failwith "nyi"

let create_objective' ctx storage org objective timeframe =
  let open Abb.Future.Infix_monad in
  Storage.create_objective storage (Okr_org.of_string org) objective timeframe
  >>| function
  | Ok () ->
    let headers = Cohttp.Header.of_list [("location", "/")] in
    Brtl.(Ctx.set_response (Rspnc.create ~headers ~status:`See_other "") ctx)
  | Error _ ->
    failwith "nyi"

let create_objective storage ctx =
  let data = Uri.of_string ("?" ^ Brtl.Ctx.body ctx) in
  match
    CCOpt.(create_objective' ctx storage
           <$> Uri.get_query_param data "organization"
           <*> Uri.get_query_param data "objective"
           <*> Uri.get_query_param data "timeframe")
  with
    | Some fut -> fut
    | None -> failwith "nyi"

