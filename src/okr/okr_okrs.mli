val delete_kr :
  Okr_storage.t ->
  string ->
  string ->
  (string, unit) Brtl.Make(Abb).Ctx.t ->
  (string, Brtl.Make(Abb).Rspnc.t) Brtl.Make(Abb).Ctx.t Abb.Future.t

val create_kr :
  Okr_storage.t ->
  string ->
  (string, unit) Brtl.Make(Abb).Ctx.t ->
  (string, Brtl.Make(Abb).Rspnc.t) Brtl.Make(Abb).Ctx.t Abb.Future.t

val create_objective :
  Okr_storage.t ->
  (string, unit) Brtl.Make(Abb).Ctx.t ->
  (string, Brtl.Make(Abb).Rspnc.t) Brtl.Make(Abb).Ctx.t Abb.Future.t

