module Search_responce = struct
  type article = { id: int
                 ; uri: string
                 ; title: string
                 } [@@deriving yojson]

  type t = { data: article list } [@@deriving yojson]
end
