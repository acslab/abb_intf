exception Invalid_type
exception Invalid_variable of string

type invalid_toml = [ `Missing_hosts_generator
                    | `Missing_actions_run
                    | `Invalid_variable of string
                    | `Invalid_type
                    ]
[@@deriving show,eq]

module Variable_map = CCMap.Make(String)
module Template_map = CCMap.Make(String)

module Template = struct
  type t = { asset : string
           ; output_name : string
           ; kv : Snabela.Kv.t Snabela.Kv.Map.t
           }
end

type var_map = [ `Map of var_map Variable_map.t
               | `Value of string
               ]

type t = { hosts_generator : (string * string list)
         ; variables : var_map Variable_map.t
         ; assets_upload : string list
         ; actions_run : string list
         ; templates : Template.t Template_map.t
         }

let result_of_option err = function
  | Some v -> Ok v
  | None -> Error err

let rec variables_of_variables_table' tbl =
  TomlTypes.Table.fold
    (fun key v acc ->
       let open TomlTypes in
       match v with
         | TBool _
         | TInt _
         | TFloat _
         | TDate _
         | TArray _ ->
           raise (Invalid_variable (TomlTypes.Table.Key.to_string key))
         | TString s ->
           Variable_map.add
             (TomlTypes.Table.Key.to_string key)
             (`Value s)
             acc
         | TTable tab ->
           Variable_map.add
             (TomlTypes.Table.Key.to_string key)
             (`Map (variables_of_variables_table' tab))
             acc)
    tbl
    Variable_map.empty

let variables_of_variables_table tbl =
  try
    Ok (variables_of_variables_table' tbl)
  with
    | Invalid_variable var ->
      Error (`Invalid_variable var)

let result_of_toml_result = function
  | `Ok v -> Ok v
  | `Error e -> Error (`Toml_parse_error e)

let rec kv_of_toml_table_exn tbl =
  let module Kv = Snabela.Kv in
  let open TomlTypes in
  Table.fold
    (fun k v acc ->
       let k = Table.Key.to_string k in
       match v with
         | TBool b ->
           Kv.Map.add k (Kv.bool b) acc
         | TInt i ->
           Kv.Map.add k (Kv.int i) acc
         | TFloat f ->
           Kv.Map.add k (Kv.float f) acc
         | TString s ->
           Kv.Map.add k (Kv.string s) acc
         | TArray arr ->
           Kv.Map.add k (Kv.list (kv_of_toml_array_exn arr)) acc
         | TDate _
         | TTable _ ->
           raise Invalid_type)
    tbl
    (Kv.Map.empty)
and kv_of_toml_array_exn arr =
  let open TomlTypes in
  match arr with
    | NodeEmpty ->
      []
    | NodeTable tbls ->
      ListLabels.map ~f:kv_of_toml_table_exn tbls
    | NodeBool _
    | NodeInt _
    | NodeFloat _
    | NodeString _
    | NodeDate _
    | NodeArray _ ->
      raise Invalid_type

let hosts_generator tbl =
  let open CCOpt in
  TomlLenses.(get tbl (key "hosts" |-- table |-- key "generator" |-- array |-- strings))
  >>= function
  | [] -> None
  | cmd::args -> Some (cmd, args)

let create hosts_generator variables assets_upload actions_run templates =
    { hosts_generator = hosts_generator
    ; variables = variables
    ; assets_upload = assets_upload
    ; actions_run = actions_run
    ; templates = templates
    }

let of_toml_table tbl =
  let open TomlLenses in
  let hosts_generator_res =
    result_of_option
      `Missing_hosts_generator
      (hosts_generator tbl)
  in
  let variables_table_res =
    Ok
      (CCOpt.get_or
         ~default:TomlTypes.Table.empty
         (get tbl (key "variables" |-- table)))
  in
  let assets_upload_res =
    Ok
      (CCOpt.get_or
         ~default:[]
         (get tbl (key "assets" |-- table |-- key "upload" |-- array |-- strings)))
  in
  let actions_run_res =
    result_of_option
      `Missing_actions_run
      (get tbl (key "actions" |-- table |-- key "run" |-- array |-- strings))
  in
  let variables_res =
    let open CCResult.Infix in
    variables_table_res
    >>= fun variables_table ->
    variables_of_variables_table variables_table
  in
  let templates_table_res =
    Ok
      (CCOpt.get_or
         ~default:TomlTypes.Table.empty
         (get tbl (key "assets" |-- table |-- key "templates" |-- table)))
  in
  let templates_res =
    let open CCResult.Infix in
    templates_table_res
    >>= fun templates ->
    try
      Ok
        (TomlTypes.Table.fold
           (fun key tbl acc ->
              let asset = CCOpt.get_exn TomlLenses.(get tbl (table |-- key "asset" |-- string)) in
              print_endline asset;
              let output_name =
                CCOpt.get_or
                  ~default:asset
                  TomlLenses.(get tbl (table |-- key "output_name" |-- string))
              in
              let kv_table = CCOpt.get_exn (TomlLenses.(get tbl (table |-- key "kv" |-- table))) in
              let kv = kv_of_toml_table_exn kv_table in
              let template = Template.({ asset; output_name; kv }) in
              Template_map.add (TomlTypes.Table.Key.to_string key) template acc)
           templates
           Template_map.empty)
    with
      | Invalid_type ->
        Error `Invalid_type
  in
  let open CCResult in
  pure create
  <*> hosts_generator_res
  <*> variables_res
  <*> assets_upload_res
  <*> actions_run_res
  <*> templates_res

let hosts_generator t = t.hosts_generator
let variables t = t.variables
let assets_upload t = t.assets_upload
let actions_run t = t.actions_run
let templates t = t.templates
