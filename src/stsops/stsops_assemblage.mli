type t

type invalid_toml = [ `Missing_hosts_generator
                    | `Missing_actions_run
                    | `Invalid_variable of string
                    | `Invalid_type
                    ]

module Variable_map : module type of CCMap.Make(String)
module Template_map : module type of CCMap.Make(String)

module Template : sig
  type t = { asset : string
           ; output_name : string
           ; kv : Snabela.Kv.t Snabela.Kv.Map.t
           }
end

type var_map = [ `Map of var_map Variable_map.t
               | `Value of string
               ]

val of_toml_table : TomlTypes.table -> (t, [> invalid_toml ]) result

val hosts_generator : t -> (string * string list)
val variables : t -> var_map Variable_map.t
val assets_upload : t -> string list
val actions_run : t -> string list
val templates : t -> Template.t Template_map.t

(** Printers and equality for [invalid_toml]. *)
val pp_invalid_toml : Format.formatter -> invalid_toml -> unit
val show_invalid_toml : invalid_toml -> string
val equal_invalid_toml : invalid_toml -> invalid_toml -> bool
