module Failure_strategy = struct
  type t =
    | All_for_one
    | All_for_percent of int
    | All_for_one_with_retry of int
    | Ignore
end

type t = { ssh_cmd : string
         ; ssh_options : string list
         ; scp_cmd : string
         ; scp_options : string list
         ; failure_strategy : Failure_strategy.t
         }

type invalid_toml = [ `Invalid_ssh_cmd
                    | `Invalid_ssh_options
                    | `Invalid_scp_cmd
                    | `Invalid_scp_options
                    | `Invalid_failure_strategy
                    ]

let result_of_option err = function
  | Some v -> Ok v
  | None -> Error err

let compute_failure_strategy tbl =
  let open TomlLenses in
  let open CCOpt.Infix in
  get tbl (key "strategy" |-- table |-- key "failure" |-- table |-- key "name" |-- string)
  >>= function
  | "all-for-one" ->
    Some Failure_strategy.All_for_one
  | "all-for-percent" ->
    failwith "nyi"
  | "all-for-one-with-retry" ->
    failwith "nyi"
  | "ignore" ->
    Some Failure_strategy.Ignore
  | _ ->
    None

let create ssh_cmd ssh_options scp_cmd scp_options failure_strategy =
  { ssh_cmd; ssh_options; scp_cmd; scp_options; failure_strategy }

let of_toml_table tbl =
  let open TomlLenses in
  let connection = key "connection" |-- table in
  let ssh_cmd_res =
    result_of_option
      `Invalid_ssh_cmd
      (get tbl (connection |-- key "ssh" |-- table |-- key "command" |-- string))
  in
  let ssh_options_res =
    result_of_option
      `Invalid_ssh_options
      (get tbl (connection |-- key "ssh" |-- table |-- key "options" |-- array |-- strings))
  in
  let scp_cmd_res =
    result_of_option
      `Invalid_scp_cmd
      (get tbl (connection |-- key "scp" |-- table |-- key "command" |-- string))
  in
  let scp_options_res =
    result_of_option
      `Invalid_scp_options
      (get tbl (connection |-- key "scp" |-- table |-- key "options" |-- array |-- strings))
  in
  let failure_strategy_res =
    result_of_option
      `Invalid_failure_strategy
      (compute_failure_strategy tbl)
  in
  let open CCResult in
  pure create
  <*> ssh_cmd_res
  <*> ssh_options_res
  <*> scp_cmd_res
  <*> scp_options_res
  <*> failure_strategy_res

let ssh_cmd t = t.ssh_cmd
let ssh_options t = t.ssh_options

let scp_cmd t = t.scp_cmd
let scp_options t = t.scp_options

let failure_strategy t = t.failure_strategy

