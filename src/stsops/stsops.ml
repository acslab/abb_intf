module List = ListLabels

module Assemblage = Stsops_assemblage
module Config = Stsops_config

module Cmdline = struct
  module C = Cmdliner

  let assemblage =
    let doc = "Assemblage to execute." in
    C.Arg.(required & opt (some file) None & info ["a"; "assemblage"] ~docv:"FILE" ~doc)

  let config =
    let doc = "Configuration file." in
    C.Arg.(required & opt (some file) None & info ["c"; "config"] ~docv:"FILE" ~doc)

  let parallel =
    let doc = "How many hosts to apply in parallel." in
    C.Arg.(value & opt int 1 & info ["p"; "parallel"] ~docv:"INT" ~doc)

  let include_paths =
    let doc =
      ("Include path to directories that have the stsops directory layout. " ^
       "This can be included used multiple times and directories are searched " ^
       "in their order on the command line.  The current working directory is " ^
       "always at the beginning of the search path")
    in
    C.Arg.(value & opt_all dir [] & info ["I"; "include"] ~docv:"DIR" ~doc)
end

type apply_template_err =
  [ `Template_error of (string * [ Snabela.Template.err
                                 | Snabela.err
                                 | `Missing_asset of string
                                 ])]

exception File_not_found of string
exception Apply_template_error of apply_template_err

let load_assemblage fname =
  match Toml.Parser.from_filename fname with
    | `Ok table ->
      Assemblage.of_toml_table table
    | `Error err ->
      Error (`Parse_error err)

let load_config fname =
  match Toml.Parser.from_filename fname with
    | `Ok table ->
      Config.of_toml_table table
    | `Error err ->
      Error (`Parse_error err)

let process_run cmd args =
  Logs.debug (fun m -> m "Running: %s" (String.concat " " (cmd::args)));
  let res = Process.run cmd (Array.of_list args) in
  match res.Process.Output.exit_status with
    | Process.Exit.Exit 0 ->
      Ok res
    | _ ->
      Error res

let ssh config host cmd =
  let ssh_cmd = Config.ssh_cmd config in
  let ssh_options = Config.ssh_options config in
  process_run ssh_cmd (ssh_options @ (host::cmd))

let compute_hosts (cmd, args) =
  match process_run cmd args with
    | Ok res ->
      Ok (List.filter (fun h -> h <> "") res.Process.Output.stdout)
    | Error _ ->
      Error `Host_generator_failed

(* Returns the director in [include_paths] that the file is found in. *)
let find_file_dir name include_paths =
  let f path = not (Sys.file_exists (Filename.concat path name)) in
  match CCListLabels.drop_while ~f include_paths with
    | [] -> None
    | d::_ -> Some d

let link_files include_paths typ dst_base files =
  try
    List.iter
      ~f:(fun file ->
          let typ_file = Filename.concat typ file in
          match find_file_dir typ_file include_paths with
            | Some d ->
              Unix.symlink
                (Filename.concat d typ_file)
                (Filename.concat dst_base file)
            | None ->
              raise (File_not_found file))
      files;
    Ok ()
  with
    | File_not_found file -> Error (`File_not_found file)
    | exn -> Error (`Exn exn)

let link_actions dst_path assemblage include_paths =
  link_files
    include_paths
    "actions"
    dst_path
    (Assemblage.actions_run assemblage)

let link_assets dst_path assemblage include_paths =
  link_files
    include_paths
    "assets"
    dst_path
    (Assemblage.assets_upload assemblage)

let rec write_variables_to_db' path map =
  Assemblage.Variable_map.iter
    (fun k v ->
       match v with
         | `Value s ->
           CCIO.with_out
             (Filename.concat path k)
             (fun out -> CCIO.write_line out s)
         | `Map m ->
           let path = Filename.concat path k in
           Unix.mkdir path 0o700;
           write_variables_to_db' path m)
    map

let write_variables_to_db path assemblage =
  try
    write_variables_to_db' path (Assemblage.variables assemblage);
    Ok ()
  with
    | exn -> Error (`Exn exn)

let write_hosts_variable path hosts =
  try
    CCIO.with_out
      (Filename.concat path (Filename.concat "stsops" "hosts"))
      (fun out -> CCIO.write_lines_l out hosts);
    Ok ()
  with
    | exn -> Error (`Exn exn)

let write_output host stdout stderr =
  List.iter
    ~f:(fun line -> Logs.info (fun m -> m "%s:stdout: %s" host line))
    stdout;
  List.iter
    ~f:(fun line -> Logs.info (fun m -> m "%s:stderr: %s" host line))
    stderr

let rec apply_actions temp_path config host = function
  | [] ->
    Ok ()
  | action::actions ->
    let remote_action =
      Filename.concat
        temp_path
        (Filename.concat "actions" action)
    in
    let db_path = Filename.concat temp_path "db" in
    match ssh config host [remote_action; db_path] with
      | Ok res ->
        write_output host (res.Process.Output.stdout) (res.Process.Output.stderr);
        apply_actions temp_path config host actions
      | Error res ->
        write_output host (res.Process.Output.stdout) (res.Process.Output.stderr);
        Error res

let apply_host path config actions host =
  let scp_cmd = Config.scp_cmd config in
  let scp_options = Config.scp_options config in
  let open CCResult.Infix in
  ssh config host ["mktemp"; "-d"; "/tmp/stsopsXXXXXXXXXXX"]
  >>= fun res ->
  let temp_path = String.concat "" res.Process.Output.stdout in
  let upload =
    ["-r"
    ; Filename.concat path "db"
    ; Filename.concat path "actions"
    ; Filename.concat path "assets"
    ; Printf.sprintf "%s:%s" host temp_path
    ]
  in
  process_run scp_cmd (scp_options @ upload)
  >>= fun _ ->
  let hostname_path =
    Filename.concat
      temp_path
      (Filename.concat
         "db"
         (Filename.concat "stsops" "hostname"))
  in
  ssh config host [Printf.sprintf "echo '%s' > %s" host hostname_path]
  >>= fun _ ->
  let assets_path =
    Filename.concat
      temp_path
      (Filename.concat
         "db"
         (Filename.concat "stsops" "assets"))
  in
  let assets_path_on_system = Filename.concat temp_path "assets" in
  ssh config host [Printf.sprintf "echo '%s' > %s" assets_path_on_system assets_path]
  >>= fun _ ->
  apply_actions temp_path config host actions
  >>= fun () ->
  ssh config host ["rm"; "-rf"; temp_path]
  >>= fun _ ->
  Ok ()

let rec apply_assemblage path hosts config actions =
  match hosts with
    | [] ->
      Ok ()
    | h::hs ->
      let open CCResult.Infix in
      match apply_host path config actions h with
        | Ok () ->
          apply_assemblage path hs config actions
        | Error res ->
          Error (`Host_failed (h, res))

let apply_template str kv =
  let open CCResult.Infix in
  Snabela.Template.of_utf8_string str
  >>= fun template ->
  let compiled = Snabela.of_template template [] in
  Snabela.apply compiled kv

let apply_templates assets_path assemblage assets_path include_paths =
  let templates = Stsops_assemblage.templates assemblage in
  try
    Ok
      (Stsops_assemblage.Template_map.iter
         (fun key tmpl ->
            Logs.debug (fun m -> m "Applying template %s" key);
            let module T = Stsops_assemblage.Template in
            let asset_name = Filename.concat "assets" tmpl.T.asset in
            match find_file_dir asset_name include_paths with
              | Some d ->
                let asset_path = Filename.concat d asset_name in
                let template_str = CCIO.with_in asset_path CCIO.read_all in
                begin match apply_template template_str tmpl.T.kv with
                  | Ok output ->
                    let output_path = Filename.concat assets_path tmpl.T.output_name in
                    CCIO.with_out output_path (fun os -> output_string os output)
                  | Error err ->
                    raise (Apply_template_error (`Template_error (key, err)))
                end
              | None ->
                raise (Apply_template_error (`Template_error (key, (`Missing_asset tmpl.T.asset))))
         )
         templates)
  with
    | Apply_template_error err ->
      Error (err : apply_template_err :> [> apply_template_err ])

let run_assemblage assemblage config parallel include_paths =
  let open CCResult.Infix in
  compute_hosts (Assemblage.hosts_generator assemblage)
  >>= fun hosts ->
  Logs.debug (fun m -> m "Running against %d hosts" (List.length hosts));
  (* Stupid hack but there isn't a way to make temporary directories in the std
     lib so I create a file, unlink it, then mkdir.  Not many guarantees here
     but I'm not sure [temp_file] has very strong ones anyways. *)
  let temp_name = Filename.temp_file "stsops" "" in
  Unix.unlink temp_name;
  Unix.mkdir temp_name 0o700;
  Logs.info (fun m -> m "Temporary directory: %s" temp_name);
  let db_path = Filename.concat temp_name "db" in
  let actions_path = Filename.concat temp_name "actions" in
  let assets_path = Filename.concat temp_name "assets" in
  let hosts_script_path = Filename.concat temp_name "hosts" in
  Unix.mkdir db_path 0o700;
  Unix.mkdir actions_path 0o700;
  Unix.mkdir assets_path 0o700;
  Unix.mkdir hosts_script_path 0o700;
  Unix.mkdir (Filename.concat db_path "stsops") 0o700;
  Logs.debug (fun m -> m "Linking actions");
  link_actions actions_path assemblage include_paths
  >>= fun () ->
  Logs.debug (fun m -> m "Linking assets");
  link_assets assets_path assemblage include_paths
  >>= fun () ->
  Logs.debug (fun m -> m "Writing variables to database");
  write_variables_to_db db_path assemblage
  >>= fun () ->
  Logs.debug (fun m -> m "Applying templates");
  apply_templates assets_path assemblage assets_path include_paths
  >>= fun () ->
  write_hosts_variable db_path hosts
  >>= fun () ->
  Logs.info (fun m -> m "Applying assemblage to hosts");
  apply_assemblage temp_name hosts config (Assemblage.actions_run assemblage)
  >>= fun () ->
  Logs.debug (fun m -> m "Cleaning up temporary directory");
  ignore (Process.run ~exit_status:[0] "rm" [|"-rf"; temp_name|]);
  Ok ()

let assemble assemblage_fname config_fname parallel include_paths =
  let open CCResult in
  match
    pure run_assemblage
    <*> load_assemblage assemblage_fname
    <*> load_config config_fname
    <*> pure parallel
    <*> pure (Sys.getcwd () :: include_paths)
  with
    | Ok (Ok _) ->
      ()
    | Ok (Error `Host_generator_failed) ->
      Logs.err (fun m -> m "Host generation failed");
      exit 1
    | Ok (Error (`Host_failed (host, res))) ->
      Logs.err (fun m -> m "Host failed: %s" host);
      write_output host res.Process.Output.stdout res.Process.Output.stderr;
      exit 1
    | Ok (Error (`Exn exn)) ->
      Logs.err (fun m -> m "Failed with exception: %s" (Printexc.to_string exn));
      Logs.err (fun m -> m "Backtrace: %s" (Printexc.get_backtrace ()));
      exit 1
    | Ok (Error (`File_not_found file)) ->
      Logs.err (fun m -> m "Unable to find file %s while linking" file);
      exit 1
    | Ok (Error (`Template_error (tmpl, err))) ->
      let sprintf = Printf.sprintf in
      let error_str =
        match err with
          | `Exn exn -> sprintf "Unexpected exception %s" (Printexc.to_string exn)
          | `Expected_boolean (key, ln) -> sprintf "Expected bool in section %s on line %d" key ln
          | `Expected_list (key, ln) -> sprintf "Expected a list in section %s on line %d" key ln
          | `Invalid_replacement ln -> sprintf "Invalid replacement on line %d" ln
          | `Invalid_transformer ln -> sprintf "Invalid transformer section on line %d" ln
          | `Missing_asset asset -> sprintf "Could not find asset %s" asset
          | `Missing_closing_section section -> sprintf "Missing close for section %s" section
          | `Missing_key (key, ln) -> sprintf "Missing key %s on line %d" key ln
          | `Missing_transformer (tr, ln) -> sprintf "Missing transformer %s on line %d" tr ln
          | `Non_scalar_key (key, ln) -> sprintf "Non-scalar key %s on line %d" key ln
          | `Premature_eof -> "Premature eof"
      in
      Logs.err (fun m -> m "Failed to apply template '%s' with error: %s" tmpl error_str);
      exit 1
    | Error `Invalid_failure_strategy
    | Error `Invalid_scp_cmd
    | Error `Invalid_scp_options
    | Error `Invalid_ssh_cmd
    | Error `Invalid_ssh_options
    | Error `Invalid_type
    | Error (`Invalid_variable _) ->
      Logs.err (fun m -> m "Unknown error occurred");
      exit 1
    | Error `Missing_actions_run ->
      Logs.err (fun m -> m "Assemblage missing actions to run");
      exit 1
    | Error `Missing_hosts_generator ->
      Logs.err (fun m -> m "Assemblage missing hosts generator");
      exit 1
    | Error (`Parse_error _) ->
      Logs.err (fun m -> m "Failed to parse assemblage");
      exit 1

let cmd =
  let doc = "A simple program to apply actions to hosts." in
  Cmdliner.Term.((const assemble $
                  Cmdline.assemblage $
                  Cmdline.config $
                  Cmdline.parallel $
                  Cmdline.include_paths),
                 info "stsops" ~doc)

let reporter ppf =
  let report src level ~over k msgf =
    let k _ = over (); k () in
    let with_stamp h tags k ppf fmt =
      let time = Unix.gettimeofday () in
      let time_str = ISO8601.Permissive.string_of_datetime time in
      Format.kfprintf
        k
        ppf
        ("[%s]%a @[" ^^ fmt ^^ "@]@.")
        time_str
        Logs.pp_header
        (level, h)
    in
    msgf @@ fun ?header ?tags fmt -> with_stamp header tags k ppf fmt
  in
  { Logs.report = report }

let main () =
  Logs.set_reporter (reporter Format.std_formatter);
  Logs.set_level ~all:true (Some Logs.Debug);
  Random.self_init ();
  match Cmdliner.Term.eval cmd with
    | `Error _ -> exit 1
    | _ -> exit 0

let () = main ()
