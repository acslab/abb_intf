type t

type invalid_toml = [ `Invalid_ssh_cmd
                    | `Invalid_ssh_options
                    | `Invalid_scp_cmd
                    | `Invalid_scp_options
                    | `Invalid_failure_strategy
                    ]

module Failure_strategy : sig
  type t =
    | All_for_one
    | All_for_percent of int
    | All_for_one_with_retry of int
    | Ignore
end

val of_toml_table : TomlTypes.table -> (t, [> invalid_toml ]) result

val ssh_cmd : t -> string
val ssh_options : t -> string list


val scp_cmd : t -> string
val scp_options : t -> string list

val failure_strategy : t -> Failure_strategy.t
