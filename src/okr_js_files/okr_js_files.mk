.PHONY: clean_okr_js_files

# Make it serial because we are adding a NON_LIB_MODULE
.NOTPARALLEL:

NON_LIB_MODULES += okr_js_files.ml

$(SRC_DIR)/okr_js_files.ml: $(wildcard ../../../okr_js_files/*)
	ocaml-crunch -m plain ../../../okr_js_files > $(SRC_DIR)/okr_js_files.ml

clean: clean_okr_js_files

clean_okr_js_files:
	rm $(SRC_DIR)/okr_js_files.ml
