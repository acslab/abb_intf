module Brtl = Brtl.Make(Abb)

module Mw_log = Brtl_mw_log.Make(Abb)

module Add_related = Longform_add_related
module Assets = Longform_assets
module Fetch_title = Longform_fetch_title
module Page = Longform_page
module Related = Longform_related
module Search = Longform_search
module Storage = Longform_storage
module Submit_article = Longform_submit_article
module Submit_form = Longform_submit_form

let default_route ctx =
  Abb.Future.return Brtl.(Ctx.set_response (Rspnc.create ~status:`Not_found "") ctx)

let about_rt () = Brtl.Rtng.Route.(rel / "about")
let assets_rt () = Brtl.Rtng.Route.(rel / "assets" /% Path.string)
let homepage_rt () = Brtl.Rtng.Route.rel
let page_rt () = Brtl.Rtng.Route.(rel / "page" /% Path.int)
let submit_rt () = Brtl.Rtng.Route.(rel / "submit")
let submit_article_rt () = Brtl.Rtng.Route.(rel / "submit-article")
let fetch_title_rt () = Brtl.Rtng.Route.(rel / "v0" / "fetch-title")
let related_rt () = Brtl.Rtng.Route.(rel / "article" /% Path.int / "related")
let add_related_form_rt () = Brtl.Rtng.Route.(rel / "article" /% Path.int / "related" / "add")
let add_related_rt () = Brtl.Rtng.Route.(rel / "article" /% Path.int / "related" / "add" /% Path.int)
let search_rt () = Brtl.Rtng.Route.(rel / "v0" / "search")

let page storage pg = Page.render_homepage storage (Some pg)
let homepage storage = Page.render_homepage storage None
let homepage_search = Page.render_search
let assets = Assets.run
let about = Assets.run "about.html"
let submit = Submit_form.run
let submit_article = Submit_article.run
let fetch_title = Fetch_title.run
let related = Related.run
let add_related_form = Add_related.run
let add_related = Add_related.add
let search = Search.run

let rtng storage =
  Brtl.Rtng.create
    ~default:default_route
    Brtl.Rtng.Route.([ (`GET, homepage_rt () --> homepage storage)
                     ; (`POST, homepage_rt () --> homepage_search storage)
                     ; (`GET, page_rt () --> page storage)
                     ; (`GET, about_rt () --> about)
                     ; (`GET, assets_rt () --> assets)
                     ; (`GET, submit_rt () --> submit storage)
                     ; (`POST, submit_article_rt () --> submit_article storage)
                     ; (`POST, fetch_title_rt () --> fetch_title)
                     ; (`GET, related_rt () --> related storage)
                     ; (`GET, add_related_form_rt () --> add_related_form storage)
                     ; (`GET, add_related_rt () --> add_related storage)
                     ; (`POST, search_rt () --> search storage)
                     ])

let run ~port db_path =
  let one_min = Duration.of_min 1 in
  let run () =
    let open Abb.Future.Infix_monad in
    Storage.create db_path
    >>= fun res ->
    let storage = CCResult.get_exn res in
    let cfg =
      Brtl.Cfg.create
        ~port
        ~read_header_timeout:(Some one_min)
        ~handler_timeout:(Some one_min)
    in
    let mw = Brtl.Mw.create [Mw_log.create ()] in
    Brtl.run cfg mw (rtng storage)
  in
  match Abb.Scheduler.run (Abb.Scheduler.create ()) run with
    | `Det () -> ()
    | _ -> assert false

