module String_set = CCSet.Make(String)

module Fut_comb = Abb_future_combinators.Make(Abb.Future)

module Brtl = Brtl.Make(Abb)

module Storage = Longform_storage
module Article = Longform_storage.Article

let num_articles_per_page = 14

let html_escape = function
  | Brtl.Tmpl.Kv.S str ->
    str
    |> CCString.replace ~which:`All ~sub:"&" ~by:"&amp;"
    |> CCString.replace ~which:`All ~sub:"\"" ~by:"&quot;"
    |> CCString.replace ~which:`All ~sub:"'" ~by:"&#39;"
    |> CCString.replace ~which:`All ~sub:"<" ~by:"&lt;"
    |> CCString.replace ~which:`All ~sub:">" ~by:"&gt;"
    |> fun s -> Brtl.Tmpl.Kv.S s
  | any ->
    any

let page =
  Longform_tmpl.read "page.html"
  |> CCOpt.get_exn
  |> Brtl.Tmpl.Template.of_utf8_string
  |> CCResult.get_exn
  |> CCFun.flip Brtl.Tmpl.of_template [("html", html_escape)]

let calculate_next_start_id articles =
  match CCList.rev articles with
    | a::_ -> Article.id a
    | _ -> failwith "nyi"

let render_page ?search all_articles per_page =
  let articles = CCList.take num_articles_per_page all_articles in
  let template_articles =
    CCListLabels.map
      ~f:(fun e ->
          let ts =
            e
            |> Article.timestamp
            |> Int64.to_float
            |> ISO8601.Permissive.string_of_datetime
          in
          let tags =
            e
            |> Article.tags
            |> String_set.to_list
            |> ListLabels.map ~f:(fun t -> Brtl.Tmpl.Kv.(Map.of_list [ ("tag", string t) ]))
          in
          Brtl.Tmpl.Kv.(Map.of_list [ ("url", string (Uri.to_string (Article.uri e)))
                                    ; ("title", string (Article.title e))
                                    ; ("timestamp", string ts)
                                    ; ("id", int (Int64.to_int (Article.id e)))
                                    ; ("tags", list tags)
                                    ]))
      articles
  in
  let has_next_page = CCList.length all_articles > num_articles_per_page in
  let next_start_id =
    if CCList.length all_articles > num_articles_per_page then
      calculate_next_start_id all_articles
    else
      Int64.zero
  in
  let kv =
    Brtl.Tmpl.Kv.(Map.of_list
                    (List.concat
                       [ [ ("articles", list template_articles)
                         ; ("has_next_page", bool has_next_page)
                         ; ("next_start_id", int (Int64.to_int next_start_id))
                         ]
                       ; match search with
                         | Some v -> [("was_search", bool true); ("search_text", string v)]
                         | None -> [("was_search", bool false)]
                       ]))
  in
  Brtl.Tmpl.apply page kv

let rspnc_of_template_apply = function
  | Ok str ->
    Brtl.Rspnc.create ~status:`OK str
  | Error (#Brtl.Tmpl.err as err) ->
    let err_str = Brtl.Tmpl.show_err err in
    let headers = Cohttp.Header.of_list [("content-type", "text/plain")] in
    Brtl.Rspnc.create ~headers ~status:`Internal_server_error err_str
  | Error #Storage.err ->
    Brtl.Rspnc.create ~status:`Internal_server_error ""

let render_articles storage article_id =
  let open Fut_comb.Infix_result_monad in
  let start_id = CCOpt.map Int64.of_int article_id in
  Storage.articles_by_post_order
    storage
    ?start:start_id
    (num_articles_per_page + 1)
  >>= fun all_articles ->
  Abb.Future.return (render_page all_articles num_articles_per_page)

let render_homepage storage article_id ctx =
  let open Abb.Future.Infix_monad in
  render_articles storage article_id
  >>| fun result ->
  Brtl.Ctx.set_response (rspnc_of_template_apply result) ctx

let render_search storage ctx =
  let open Abb.Future.Infix_monad in
  let data = Uri.of_string ("?" ^ Brtl.Ctx.body ctx) in
  let query = CCOpt.get_or ~default:"" (Uri.get_query_param data "query") in
  let words = CCString.Split.list_cpy ~by:" " query in
  Storage.search storage words
  >>| function
  | Ok articles ->
    Brtl.Ctx.set_response
      (rspnc_of_template_apply (render_page ~search:query articles (List.length articles)))
      ctx
  | Error _ ->
    Brtl.(Ctx.set_response (Rspnc.create ~status:`Internal_server_error "") ctx)

