module String_set = CCSet.Make(String)

module Fut_comb = Abb_future_combinators.Make(Abb.Future)

module Brtl = Brtl.Make(Abb)

module Storage = Longform_storage
module Article = Longform_storage.Article

let run storage ctx =
  (* TODO: This is a cheap hack that is probably wrong.  Pretending the post
     is the same as URL query parameters *)
  let data = Uri.of_string ("?" ^ Brtl.Ctx.body ctx) in
  match
    CCOpt.((fun url title -> (url, title))
           <$> Uri.get_query_param data "article_url"
           <*> Uri.get_query_param data "article_title")
  with
    | Some (url, title) when String.length url > 0 && String.length title > 0 ->
      let open Abb.Future.Infix_monad in
      let tags =
        data
        |> Uri.query
        |> CCListLabels.filter ~f:(fun (n, v) -> n = "article_tags")
        |> CCListLabels.map ~f:snd
        |> CCList.concat
        |> String_set.of_list
      in
      let article = Article.create ~uri:(Uri.of_string url) ~title ~tags in
      Storage.save_article
        storage
        article
      >>| fun _ ->
      (* TODO: Handle failure *)
      let headers = Cohttp.Header.of_list [("location", "/")] in
      Brtl.(Ctx.set_response (Rspnc.create ~headers ~status:`See_other "") ctx)
    | _ ->
      failwith "nyi"

