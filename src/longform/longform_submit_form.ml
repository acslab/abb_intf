module String_set = CCSet.Make(String)

module Fut_comb = Abb_future_combinators.Make(Abb.Future)

module Brtl = Brtl.Make(Abb)

module Storage = Longform_storage

let page =
  Longform_tmpl.read "submit.html"
  |> CCOpt.get_exn
  |> Brtl.Tmpl.Template.of_utf8_string
  |> CCResult.get_exn
  |> CCFun.flip Brtl.Tmpl.of_template []

let render_page storage =
  let open Fut_comb.Infix_result_monad in
  Storage.tags storage
  >>= fun tags ->
  let tags =
    ListLabels.map
      ~f:(fun t -> Brtl.Tmpl.Kv.(Map.of_list [("tag", string t)]))
      (String_set.to_list tags)
  in
  let kv = Brtl.Tmpl.Kv.(Map.of_list [("tags", list tags)]) in
  Abb.Future.return (Brtl.Tmpl.apply page kv)

let run storage ctx =
  let open Abb.Future.Infix_monad in
  render_page storage
  >>| function
  | Ok str ->
    Brtl.(Ctx.set_response (Rspnc.create ~status:`OK str) ctx)
  | Error (#Brtl.Tmpl.err as err) ->
    let err_str = Brtl.Tmpl.show_err err in
    let headers = Cohttp.Header.of_list [("content-type", "text/plain")] in
    Brtl.(Ctx.set_response (Rspnc.create ~headers ~status:`Internal_server_error err_str) ctx)
  | Error #Storage.err ->
    Brtl.(Ctx.set_response (Rspnc.create ~status:`Internal_server_error "") ctx)
