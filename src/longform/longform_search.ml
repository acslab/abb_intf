module Fut_comb = Abb_future_combinators.Make(Abb.Future)

module Brtl = Brtl.Make(Abb)

module Article = Longform_storage.Article
module Storage = Longform_storage
module Search_responce = Longform_data.Search_responce

let rspnc_of_articles articles =
  let responce =
    Search_responce.({ data =
                         ListLabels.map
                           ~f:(fun art ->
                               { id = Int64.to_int (Article.id art)
                               ; uri = Uri.to_string (Article.uri art)
                               ; title = Article.title art
                               })
                           articles
                     })
  in
  Abb.Future.return
    (Brtl.Rspnc.create
       ~status:`OK
       (Yojson.Safe.to_string (Search_responce.to_yojson responce)))

let exec_search storage search =
  let open Abb.Future.Infix_monad in
  let words = CCString.Split.list_cpy ~by:" " search in
  Storage.search storage words
  >>= function
  | Ok articles ->
    rspnc_of_articles articles
  | Error _ ->
    Abb.Future.return (Brtl.Rspnc.create ~status:`Internal_server_error "")

let run storage ctx =
  (* TODO: This is a cheap hack that is probably wrong.  Pretending the post
     is the same as URL query parameters *)
  let data = Uri.of_string ("?" ^ Brtl.Ctx.body ctx) in
  match Uri.get_query_param data "query" with
    | Some search ->
      let open Abb.Future.Infix_monad in
      exec_search storage search
      >>| fun rspnc ->
      Brtl.Ctx.set_response rspnc ctx
    | None ->
      failwith "nyi"

