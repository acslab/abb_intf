(* TODO: Normalize article id type *)

module Article : sig
  type stored
  type unstored

  type 'a t

  val create : uri:Uri.t -> title:string -> tags:CCSet.Make(String).t -> unstored t

  val id : stored t -> Int64.t
  val uri : 'a t -> Uri.t
  val title : 'a t -> string
  val tags : 'a t -> CCSet.Make(String).t
  val timestamp : stored t -> Int64.t
end

type t

type err = [ `Sql_error of Sqlite3.Rc.t | `Closed ]

val create : string -> (t, [> err ]) result Abb.Future.t
val destroy : t -> (unit, [> err ]) result Abb.Future.t

val create_tables : t -> (unit, [> err ]) result Abb.Future.t

val articles_by_post_order :
  t ->
  ?start:Int64.t ->
  int ->
  (Article.stored Article.t list, [> err ]) result Abb.Future.t

val article_by_url :
  t ->
  Uri.t ->
  (Article.stored Article.t option, [> err ]) result Abb.Future.t

val save_article :
  t ->
  Article.unstored Article.t ->
  (unit, [> err ]) result Abb.Future.t

val tags : t -> (CCSet.Make(String).t, [> err ]) result Abb.Future.t

val add_article_related : t -> int -> int -> (unit, [> err ]) result Abb.Future.t

val related_articles :
  t ->
  id:int ->
  int ->
  (Article.stored Article.t list, [> err ]) result Abb.Future.t

val search : t -> string list -> (Article.stored Article.t list, [> err ]) result Abb.Future.t
