module Fut_comb = Abb_future_combinators.Make(Abb.Future)

module Server = Longform_server
module Storage = Longform_storage

module Cmdline = struct
  module C = Cmdliner

  let port =
    let doc = "Port to run server on." in
    C.Arg.(value & opt int 8080 & info ["p"; "port"] ~doc)

  let db_path =
    let doc = "Path to database directory." in
    C.Arg.(required & opt (some dir) None & info ["db"] ~doc)

  let database_create_cmd f =
    let doc = "Create the database" in
    let exits = C.Term.default_exits in
    C.Term.(const f $ db_path, info "database-create" ~doc ~exits)

  let server_cmd f =
    let doc = "Run an Longform server." in
    let exits = C.Term.default_exits in
    C.Term.(const f $ port $ db_path, info "server" ~doc ~exits)

  let default_cmd =
    let doc = "Longform." in
    let exits = C.Term.default_exits in
    C.Term.(ret (const (fun _ -> (`Help (`Pager, None))) $ port),
            info "longform" ~doc ~exits)
end

let database_create db_path =
  let create () =
    let open Fut_comb.Infix_result_monad in
    Storage.create db_path
    >>= fun db ->
    Fut_comb.with_finally
      (fun () -> Storage.create_tables db)
      ~finally:(fun () -> Fut_comb.ignore (Storage.destroy db))
  in
  let run () =
    let open Abb.Future.Infix_monad in
    create ()
    >>= fun _ ->
    Abb.Future.return ()
  in
  match Abb.Scheduler.run (Abb.Scheduler.create ()) run with
    | `Det () -> ()
    | _ -> assert false

let server port db_path =
  Logs.set_reporter (Logs_fmt.reporter ());
  Logs.set_level ~all:true (Some Logs.Debug);
  Server.run ~port db_path

let cmds = Cmdline.([database_create_cmd database_create; server_cmd server])

let () = Cmdliner.Term.(exit @@ eval_choice Cmdline.default_cmd cmds)
