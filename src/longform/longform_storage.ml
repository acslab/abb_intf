module Int_map = CCMap.Make(CCInt)
module String_set = CCSet.Make(String)

module Channel = Abb_channel.Make(Abb.Future)
module Fut_comb = Abb_future_combinators.Make(Abb.Future)
module Serializer = Abb_service_serializer.Make(Abb.Future)


module Article = struct
  type stored = { id : Int64.t; timestamp : Int64.t }
  type unstored = unit

  type 'a t = { stored : 'a
              ; uri : Uri.t
              ; title : string
              ; tags : String_set.t
              }

  let create ~uri ~title ~tags =
    { stored = (); uri; title; tags }

  let id t = t.stored.id
  let uri t = t.uri
  let title t = t.title
  let tags t = t.tags
  let timestamp t = t.stored.timestamp
end


type t = { serializer : Serializer.t
         ; db : Sqlite3.db
         }

type err = [ `Sql_error of Sqlite3.Rc.t | `Closed ]

module Sql = struct
  let foreign_key_pragma = "PRAGMA foreign_keys = ON;"

  let create_articles =
    "CREATE TABLE articles " ^
    "(id INTEGER PRIMARY KEY AUTOINCREMENT, " ^
    "url STRING NOT NULL, " ^
    "title TEXT NOT NULL, " ^
    "timestamp TEXT DEFAULT (datetime('now')));"

  let create_related =
    "CREATE TABLE related " ^
    "(id1 INTEGER, " ^
    "id2 INTEGER, " ^
    "FOREIGN KEY(id1) REFERENCES articles(id), " ^
    "FOREIGN KEY(id2) REFERENCES articles(id), " ^
    "PRIMARY KEY (id1, id2));"

  let create_tags =
    "CREATE TABLE tags(tag TEXT PRIMARY KEY, meaning TEXT);"

  let create_article_tags =
    "CREATE TABLE article_tags " ^
    "(id INTEGER, tag TEXT, " ^
    "FOREIGN KEY(id) REFERENCES articles(id), " ^
    "FOREIGN KEY(tag) REFERENCES tags(tag), " ^
    "PRIMARY KEY (id, tag));"

  let articles_by_post_order =
    "SELECT id, url, title, strftime('%s', timestamp) FROM articles ORDER BY timestamp DESC LIMIT ?"

  let articles_by_post_order_start =
    "SELECT id, url, title, strftime('%s', timestamp) FROM articles WHERE id <= ? ORDER BY timestamp DESC LIMIT ?"

  let tags_by_post_order =
    "SELECT articles.id, article_tags.tag FROM articles INNER JOIN article_tags ON articles.id = article_tags.id ORDER BY articles.timestamp DESC LIMIT ?"

  let tags_by_post_order_start =
    "SELECT articles.id, article_tags.tag FROM articles INNER JOIN article_tags ON articles.id = article_tags.id WHERE articles.id <= ? ORDER BY articles.timestamp DESC LIMIT ?"

  let tags_by_url =
    "SELECT articles.id, article_tags.tag FROM articles INNER JOIN article_tags ON articles.id = article_tags.id WHERE url = ?;"

  let insert_article =
    "INSERT INTO articles (url, title) VALUES(?, ?);"

  let insert_article_tag =
    "INSERT INTO article_tags VALUES(?, ?);"

  let get_article_by_url =
    "SELECT id, url, title, strftime('%s', timestamp) FROM articles WHERE url = ?;"

  let get_tags =
    "SELECT tag FROM tags;"

  let add_article_related =
    "INSERT INTO related VALUES(?, ?);"

  let related_articles_by_id =
    "SELECT id, url, title, strftime('%s', timestamp) FROM articles, related WHERE related.id1 = ? AND articles.id = related.id2 OR related.id2 = ? AND articles.id = related.id1 ORDER BY timestamp DESC LIMIT ?;"

  let tags_by_related =
    "SELECT article_tags.id, article_tags.tag FROM article_tags, related INNER JOIN articles ON article_tags.id = articles.id WHERE (related.id1 = ? AND related.id2 = article_tags.id OR related.id2 = ? AND related.id1 = article_tags.id) ORDER BY articles.timestamp DESC LIMIT ?;"

  let search_article_head =
    "SELECT articles.id, url, title, strftime('%s', timestamp) FROM articles " ^
    "INNER JOIN article_tags ON articles.id = article_tags.id"

  let search_article_tail =
    "GROUP BY articles.id, url, title, strftime('%s', timestamp) ORDER BY timestamp DESC"

  let search_tag_head =
    "SELECT articles.id, article_tags.tag FROM articles INNER JOIN article_tags ON articles.id = article_tags.id"

  let int = function
    | Sqlite3.Data.INT i -> i
    | _ -> assert false

  let float = function
    | Sqlite3.Data.FLOAT f -> f
    | _ -> assert false

  let text = function
    | Sqlite3.Data.TEXT t -> t
    | _ -> assert false

  let column stmt idx f = f (Sqlite3.column stmt idx)
end

let exec db sql =
  let open Sqlite3.Rc in
  match Sqlite3.exec db sql with
    | OK
    | DONE->
      Ok ()
    | ERROR
    | INTERNAL
    | PERM
    | ABORT
    | BUSY
    | LOCKED
    | NOMEM
    | READONLY
    | INTERRUPT
    | IOERR
    | CORRUPT
    | NOTFOUND
    | FULL
    | CANTOPEN
    | PROTOCOL
    | EMPTY
    | SCHEMA
    | TOOBIG
    | CONSTRAINT
    | MISMATCH
    | MISUSE
    | NOFLS
    | AUTH
    | FORMAT
    | RANGE
    | NOTADB
    | ROW
    | UNKNOWN _ as err ->
      Error (`Sql_error err)

let step stmt =
  let open Sqlite3.Rc in
  match Sqlite3.step stmt with
    | OK
    | DONE
    | ROW as ret ->
      Ok ret
    | ERROR
    | INTERNAL
    | PERM
    | ABORT
    | BUSY
    | LOCKED
    | NOMEM
    | READONLY
    | INTERRUPT
    | IOERR
    | CORRUPT
    | NOTFOUND
    | FULL
    | CANTOPEN
    | PROTOCOL
    | EMPTY
    | SCHEMA
    | TOOBIG
    | CONSTRAINT
    | MISMATCH
    | MISUSE
    | NOFLS
    | AUTH
    | FORMAT
    | RANGE
    | NOTADB
    | UNKNOWN _ as err ->
      Error (`Sql_error err)

let map ~f stmt =
  let rec map' f stmt =
    match step stmt with
      | Ok Sqlite3.Rc.ROW ->
        let v = f stmt in
        v::map' f stmt
      | Ok (Sqlite3.Rc.OK)
      | Ok (Sqlite3.Rc.DONE) ->
        []
      | _ ->
        assert false
  in
  Ok (map' f stmt)

let with_stmt stmt ~f =
  let res = f stmt in
  ignore (Sqlite3.finalize stmt);
  res

let with_sql db sql ~f =
  let stmt = Sqlite3.prepare db sql in
  with_stmt stmt ~f

let create db_path =
  let db_fname = Filename.concat db_path "longform.db" in
  let open Fut_comb.Infix_result_monad in
  Fut_comb.to_result (Serializer.create ())
  >>= fun serializer ->
  let db = Sqlite3.db_open db_fname in
  Abb.Future.return (exec db Sql.foreign_key_pragma)
  >>= fun () ->
  Abb.Future.return (Ok { serializer; db })

let destroy t =
  let open Abb.Future.Infix_monad in
  ignore (Sqlite3.db_close t.db);
  Channel.close t.serializer
  >>= fun () ->
  Abb.Future.return (Ok ())

let create_tables t =
  let open Abb.Future.Infix_monad in
  Serializer.run
    t.serializer
    ~f:(fun () ->
        let open CCResult.Infix in
        Abb.Future.return
          (exec t.db Sql.create_articles
           >>= fun () ->
           exec t.db Sql.create_related
           >>= fun () ->
           exec t.db Sql.create_tags
           >>= fun () ->
           exec t.db Sql.create_article_tags))
  >>= function
  | `Ok res -> Abb.Future.return res
  | `Closed -> Abb.Future.return (Error `Closed)

let tags_by_stmt stmt =
  with_stmt
    stmt
    ~f:(fun stmt ->
        let f stmt =
          let id = Int64.to_int Sql.(column stmt 0 int) in
          let tag = Sql.(column stmt 1 text) in
          (id, tag)
        in
        match map ~f stmt with
          | Ok tags ->
            ListLabels.fold_left
              ~f:(fun m (id, tag) ->
                  match Int_map.get id m with
                    | Some tags -> Int_map.add id (String_set.add tag tags) m
                    | None -> Int_map.add id (String_set.singleton tag) m)
              ~init:Int_map.empty
              tags
          | Error _ ->
            failwith "nyi")

let tags_by_post_order t ?start limit =
  let stmt =
    match start with
      | Some start_id ->
        let stmt = Sqlite3.prepare t.db Sql.tags_by_post_order_start in
        ignore (Sqlite3.bind stmt 1 (Sqlite3.Data.INT start_id));
        ignore (Sqlite3.bind stmt 2 (Sqlite3.Data.INT (Int64.of_int limit)));
        stmt
      | None ->
        let stmt = Sqlite3.prepare t.db Sql.tags_by_post_order in
        ignore (Sqlite3.bind stmt 1 (Sqlite3.Data.INT (Int64.of_int limit)));
        stmt
  in
  tags_by_stmt stmt

let tags_by_url t url =
  let stmt = Sqlite3.prepare t.db Sql.tags_by_url in
  ignore (Sqlite3.bind stmt 1 (Sqlite3.Data.TEXT url));
  tags_by_stmt stmt

let tags_by_related t id limit =
  let stmt = Sqlite3.prepare t.db Sql.tags_by_related in
  ignore (Sqlite3.bind stmt 1 (Sqlite3.Data.INT (Int64.of_int id)));
  ignore (Sqlite3.bind stmt 2 (Sqlite3.Data.INT (Int64.of_int id)));
  ignore (Sqlite3.bind stmt 3 (Sqlite3.Data.INT (Int64.of_int limit)));
  tags_by_stmt stmt

let article_of_stmt tag_map stmt =
  let article_id = Sql.(column stmt 0 int) in
  (* TODO: Fix whatever weirdness happens in sqlite to cause this *)
  let ts = Int64.of_string Sql.(column stmt 3 text) in
  Article.({ stored = { id = article_id
                      ; timestamp = ts
                      }
           ; uri = Uri.of_string Sql.(column stmt 1 text)
           ; title = Sql.(column stmt 2 text)
           ; tags = Int_map.get_or
                 ~default:String_set.empty
                 (Int64.to_int article_id)
                 tag_map
           })

let articles_by_post_order t ?start limit =
  let open Abb.Future.Infix_monad in
  let stmt =
    match start with
      | Some start_id ->
        let stmt = Sqlite3.prepare t.db Sql.articles_by_post_order_start in
        ignore (Sqlite3.bind stmt 1 (Sqlite3.Data.INT start_id));
        ignore (Sqlite3.bind stmt 2 (Sqlite3.Data.INT (Int64.of_int limit)));
        stmt
      | None ->
        let stmt = Sqlite3.prepare t.db Sql.articles_by_post_order in
        ignore (Sqlite3.bind stmt 1 (Sqlite3.Data.INT (Int64.of_int limit)));
        stmt
  in
  let f () =
    (* Limit the size of the tag map and assume at most 10 tags per article *)
    let tag_map = tags_by_post_order t ?start (limit * 10) in
    with_stmt
      stmt
      ~f:(fun stmt ->
          let articles = map ~f:(article_of_stmt tag_map) stmt in
          Abb.Future.return articles)
  in
  Serializer.run
    t.serializer
    ~f
  >>= function
  | `Ok res -> Abb.Future.return res
  | `Closed -> Abb.Future.return (Error `Closed)

let add_tags_to_article db id tags =
  with_sql
    db
    Sql.insert_article_tag
    ~f:(fun stmt ->
        ListLabels.iter
          ~f:(fun tag ->
              ignore (Sqlite3.reset stmt);
              ignore (Sqlite3.bind stmt 1 (Sqlite3.Data.INT id));
              ignore (Sqlite3.bind stmt 2 (Sqlite3.Data.TEXT tag));
              ignore (step stmt))
          tags)

let add_tags db url tags =
  with_sql
    db
    Sql.get_article_by_url
    ~f:(fun stmt ->
        let open CCResult.Infix in
        ignore (Sqlite3.bind stmt 1 (Sqlite3.Data.TEXT url));
        step stmt
        >>= function
        | Sqlite3.Rc.ROW ->
          let id = Sql.(column stmt 0 int) in
          add_tags_to_article db id (String_set.to_list tags);
          Ok ()
        | _ ->
          failwith "nyi")

let article_by_url t uri =
  let open Abb.Future.Infix_monad in
  let uri = Uri.to_string uri in
  let f () =
    let tag_map = tags_by_url t uri in
    with_sql
      t.db
      Sql.get_article_by_url
      ~f:(fun stmt ->
          ignore (Sqlite3.bind stmt 1 (Sqlite3.Data.TEXT uri));
          match step stmt with
            | Ok Sqlite3.Rc.ROW ->
              Abb.Future.return (Ok (Some (article_of_stmt tag_map stmt)))
            | _ ->
              Abb.Future.return (Ok None))
  in
  Serializer.run
    t.serializer
    ~f
  >>= function
  | `Ok res -> Abb.Future.return res
  | `Closed -> Abb.Future.return (Error `Closed)

let save_article t article =
  let open Abb.Future.Infix_monad in
  let f () =
    let res =
      with_sql
        t.db
        Sql.insert_article
        ~f:(fun stmt ->
            ignore (Sqlite3.bind stmt 1 (Sqlite3.Data.TEXT (Uri.to_string (Article.uri article))));
            ignore (Sqlite3.bind stmt 2 (Sqlite3.Data.TEXT (Article.title article)));
            let open CCResult.Infix in
            step stmt
            >>= fun _ ->
            add_tags t.db (Uri.to_string (Article.uri article)) (Article.tags article))
    in
    Abb.Future.return res
  in
  Serializer.run
    t.serializer
    ~f
  >>= function
  | `Ok res -> Abb.Future.return res
  | `Closed -> Abb.Future.return (Error `Closed)

let tags t =
  let open Abb.Future.Infix_monad in
  let f () =
    with_sql
      t.db
      Sql.get_tags
      ~f:(fun stmt ->
          let f stmt = Sql.(column stmt 0 text) in
          match map ~f stmt with
            | Ok tags ->
              Abb.Future.return (Ok (String_set.of_list tags))
            | Error _ as err ->
              Abb.Future.return err)
  in
  Serializer.run
    t.serializer
    ~f
  >>= function
  | `Ok res -> Abb.Future.return res
  | `Closed -> Abb.Future.return (Error `Closed)

let add_article_related t article1 article2 =
  let open Abb.Future.Infix_monad in
  let (a1, a2) =
    match (article1, article2) with
      | (a1, a2) as articles when a1 < a2 -> articles
      | (a2, a1) -> (a1, a2)
  in
  let f () =
    with_sql
      t.db
      Sql.add_article_related
      ~f:(fun stmt ->
          ignore (Sqlite3.bind stmt 1 (Sqlite3.Data.INT (Int64.of_int a1)));
          ignore (Sqlite3.bind stmt 2 (Sqlite3.Data.INT (Int64.of_int a2)));
          match step stmt with
            | Ok _ -> Abb.Future.return (Ok ())
            | Error _ as err -> Abb.Future.return err)
  in
  Serializer.run
    t.serializer
    ~f
  >>= function
  | `Ok res -> Abb.Future.return res
  | `Closed -> Abb.Future.return (Error `Closed)

let related_articles t ~id limit =
  let open Abb.Future.Infix_monad in
  let f () =
    (* Limit the size of the tag map and assume at most 10 tags per article *)
    let tag_map = tags_by_related t id (limit * 10) in
    with_sql
      t.db
      Sql.related_articles_by_id
      ~f:(fun stmt ->
          ignore (Sqlite3.bind stmt 1 (Sqlite3.Data.INT (Int64.of_int id)));
          ignore (Sqlite3.bind stmt 2 (Sqlite3.Data.INT (Int64.of_int id)));
          ignore (Sqlite3.bind stmt 3 (Sqlite3.Data.INT (Int64.of_int limit)));
          let articles = map ~f:(article_of_stmt tag_map) stmt in
          Abb.Future.return articles)
  in
  Serializer.run
    t.serializer
    ~f
  >>= function
  | `Ok res -> Abb.Future.return res
  | `Closed -> Abb.Future.return (Error `Closed)

let bind_words stmt words =
  let count = ref 0 in
  CCListLabels.iter
    ~f:(function
        | w when CCString.prefix ~pre:"tag:" w ->
          let tag = CCString.drop (String.length "tag:") w in
          count := !count + 1;
          ignore (Sqlite3.bind stmt !count (Sqlite3.Data.TEXT tag))
        | w when CCString.prefix ~pre:"url:" w ->
          let url = CCString.drop (String.length "url:") w in
          let w = "%" ^ url ^ "%" in
          count := !count + 1;
          ignore (Sqlite3.bind stmt !count (Sqlite3.Data.TEXT w))
        | w ->
          let w = "%" ^ w ^ "%" in
          count := !count + 1;
          ignore (Sqlite3.bind stmt !count (Sqlite3.Data.TEXT w)))
    words

let create_likes words =
  CCString.concat
    " AND "
    (CCListLabels.map
       ~f:(function
           | w when CCString.prefix ~pre:"tag:" w ->
             Printf.sprintf "articles.id IN (SELECT id FROM article_tags WHERE tag = ?)"
           | w when CCString.prefix ~pre:"url:" w ->
             Printf.sprintf "articles.url LIKE ?"
           | _ ->
             Printf.sprintf "articles.title LIKE ?")
       words)

let search t words =
  let open Abb.Future.Infix_monad in
  let like_clause = create_likes words in
  let search_tag_sql =
    Printf.sprintf
      "%s WHERE %s;"
      Sql.search_tag_head
      like_clause
  in
  let stmt = Sqlite3.prepare t.db search_tag_sql in
  bind_words stmt words;
  let tag_map = tags_by_stmt stmt in
  let search_sql =
    Printf.sprintf
      "%s WHERE %s %s;"
      Sql.search_article_head
      like_clause
      Sql.search_article_tail
  in
  let stmt = Sqlite3.prepare t.db search_sql in
  bind_words stmt words;
  let f () =
    with_stmt
      stmt
      ~f:(fun stmt ->
          let articles = map ~f:(article_of_stmt tag_map) stmt in
          Abb.Future.return articles)
  in
  Serializer.run
    t.serializer
    ~f
  >>= function
  | `Ok res -> Abb.Future.return res
  | `Closed -> Abb.Future.return (Error `Closed)

