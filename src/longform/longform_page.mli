val render_page :
  ?search:string ->
  Longform_storage.Article.stored Longform_storage.Article.t list ->
  int ->
  (string, [> Brtl.Make(Abb).Tmpl.err ]) result

val render_homepage :
  Longform_storage.t ->
  int option ->
  (string, unit) Brtl.Make(Abb).Ctx.t ->
  (string, Brtl.Make(Abb).Rspnc.t) Brtl.Make(Abb).Ctx.t Abb.Future.t

val render_search :
  Longform_storage.t ->
  (string, unit) Brtl.Make(Abb).Ctx.t ->
  (string, Brtl.Make(Abb).Rspnc.t) Brtl.Make(Abb).Ctx.t Abb.Future.t

