module Fut_comb = Abb_future_combinators.Make(Abb.Future)

module Http = Cohttp_abb.Make(Abb)

module Brtl = Brtl.Make(Abb)

let tls_config =
  let cfg = Otls.Tls_config.create () in
  Otls.Tls_config.insecure_noverifycert cfg;
  cfg

let title_pattern = CCOpt.get_exn (Lua_pattern.of_string "<title>(.-)</title>")

let fetch_title data =
  match Lua_pattern.mtch data title_pattern with
    | Some mtch ->
      Lua_pattern.Capture.to_string (CCList.hd (Lua_pattern.Match.captures mtch))
    | None ->
      ""

let rec fetch_uri uri =
  let open Fut_comb.Infix_result_monad in
  Logs.debug (fun m -> m "Attempting to fetch title for %s" (Uri.to_string uri));
  Http.Client.call ~tls_config `GET uri
  >>= fun (resp, body) ->
  match resp.Http.Response.status with
    | `OK ->
      Abb.Future.return (Ok body)
    | `Moved_permanently ->
      (match Cohttp.Header.get (Http.Response.headers resp) "location" with
        | Some location -> fetch_uri (Uri.of_string location)
        | None -> failwith "nyi")
    | _ ->
      Abb.Future.return (Ok "")

let run ctx =
  let open Abb.Future.Infix_monad in
  let uri = Uri.of_string (Brtl.Ctx.body ctx) in
  fetch_uri uri
  >>| function
  | Ok body ->
    Brtl.(Ctx.set_response (Rspnc.create ~status:`OK (fetch_title body)) ctx)
  | _ ->
    failwith "nyi"

