val add :
  Longform_storage.t ->
  int ->
  int ->
  (string, unit) Brtl.Make(Abb).Ctx.t ->
  (string, Brtl.Make(Abb).Rspnc.t) Brtl.Make(Abb).Ctx.t Abb.Future.t

val run :
  Longform_storage.t ->
  int ->
  (string, unit) Brtl.Make(Abb).Ctx.t ->
  (string, Brtl.Make(Abb).Rspnc.t) Brtl.Make(Abb).Ctx.t Abb.Future.t

