module Fut_comb = Abb_future_combinators.Make(Abb.Future)

module Brtl = Brtl.Make(Abb)

module Article = Longform_storage.Article
module Storage = Longform_storage

let page =
  Longform_tmpl.read "add_related.html"
  |> CCOpt.get_exn
  |> Brtl.Tmpl.Template.of_utf8_string
  |> CCResult.get_exn
  |> CCFun.flip Brtl.Tmpl.of_template []

let create_rspnc = function
  | Ok body ->
    Brtl.Rspnc.create ~status:`OK body
  | Error _ ->
    Brtl.Rspnc.create ~status:`Internal_server_error ""

let add_rspnc = function
  | Ok () ->
    let headers = Cohttp.Header.of_list [("location", "/")] in
    Brtl.Rspnc.create ~headers ~status:`See_other ""
  | Error _ ->
    failwith "nyi"

let run storage article ctx =
  let kv = Brtl.Tmpl.Kv.(Map.of_list [("article_id", int article)]) in
  Abb.Future.return Brtl.(Ctx.set_response (create_rspnc (Tmpl.apply page kv)) ctx)

let add storage article_id add_id ctx =
  let open Abb.Future.Infix_monad in
  Storage.add_article_related storage add_id article_id
  >>| fun res ->
  Brtl.Ctx.set_response (add_rspnc res) ctx
