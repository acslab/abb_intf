module Fut_comb = Abb_future_combinators.Make(Abb.Future)

module Brtl = Brtl.Make(Abb)

module Article = Longform_storage.Article
module Page = Longform_page
module Storage = Longform_storage

let num_articles_per_page = 15

let render_articles storage article_id =
  let open Fut_comb.Infix_result_monad in
  Storage.related_articles
    storage
    ~id:article_id
    (num_articles_per_page + 1)
  >>= fun all_articles ->
  Abb.Future.return (Page.render_page all_articles num_articles_per_page)

let run storage article_id ctx =
  let open Abb.Future.Infix_monad in
  render_articles storage article_id
  >>| function
  | Ok str ->
    Brtl.(Ctx.set_response (Rspnc.create ~status:`OK str) ctx)
  | Error (#Brtl.Tmpl.err as err) ->
    let err_str = Brtl.Tmpl.show_err err in
    let headers = Cohttp.Header.of_list [("content-type", "text/plain")] in
    Brtl.(Ctx.set_response (Rspnc.create ~headers ~status:`Internal_server_error err_str) ctx)
  | Error #Storage.err ->
    Brtl.(Ctx.set_response (Rspnc.create ~status:`Internal_server_error "") ctx)

