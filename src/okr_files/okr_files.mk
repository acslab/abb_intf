.PHONY: clean_okr_files

# Make it serial because we are adding a NON_LIB_MODULE
.NOTPARALLEL:

NON_LIB_MODULES += okr_files.ml

$(SRC_DIR)/okr_files.ml: $(wildcard ../../../okr_files/*)
	ocaml-crunch -m plain ../../../okr_files > $(SRC_DIR)/okr_files.ml

clean: clean_okr_files

clean_okr_files:
	rm $(SRC_DIR)/okr_files.ml
