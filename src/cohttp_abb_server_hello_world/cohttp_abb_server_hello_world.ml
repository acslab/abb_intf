module Abb = Abb_scheduler_select
module Buffered = Abb_io_buffered.Make(Abb.Future)
module Fut_comb = Abb_future_combinators.Make(Abb.Future)
module Http = Cohttp_abb.Make(Abb)
module Router_handler = Cohttp_abb_router_handler.Make(Abb)
module Fc = Furl_capture

module Cmdline = struct
  module C = Cmdliner

  let port =
    let doc = "Port to run server on." in
    C.Arg.(value & opt int 8080 & info ["p"; "port"] ~doc)

  let http_cmd f =
    let doc = "Run an HTTP server." in
    let exits = C.Term.default_exits in
    C.Term.(const f $ port, info "http" ~doc ~exits)

  let https_cmd f =
    let cert =
      let doc = "Server certificate, a PEM file." in
      C.Arg.(required & opt (some file) None & info ["c"; "cert"] ~doc)
    in
    let priv_key =
      let doc = "Server private key." in
      C.Arg.(required & opt (some file) None & info ["k"; "key"] ~doc)
    in
    let doc = "Run an HTTPS server." in
    let exits = C.Term.default_exits in
    C.Term.(const f $ port $ cert $ priv_key, info "https" ~doc ~exits)

  let default_cmd =
    let doc = "A simple HTTP(S) server." in
    let exits = C.Term.default_exits in
    C.Term.(ret (const (fun _ -> (`Help (`Pager, None))) $ port),
            info "server" ~doc ~exits)
end

let hello_name () =
  Furl.(rel / "hello" /% Fc.Path.string)

let goodbye_name () =
  Furl.(rel / "goodbye" /% Fc.Path.string)

let query_too () =
  Furl.(rel / "query" /? Fc.Query.string "this_too")

let handle_hello_name name req r w =
  let open Abb.Future.Infix_monad in
  let response =
    Http.Response.make
      ~status:`OK
      ()
  in
  Http.Response_io.write
    (fun writer -> Http.Response_io.write_body writer (Printf.sprintf "Hello, %s" name))
    response
    w
  >>| fun () ->
  `Ok

let handle_goodbye_name name req r w =
  let open Abb.Future.Infix_monad in
  let response =
    Http.Response.make
      ~status:`OK
      ()
  in
  Http.Response_io.write
    (fun writer -> Http.Response_io.write_body writer (Printf.sprintf "Goodbye, %s" name))
    response
    w
  >>| fun () ->
  `Stop

let handle_query_too this_too req r w =
  let open Abb.Future.Infix_monad in
  let response =
    Http.Response.make
      ~status:`OK
      ()
  in
  Http.Response_io.write
    (fun writer -> Http.Response_io.write_body writer (Printf.sprintf "this_too = %s" this_too))
    response
    w
  >>| fun () ->
  `Ok

let page_not_found req r w =
  let open Abb.Future.Infix_monad in
  let response =
    Http.Response.make
      ~status:`Not_found
      ()
  in
  Http.Response_io.write
    (fun writer -> Fut_comb.unit)
    response
    w
  >>| fun () ->
  `Ok

let router =
  Furl.(match_url
          ~default:(fun _ -> page_not_found)
          [ hello_name () --> handle_hello_name
          ; goodbye_name () --> handle_goodbye_name
          ; query_too () --> handle_query_too
          ])

let https_server port cert_file priv_key_file =
  let run () =
    let tls_config = Otls.Tls_config.create () in
    ignore (Otls.Tls_config.set_cert_file tls_config cert_file);
    ignore (Otls.Tls_config.set_key_file tls_config priv_key_file);
    let config =
      Http.Server.(Config.of_view
                     { Config.View.scheme = Scheme.Https tls_config
                     ; on_handler_exn = `Ignore
                     ; port
                     ; handler = Router_handler.handler router
                     ; read_header_timeout = Some (Duration.of_sec 10)
                     ; handler_timeout = Some (Duration.of_sec 1)
                     })
    in
    Http.Server.run (CCResult.get_exn config)
  in
  match Abb.Scheduler.run (Abb.Scheduler.create ()) run with
    | `Det (Ok _) ->
      ()
    | _ ->
      assert false

let http_server port =
  let run () =
    let config =
      Http.Server.(Config.of_view
                     { Config.View.scheme = Scheme.Http
                     ; on_handler_exn = `Ignore
                     ; port
                     ; handler = Router_handler.handler router
                     ; read_header_timeout = Some (Duration.of_sec 10)
                     ; handler_timeout = Some (Duration.of_sec 1)
                     })
    in
    Http.Server.run (CCResult.get_exn config)
  in
  match Abb.Scheduler.run (Abb.Scheduler.create ()) run with
    | `Det (Ok _) ->
      ()
    | _ ->
      assert false

let cmds = Cmdline.([http_cmd http_server; https_cmd https_server])

let () = Cmdliner.Term.(exit @@ eval_choice Cmdline.default_cmd cmds)
