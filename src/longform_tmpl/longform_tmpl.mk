.PHONY: clean_longform_tmpl

# Make it serial because we are adding a NON_LIB_MODULE
.NOTPARALLEL:

NON_LIB_MODULES += longform_tmpl.ml

$(SRC_DIR)/longform_tmpl.ml: $(wildcard ../../../longform_files/*)
	ocaml-crunch -m plain ../../../longform_files > $(SRC_DIR)/longform_tmpl.ml

clean: clean_longform_tmpl

clean_longform_tmpl:
	rm $(SRC_DIR)/longform_tmpl.ml
