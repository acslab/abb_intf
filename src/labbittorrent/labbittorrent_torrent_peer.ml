(* Implements a peer for a torrent.  It monitors a tracker and connects to peers
   to download/upload pieces.  The algorithm used prioritizes downloaded the
   rarest piece it knows out first.

   Every 60 seconds the service updates its list of peers from the tracker, it
   then attempts to make connections to any peers it does not have a connection
   to.  Peers will then report what pieces they have.  On any event related to
   data transfer (reporting a piece a peer has, successful and failed request
   downloads) any open download slots are filled.  The algorithm for filling a
   download slot:

   - Desired pieces are ordered by their frequency count, least frequent to
     most.

   - All unchoking peers are listed along with the list of pieces they have and
     how many open download slots they have.  Each node is given 10 download
     slots. and a maximum of 10 nodes will be requested from.

   - The desired pieces are then iterated in order and requested to the first
     peer that has free slots, and the desired piece.  If the peer connection is
     not set to "interested", it is before the requests are sent.

   - With a node chosen for a desired piece, the piece is broken up into 16kb
     blocks, and all requests are sent to the peer.  The only special block is
     the last one which can be less than 16kb.

   Along with this, the choking algorithm is:

   - List all nodes that are interested.

   - Sort them by downloaded bytes in descending order.

   - Take the top four uploaders and unchoke them.

   - Take a random node and unchoke it.

   Throughout this process, dead peers are discovered by their channels
   closing. *)
module Int_map = Map.Make(CCInt)
module Int_set = Set.Make(CCInt)
module List = ListLabels

module Peer_protocol = Labbittorrent_peer_protocol
module Peer_set = Labbittorrent_peer_set
module Piece_matcher = Labbittorrent_piece_matcher
module Request_set = Labbittorrent_peer_connection.Request_set
module Torrent = Labbittorrent_torrent

type request = Peer_protocol.Frame.Request.t

module Request_map = CCMap.Make(struct
    type t = request
    let compare = compare
  end)

module Peer_map = Map.Make(struct
    type t = Peer_set.Peer.t
    let compare = compare
  end)

module Make (Abb : Abb_intf.S) = struct
  open Abb.Future.Infix_monad

  module Channel = Abb_channel.Make(Abb.Future)
  module Service_local = Abb_service_local.Make(Abb.Future)
  module Serializer = Abb_service_serializer.Make(Abb.Future)
  module Fut_comb = Abb_future_combinators.Make(Abb.Future)
  module Tracker_client = Labbittorrent_tracker_client.Make(Abb.Future)
  module Peer_connection = Labbittorrent_peer_connection.Make(Abb)

  type msg =
    | Issue_requests
    | Update_peer_set
    | Have of (Peer_set.Peer.t * int)
    | Request_complete of (Peer_set.Peer.t *  (request, request) result)
    | Peer_connected of (Peer_set.Peer.t * Peer_connection.t)

  type t = (Channel.writer, msg) Channel.t

  type init_args = { tracker : Tracker_client.t
                   ; torrent : Torrent.t
                   ; download_dir : string
                   ; peer_id : string
                   }

  module Server = struct
    type t = { tracker : Tracker_client.t
             ; peer_id : string
             ; torrent : Torrent.t
             ; num_pieces : int
             ; download_dir : string
             ; peer_conns : Peer_connection.t Peer_map.t
             ; requests_to_peers : Peer_set.Peer.t Request_map.t
             (* ;  *)
             ; peers_have : Peer_set.t Int_map.t
             ; have : Int_set.t
             ; matcher : Abb.File.t Serializer.Mutex.t Piece_matcher.t
             ; writer : (Channel.writer, msg) Channel.t
             ; killswitch : unit Abb.Future.t
             }

    let create_files download_dir files =
      Fut_comb.List.iter
        ~f:(fun file ->
            let full_path = Filename.concat download_dir (Torrent.File.path file) in
            Fut_comb.ignore (Abb.File.truncate full_path (Torrent.File.length file)))
        files

    let read_contents b mtches =
      Fut_comb.ignore
        (Fut_comb.List.fold_left
           ~f:(fun pos (file, offset, length) ->
               Abb.File.pread
                 file
                 ~offset:(Piece_matcher.Offset.to_int offset)
                 ~buf:b
                 ~pos
                 ~len:(Piece_matcher.Length.to_int length)
               >>| function
               | Ok n ->
                 assert (n = Piece_matcher.Length.to_int length);
                 pos + Piece_matcher.Length.to_int length
               | Error _ ->
                 assert false)
           ~init:0
           mtches)

    let collect_downloaded_pieces download_dir matcher torrent =
      let piece_length = Torrent.piece_length torrent in
      let piece_hashes = Torrent.piece_hashes torrent in
      let b = Bytes.create piece_length in
      let length = Piece_matcher.Length.of_int piece_length in
      Fut_comb.List.fold_left
        ~f:(fun acc (idx, hash) ->
            let offset = Piece_matcher.Offset.of_int (idx * piece_length) in
            let mtches = Piece_matcher.mtch (offset, length) matcher in
            read_contents b mtches
            >>| fun () ->
            match Sha1.to_bin (Sha1.string (Bytes.unsafe_to_string b)) with
              | sha when sha = hash ->
                Int_set.add idx acc
              | _ ->
                acc)
        ~init:Int_set.empty
        (List.mapi ~f:(fun idx v -> (idx, v)) piece_hashes)

        let request_set_of_piece idx length piece_length =
      let rec f = function
        | offset when offset < piece_length ->
          let len = min (piece_length - offset) length in
          let req =
            Peer_protocol.Frame.Request.({ index = idx
                                         ; start = offset
                                         ; length = len
                                         })
          in
          req::f (offset + len)
        | _ ->
          []
      in
      Request_set.of_list (f 0)

    let pieces_ordered_by_count peers_have =
      let piece_count_compare (_, p1) (_, p2) =
        compare (Peer_set.cardinal p1) (Peer_set.cardinal p2)
      in
      peers_have
      |> Int_map.bindings
      |> List.sort ~cmp:piece_count_compare

    let unchoking_peers peer_conns =
      Fut_comb.List.filter
        ~f:(fun (_, p) ->
            Peer_connection.peer_choking p
            >>| function
            | `Ok true
            | `Closed -> false
            | `Ok false -> true)
        (Peer_map.bindings peer_conns)

    let free_download_slots have ongoing_requests pieces unchoking_peers =
      CCList.take
        (100 - Int_map.cardinal ongoing_requests)
        (List.filter
           ~f:(fun (idx, p) ->
               not (Int_set.mem idx have) &&
               not (Peer_set.is_empty (Peer_set.inter p unchoking_peers)))
           pieces)

    let send_requests writer peer peer_conn rs =
      let open Fut_comb.Infix_result_monad in
      Channel.Combinators.to_result (Peer_connection.set_interested peer_conn true)
      >>= fun _ ->
      Channel.Combinators.to_result
        (Peer_connection.request
           peer_conn
           ~f:(fun r ->
               Fut_comb.ignore
                 (Channel.send writer (Request_complete (peer, r))))
           rs)

    let merge_with_ongoing_requests rs idx ongoing_requests =
      ongoing_requests
      |> Int_map.find idx
      |> Request_set.union rs
      |> (fun rs -> Int_map.add idx rs ongoing_requests)

    let handle_issue_requests t = failwith "nyi"
      (* let pieces = pieces_ordered_by_count t.peers_have in *)
      (* unchoking_peers t.peer_conns *)
      (* >>= fun unchoking_peer_bindings -> *)
      (* let unchoking_peers = Peer_set.of_list (List.map ~f:fst unchoking_peer_bindings) in *)
      (* (\* FIX: For now, just have 100 outstanding pieces.  This should be 100 *)
      (*    outstanding requests. *\) *)
      (* let downloadable = free_download_slots t.have t.ongoing_requests pieces unchoking_peers in *)
      (* Fut_comb.List.fold_left *)
      (*   ~f:(fun t (idx, peers) -> *)
      (*       let peers = Peer_set.inter peers unchoking_peers in *)
      (*       let rs = request_set_of_piece idx (16 * 1024 * 1024) (Torrent.piece_length t.torrent) in *)
      (*       match Peer_set.elements peers with *)
      (*         | [] -> *)
      (*           assert false *)
      (*         | p::_ -> *)
      (*           let peer_conn = Peer_map.find p t.peer_conns in *)
      (*           send_requests t.writer p peer_conn rs *)
      (*           >>| function *)
      (*           | Ok _ -> *)
      (*             let ongoing_requests = merge_with_ongoing_requests rs idx t.ongoing_requests in *)
      (*             { t with ongoing_requests } *)
      (*           | Error _ -> *)
      (*             t) *)
      (*   ~init:t *)
      (*   downloadable *)

    let handle_update_peer_set t =
      Fut_comb.List.map
        ~f:(fun (peer, peer_conn) ->
            Peer_connection.downloaded peer_conn
            >>| fun ret ->
            (peer, ret))
        (Peer_map.bindings t.peer_conns)
      >>= fun downloaded ->
      List.fold_left
        ~f:(fun t (peer, downloaded_ret) ->
            match downloaded_ret with
              | Ok _ ->
                t
              | Error _ ->
                failwith "nyi")
        ~init:(failwith "nyi")
        (failwith "nyi")

    let handle_have peer idx t = failwith "nyi"

    let handle_request_complete peer res t = failwith "nyi"

    let handle_peer_connected peer conn t = failwith "nyi"

    let loop t = function
      | Issue_requests -> handle_issue_requests t
      | Update_peer_set -> handle_update_peer_set t
      | Have (peer, idx) -> handle_have peer idx t
      | Request_complete (peer, res) -> handle_request_complete peer res t
      | Peer_connected (peer, conn) -> handle_peer_connected peer conn t

    let init (init_args : init_args) writer reader =
      Fut_comb.List.map
        ~f:(fun file ->
            let full_path = Filename.concat init_args.download_dir (Torrent.File.path file) in
            Abb.File.open_file
              ~flags:Abb_intf.File.Flag.([Read_write; Create 0o644])
              full_path
            >>| function
            | Ok f -> f
            | Error _ -> assert false)
        (Torrent.files init_args.torrent)
      >>= fun files ->
      let file_offsets =
        List.rev
          (fst
             (List.fold_left
                ~f:(fun (acc, offset) (file, fd) ->
                    let acc = (Piece_matcher.Offset.of_int offset, fd)::acc in
                    (acc, offset + Int64.to_int (Torrent.File.length file)))
                ~init:([], 0)
                (List.combine (Torrent.files init_args.torrent) files)))
      in
      let matcher = Piece_matcher.create file_offsets in
      collect_downloaded_pieces init_args.download_dir matcher init_args.torrent
      >>= fun have ->
      Fut_comb.List.map
        ~f:(fun (offset, fd) ->
            Serializer.create ()
            >>| fun serializer ->
            let mutex = Serializer.Mutex.create serializer fd in
            (offset, mutex))
        file_offsets
      >>= fun mutx_file_offsets ->
      let killswitch = Abb.Future.Promise.future (Abb.Future.Promise.create ()) in
      failwith "nyi"
      (* let t = { tracker = init_args.tracker *)
      (*         ; peer_id = init_args.peer_id *)
      (*         ; torrent = init_args.torrent *)
      (*         ; num_pieces = List.length (Torrent.piece_hashes init_args.torrent) *)
      (*         ; download_dir = init_args.download_dir *)
      (*         ; peer_conns = Peer_map.empty *)
      (*         ; ongoing_requests = Int_map.empty *)
      (*         ; peers_have = Int_map.empty *)
      (*         ; have =  have *)
      (*         ; matcher = Piece_matcher.create mutex_file_offsets *)
      (*         ; writer = writer *)
      (*         ; killswitch = killswitch *)
      (*         } *)
      (* in *)
      (* ignore (Channel.send writer Issue_requests); *)
      (* let loop_fut = *)
      (*   Channel.Combinators.fold *)
      (*     ~f:loop *)
      (*     ~init:t *)
      (*     reader *)
      (* in *)
      (* Fut_comb.link loop_fut killswitch; *)
      (* Fut_comb.ignore loop_fut *)
  end

  let create init_args =
    Service_local.create (Server.init init_args)
end
