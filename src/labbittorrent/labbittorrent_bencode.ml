module List = ListLabels
module String_map = Map.Make(String)

module View = struct
  type t =
    | String of string
    | Int of Num.num
    | Dict of t String_map.t
    | List of t list
end

module Lens = struct
  open Lens

  let key k =
    { get = (fun d -> String_map.find k d)
    ; set = (fun v d -> String_map.add k v d)
    }

  let key_opt k =
    { get =
        (fun d ->
           try Some (String_map.find k d) with
             | Not_found -> None)
    ; set =
        (fun v_opt d ->
           match v_opt with
             | Some v ->
               String_map.add k v d
             | None ->
               String_map.remove k d)
    }

  let dict =
    { get =
        (function
          | View.Dict d -> d
          | _ -> raise (Invalid_argument "dict"))
    ; set = (fun d _ -> View.Dict d)
    }

  let list =
    { get =
        (function
          | View.List l -> l
          | _ -> raise (Invalid_argument "list"))
    ; set = (fun l _ -> View.List l)
    }

  let int =
    { get =
        (function
          | View.Int n -> n
          | _ -> raise (Invalid_argument "int"))
    ; set = (fun n _ -> View.Int n)
    }

  let string =
    { get =
        (function
          | View.String s -> s
          | _ -> raise (Invalid_argument "string"))
    ; set = (fun s _ -> View.String s)
    }

  let list_of lens =
    { get =
        (function
          | View.List l ->
            Lens.Infix.(List.map ~f:(fun v -> v |. lens) l)
          | _ ->
            raise (Invalid_argument "list_of"))
    ; set = (fun _ _ -> assert false)
    }
end


type t = View.t

let read_string pos b =
  let e = Bytes.index_from b pos ':' in
  let len = int_of_string (Bytes.sub_string b pos (e - pos)) in
  let s = Bytes.sub_string b (e + 1) len in
  (e + 1 + len, s)

let rec of_bytes ?(pos = 0) b =
  match Bytes.get b pos with
    | 'l' -> begin
      let (pos', list) = read_list (pos + 1) b [] in
      let l = View.List list in
      (pos', l)
    end
    | 'i' -> begin
      let pos' = pos + 1 in
      let e = Bytes.index_from b pos' 'e' in
      let i = View.Int (Num.num_of_string (Bytes.sub_string b pos' (e - pos'))) in
      (e + 1, i)
    end
    | 'd' -> begin
      let (pos', dict) = read_dict (pos + 1) b String_map.empty in
      let d = View.Dict dict in
      (pos', d)
    end
    | '0'..'9' -> begin
      let (e, s) = read_string pos b in
      (e, View.String s)
    end
    | _ ->
      failwith (Printf.sprintf "Invalid string, pos = %d string = %s" pos (Bytes.to_string b))
and read_list pos b acc =
  if Bytes.get b pos = 'e' then
    (pos + 1, List.rev acc)
  else
    let (pos, v) = of_bytes ~pos b in
    read_list pos b (v::acc)
and read_dict pos b acc =
  if Bytes.get b pos = 'e' then
    (pos + 1, acc)
  else
    let (pos, key) = read_string pos b in
    let (pos, v) = of_bytes ~pos b in
    read_dict pos b (String_map.add key v acc)

let to_bytes t =
  let rec tb buf = function
    | View.String s -> begin
      Buffer.add_string buf (Printf.sprintf "%d:" (String.length s));
      Buffer.add_string buf s
    end
    | View.Int n -> begin
      Buffer.add_string buf (Printf.sprintf "i%se" (Num.string_of_num n))
    end
    | View.List l -> begin
      Buffer.add_char buf 'l';
      List.iter ~f:(tb buf) l;
      Buffer.add_char buf 'e'
    end
    | View.Dict d -> begin
      Buffer.add_char buf 'd';
      (* This depends on folding over a dictionary being in sorted order *)
      String_map.iter
        (fun k v ->
          tb buf (View.String k);
          tb buf v)
        d;
      Buffer.add_char buf 'e'
    end
  in
  let b = Buffer.create 100 in
  tb b t;
  Buffer.to_bytes b

external to_view : t -> View.t = "%identity"
external of_view : View.t -> t = "%identity"

