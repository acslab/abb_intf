module List = ListLabels
module Unix = UnixLabels

module Peer_set = Labbittorrent_peer_set
module Key = Labbittorrent_tracker_client.Key
module Bencode = Labbittorrent_bencode

module Reader = struct
  type t = { mutable offset : int
           ; s : string
           }

  let create ?(offset = 0) s = { offset; s }

  let offset t = t.offset

  let int32 t =
    let offset = t.offset in
    t.offset <- t.offset + 4;
    EndianString.BigEndian.get_int32 t.s offset

  let uint16 t =
    let offset = t.offset in
    t.offset <- t.offset + 2;
    EndianString.BigEndian.get_uint16 t.s offset
end

module Make (Abb : Abb_intf.S with type Native.t = Unix.file_descr) = struct
  module Tracker_client = Labbittorrent_tracker_client.Make(Abb.Future)
  module Fut_comb = Abb_future_combinators.Make(Abb.Future)
  module Fut = Abb.Future

  let read_file file =
    let open Fut.Infix_monad in
    let b = Bytes.create 1024 in
    let buf = Buffer.create 1024 in
    let rec rf () =
      Abb.File.read file ~buf:b ~pos:0 ~len:(Bytes.length b)
      >>= function
      | Ok 0 ->
        Fut.return (Buffer.to_bytes buf)
      | Ok n ->
        Buffer.add_subbytes buf b 0 n;
        rf ()
      | _ ->
        assert false
    in
    rf ()

  let run_process proc_args =
    let (src_stdin, local_stdin) = Unix.pipe () in
    let (local_stdout, src_stdout) = Unix.pipe () in
    let (local_stderr, src_stderr) = Unix.pipe () in
    let dups =
      Abb_intf.Process.Dup.([ create ~src:src_stdin ~dst:Unix.stdin
                            ; create ~src:src_stdout ~dst:Unix.stdout
                            ; create ~src:src_stderr ~dst:Unix.stderr
                            ])
    in
    Fut_comb.with_finally
      (fun () ->
         match Abb.Process.spawn proc_args dups with
           | Ok proc ->
             List.iter
               ~f:Unix.close
               [ local_stdin
               ; src_stdout
               ; src_stderr
               ];
             let local_stdout = Abb.File.of_native local_stdout in
             let local_stderr = Abb.File.of_native local_stderr in
             let wait exit_code stdout stderr =
               match exit_code with
                 | Abb_intf.Process.Exit_code.Exited 0 ->
                   Ok (stdout, stderr)
                 | _ ->
                   Error (`Run (exit_code, stdout, stderr))
             in
             let open Fut.Infix_app in
             wait <$> Abb.Process.wait proc <*> read_file local_stdout <*> read_file local_stderr
           | Error _ ->
             Fut.return (Error `Spawn))
      ~finally:(fun () ->
          Unix.close local_stdout;
          Unix.close local_stderr;
          Fut.return ())

  let run_curl url =
    let open Fut.Infix_monad in
    let url_s = Uri.to_string url in
    let proc_args =
      Abb_intf.Process.({ exec_name = "curl"
                        ; args = ["curl"; url_s]
                        ; env = None
                        ; cwd = None
                        })
    in
    run_process proc_args
    >>| function
    | Ok (stdout, _) ->
      snd (Bencode.of_bytes stdout)
    | Error (`Run (_, _, stderr)) ->
      Logs.err
        (fun m ->
           m "curl of url %s run failed with error: %s"
             url_s
             (Bytes.to_string stderr));
      assert false
    | Error `Spawn ->
      Logs.err (fun m -> m "curl could not be spawned.  Not installed?");
      assert false

  let rec read_peers r peer_set = function
    | 0 ->
      peer_set
    | n ->
      let ip = Ipaddr_unix.V4.to_inet_addr (Ipaddr.V4.of_int32 (Reader.int32 r)) in
      let port = Reader.uint16 r in
      read_peers r (Peer_set.add (ip, port) peer_set) (n - 1)

  let do_announce url s =
    let open Fut.Infix_monad in
    let url_query =
      Uri.with_query'
        url
        (List.concat
           [ [ ("info_hash", Sha1.to_bin s.Tracker_client.info_hash)
             ; ("peer_id", s.Tracker_client.peer_id)
             ; ("uploaded", string_of_int s.Tracker_client.uploaded)
             ; ("downloaded", string_of_int s.Tracker_client.downloaded)
             ; ("left", string_of_int s.Tracker_client.left)
             ; ("port", string_of_int s.Tracker_client.port)
             ; ("compact", "1")
             ]
           ; (match s.Tracker_client.ip with
               | Some i ->
                 [("ip", Ipaddr.V4.to_string (Ipaddr_unix.V4.of_inet_addr_exn i))]
               | None ->
                 [])
           ])
    in
    run_curl url_query
    >>| fun resp ->
    let view = Bencode.to_view resp in
    let open Bencode.Lens in
    let open Lens.Infix in
    let interval = view |. (dict |-- key "interval" |-- int) in
    let peer_str = view |. (dict |-- key "peers" |-- string) in
    let num_peers = String.length peer_str / 6 in
    let reader = Reader.create peer_str in
    let peers = read_peers reader Peer_set.empty num_peers in
    Tracker_client.({ leechers = 0
                    ; seeders = 0
                    ; peers = peers
                    ; interval = Num.int_of_num interval
                    })

  let announce url key s =
    let open Fut.Infix_monad in
    do_announce url s
    >>| fun resp ->
    Ok resp
end
