module Peer_set = Labbittorrent_peer_set

module Key = Labbittorrent_tracker_client.Key

(* Builds network packages.  All numbers will be converted to Big Endian *)
module Builder = struct
  type t = { mutable offset : int
           ; buf : Buffer.t
           }

  let create buf = { offset = 0; buf = buf }

  let offset t = t.offset

  let int64 t n =
    let len = 8 in
    let b = Bytes.create len in
    EndianBytes.BigEndian.set_int64 b 0 n;
    Buffer.add_bytes t.buf b;
    t.offset <- t.offset + len

  let int32 t n =
    let len = 4 in
    let b = Bytes.create len in
    EndianBytes.BigEndian.set_int32 b 0 n;
    Buffer.add_bytes t.buf b;
    t.offset <- t.offset + len

  let int16 t n =
    let len = 2 in
    let b = Bytes.create len in
    EndianBytes.BigEndian.set_int16 b 0 n;
    Buffer.add_bytes t.buf b;
    t.offset <- t.offset + len

  let bytes t b =
    Buffer.add_bytes t.buf b;
    t.offset <- t.offset + Bytes.length b
end

(* Reads network packages. *)
module Reader = struct
  type t = { mutable offset : int
           ; b : bytes
           }

  let create ?(offset = 0) b = { offset; b }

  let offset t = t.offset

  let int64 t =
    let offset = t.offset in
    t.offset <- t.offset + 8;
    EndianBytes.BigEndian.get_int64 t.b offset

  let int32 t =
    let offset = t.offset in
    t.offset <- t.offset + 4;
    EndianBytes.BigEndian.get_int32 t.b offset

  let int16 t =
    let offset = t.offset in
    t.offset <- t.offset + 2;
    EndianBytes.BigEndian.get_int16 t.b offset

  let uint16 t =
    let offset = t.offset in
    t.offset <- t.offset + 2;
    EndianBytes.BigEndian.get_uint16 t.b offset

  let bytes t n =
    let offset = t.offset in
    t.offset <- t.offset + n;
    Bytes.sub t.b offset n
end

module Make (Abb : Abb_intf.S) = struct
  module Tracker_client = Labbittorrent_tracker_client.Make(Abb.Future)

  module Fut_comb = Abb_future_combinators.Make(Abb.Future)

  module Request = struct
    module Connect = struct
      type t = { transaction_id : int32 }

      let size = 8 + 4 + 4

      (* This is the connection ID used to start a connection, the server
         will give back generated connection id. *)
      let magic_connection_id = 0x41727101980L

      let action = 0l

      let add buf t =
        let b = Builder.create buf in
        Builder.int64 b magic_connection_id;
        Builder.int32 b action;
        Builder.int32 b t.transaction_id
    end

    module Announce = struct
      type t = { connection_id : int64
               ; transaction_id : int32
               ; info_hash : bytes
               ; peer_id : bytes
               ; downloaded : int64
               ; left : int64
               ; uploaded : int64
               ; event : int32
               ; ip : int32
               ; key : int32
               ; num_want : int32
               ; port : int (* Actually an uint16 *)
               ; extensions : int (* Actually an uint16 *)
               }

      let size = 8 + 4 + 20 + 20 + 8 + 8 + 8 + 4 + 4 + 4 + 4 + 2 + 2

      let action = 1l

      let add buf t =
        assert (Bytes.length t.info_hash = 20);
        assert (Bytes.length t.peer_id = 20);
        assert (t.downloaded >= 0L);
        assert (t.left >= 0L);
        assert (t.uploaded >= 0L);
        assert (t.num_want > 0l || t.num_want = (-1l));
        let b = Builder.create buf in
        Builder.int64 b t.connection_id;
        Builder.int32 b action;
        Builder.int32 b t.transaction_id;
        Builder.bytes b t.info_hash;
        Builder.bytes b t.peer_id;
        Builder.int64 b t.downloaded;
        Builder.int64 b t.left;
        Builder.int64 b t.uploaded;
        Builder.int32 b t.event;
        Builder.int32 b t.ip;
        Builder.int32 b t.key;
        Builder.int32 b t.num_want;
        Builder.int16 b t.port;
        Builder.int16 b t.extensions
    end
  end

  module Response = struct
    module Connect = struct
      type t = { action : int32
               ; transaction_id : int32
               ; connection_id : int64
               }

      let size = 4 + 4 + 8

      let read b pos =
        assert ((Bytes.length b - pos) >= 4 + 4 + 8);
        let r = Reader.create ~offset:pos b in
        let action = Reader.int32 r in
        let transaction_id = Reader.int32 r in
        let connection_id = Reader.int64 r in
        { action; transaction_id; connection_id }
    end

    module Announce_header = struct
      type t = { action : int32
               ; transaction_id : int32
               ; interval : int32
               ; leechers : int32
               ; seeders : int32
               }

      let size = 4 * 5

      let read b pos =
        assert ((Bytes.length b - pos) >= 4 * 5);
        let r = Reader.create ~offset:pos b in
        let action = Reader.int32 r in
        let transaction_id = Reader.int32 r in
        let interval = Reader.int32 r in
        let leechers = Reader.int32 r in
        let seeders = Reader.int32 r in
        { action; transaction_id; interval; leechers; seeders }
    end

    module Announce_body = struct
      type t = { peers : Peer_set.t }

      let record_size = 4 + 2

      let rec read_n ~n peers r =
        match n with
          | 0 ->
            peers
          | n ->
            let ip = Ipaddr_unix.V4.to_inet_addr (Ipaddr.V4.of_int32 (Reader.int32 r)) in
            let port = Reader.uint16 r in
            read_n ~n:(n - 1) (Peer_set.add (ip, port) peers) r

      let read b pos n =
        assert ((Bytes.length b - pos) >= (n * (record_size)));
        let r = Reader.create ~offset:pos b in
        let peers = read_n ~n Peer_set.empty r in
        { peers }
    end
  end

  let make_bufs b =
    Abb_intf.Write_buf.([{ buf = b; pos = 0; len = Bytes.length b }])

  let make_conn_req trans_id =
    let buf = Buffer.create Request.Connect.size in
    Request.Connect.(add buf { transaction_id = trans_id });
    Buffer.to_bytes buf

  let make_announce_req key trans_id conn_id s =
    let buf = Buffer.create Request.Announce.size in
    let t =
      let open Tracker_client in
      let ip =
        match s.ip with
          | Some i ->
            Ipaddr.V4.to_int32 (Ipaddr_unix.V4.of_inet_addr_exn i)
          | None ->
            0l
      in
      Request.Announce.({ connection_id = conn_id
                        ; transaction_id = trans_id
                        ; info_hash = Bytes.of_string (Sha1.to_bin s.Tracker_client.info_hash)
                        ; peer_id = Bytes.of_string s.peer_id
                        ; downloaded = Int64.of_int s.downloaded
                        ; left = Int64.of_int s.left
                        ; uploaded = Int64.of_int s.uploaded
                        ; event = 0l
                        ; ip = ip
                        ; key = Key.to_int32 key
                        ; num_want = -1l
                        ; port = s.port
                        ; extensions = 0
                        })
    in
    Request.Announce.add buf t;
    Buffer.to_bytes buf

  let do_announce sock sockaddr key s =
    let open Fut_comb.Infix_result_monad in
    let trans_id = 0xdeadl in
    let recv_buf = Bytes.create 1500 in
    let conn_req = make_conn_req trans_id in
    let bufs = make_bufs conn_req in
    Abb.Socket.sendto sock ~bufs sockaddr
    >>= fun n ->
    assert (n = Bytes.length conn_req);
    Abb.Socket.recvfrom sock ~buf:recv_buf ~pos:0 ~len:(Bytes.length recv_buf)
    >>= fun (n, _) ->
    assert (n = Response.Connect.size);
    let conn_resp = Response.Connect.read recv_buf 0 in
    assert (conn_resp.Response.Connect.action = Request.Connect.action);
    assert (conn_resp.Response.Connect.transaction_id = trans_id);
    let conn_id = conn_resp.Response.Connect.connection_id in
    let announce_req = make_announce_req key trans_id conn_id s in
    let bufs = make_bufs announce_req in
    Abb.Socket.sendto sock ~bufs sockaddr
    >>= fun n ->
    assert (n = Bytes.length announce_req);
    Abb.Socket.recvfrom sock ~buf:recv_buf ~pos:0 ~len:(Bytes.length recv_buf)
    >>| fun (n, _) ->
    assert (n >= Response.Announce_header.size);
    let announce_header_resp = Response.Announce_header.read recv_buf 0 in
    assert (announce_header_resp.Response.Announce_header.action = Request.Announce.action);
    assert (announce_header_resp.Response.Announce_header.transaction_id = trans_id);
    let leechers = Int32.to_int announce_header_resp.Response.Announce_header.leechers in
    let seeders = Int32.to_int announce_header_resp.Response.Announce_header.seeders in
    let num_peers = (n - Response.Announce_header.size)/Response.Announce_body.record_size in
    let announce_body_resp =
      Response.Announce_body.read
        recv_buf
        Response.Announce_header.size
        num_peers
    in
    Tracker_client.({ leechers = leechers
                    ; seeders = seeders
                    ; peers = announce_body_resp.Response.Announce_body.peers
                    ; interval = Int32.to_int announce_header_resp.Response.Announce_header.interval
                    })

  let announce sockaddr key s =
    let open Abb.Future.Infix_monad in
    let sock = CCResult.get_exn (Abb.Socket.Udp.create ~domain:(Abb_intf.Socket.Domain.Inet4)) in
    Fut_comb.with_finally
      (fun () ->
         Fut_comb.first
           (do_announce sock sockaddr key s)
           (Abb.Sys.sleep 30.0 >>| fun () -> Error `Timedout)
         >>= function
         | (Ok resp, timer) ->
           Abb.Future.abort timer
           >>| fun () ->
           Ok resp
         | (Error _, req) ->
           Abb.Future.abort req
           >>| fun () ->
           Error `Timedout)
      ~finally:(fun () ->
          Abb.Socket.close sock
          >>| fun _ ->
          ())
end
