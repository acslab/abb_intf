type 'a t

module Offset : sig
  type t

  val of_int : int -> t
  val to_int : t -> int
end

module Length : sig
  type t

  val of_int : int -> t
  val to_int : t -> int
end

val create : (Offset.t * 'a) list -> 'a t

(** Assumes that the given offset is within the range of the pieces.  We cannot
    tell if the offset is too high because we only have the offsets of the files
    rather than their lengths.

    Returns a list of triples that include the identifier for file, the offset
    within that file to start writing and the length to write into that
    file. *)
val mtch : (Offset.t * Length.t) -> 'a t -> ('a * Offset.t * Length.t) list
