(** Represents peers *)
module Peer : sig
  type t = (Unix.inet_addr * int)
end

(** Compare module for peers *)
module Peer_compare : Set.OrderedType with type t = Peer.t

include module type of Set.Make(Peer_compare)
