module List = ListLabels

module String_map = Map.Make(String)
module Uri_set = Set.Make(Uri)

module Bencode = Labbittorrent_bencode

(* Handy function for extracting an option with a default *)
let option_get_default d =
  Lens.({ get =
            (function
              | Some v -> v
              | None -> d)
        ; set = (fun v _ -> Some v)
        })

module File = struct
  type t = { length : int64
           ; path : string
           }

  let join_paths = function
    | [] ->
      assert false
    | f::fs ->
      List.fold_left
        ~f:Filename.concat
        ~init:f
        fs

  let create ~length ~path_list =
    { length = length
    ; path = join_paths path_list
    }

  let length t = t.length

  let path t = t.path
end


type t = { announce_urls : Uri_set.t
         ; info_hash : Sha1.t
         ; piece_length : int
         ; files : File.t list
         ; piece_hashes : string list
         }

let compute_announce_urls view =
  let open Lens.Infix in
  let open Bencode.Lens in
  let announce = view |. (dict |-- key "announce" |-- string) in
  let announce_list =
    view |. (dict
             |-- key_opt "announce-list"
             |-- option_get_default (Bencode.View.List [])
             |-- list_of (list_of string))
  in
  Uri_set.of_list
    (List.map
       ~f:Uri.of_string
       (List.concat
          [ [announce]
          ; List.concat announce_list
          ]))

let compute_info_hash view =
  let open Lens.Infix in
  let open Bencode.Lens in
  let info = view |. (dict |-- key "info") in
  info
  |> Bencode.of_view
  |> Bencode.to_bytes
  |> Bytes.to_string
  |> Sha1.string

let get_piece_length view =
  let open Lens.Infix in
  let open Bencode.Lens in
  let piece_length = view |. (dict |-- key "info" |-- dict |-- key "piece length" |-- int) in
  Num.int_of_num piece_length

let compute_single_file_torrent view =
  let open Lens.Infix in
  let open Bencode.Lens in
  let length = view |. (dict |-- key "length" |-- int) in
  let name = view |. (dict |-- key "name" |-- string) in
  let file = File.create (Int64.of_string (Num.string_of_num length)) [name] in
  [file]

let compute_multiple_file_torrent view =
  let open Lens.Infix in
  let open Bencode.Lens in
  let files = view |. (dict |-- key "files" |-- list_of dict) in
  List.map
    ~f:(fun d ->
        let path = d |. (key "path" |-- list_of string) in
        let length = d |. (key "length" |-- int) in
        File.create (Int64.of_string (Num.string_of_num length)) path)
    files

let compute_files view =
  let open Lens.Infix in
  let open Bencode.Lens in
  let info = view |. (dict |-- key "info") in
  match info |. (dict |-- key_opt "length") with
    | Some _ ->
      (* Single file *)
      compute_single_file_torrent info
    | None ->
      (* Multiple file torrent (or an invalid torrent) *)
      compute_multiple_file_torrent info

let compute_piece_hashes view =
  let open Lens.Infix in
  let open Bencode.Lens in
  let info = view |. (dict |-- key "info") in
  let pieces = info |. (dict |-- key "pieces" |-- string) in
  let length = String.length pieces in
  let hash_length = 20 in
  let num_pieces = length / hash_length in
  let rec split = function
    | n when n = num_pieces -> []
    | n -> (String.sub pieces (n * hash_length) hash_length)::split (n + 1)
  in
  split 0

let of_bencode bencode =
  let view = Bencode.to_view bencode in
  let announce_urls = compute_announce_urls view in
  let info_hash = compute_info_hash view in
  let piece_length = get_piece_length view in
  let files = compute_files view in
  let piece_hashes = compute_piece_hashes view in
  { announce_urls; info_hash; piece_length; files; piece_hashes }

let announce_urls t = t.announce_urls

let info_hash t = t.info_hash

let piece_length t = t.piece_length

let files t = t.files

let piece_hashes t = t.piece_hashes
