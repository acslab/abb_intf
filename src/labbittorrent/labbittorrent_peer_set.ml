module Peer = struct
  type t = (Unix.inet_addr * int)
end

module Peer_compare = struct
  type t = Peer.t
  let compare = compare
end

include Set.Make(Peer_compare)

