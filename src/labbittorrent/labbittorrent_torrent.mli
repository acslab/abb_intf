(** Internal representation of a torrent.  This is not a 1-to-1 representation
    of the torrent file, this keeps only the essential information needed to
    download a torrent.  It also combines some information, for example an
    announce and an announce-list. *)

(** An announce list is an unordered set of URLs. *)
module Uri_set : sig
  include module type of Set.Make(Uri)
end

(** Opaque datatype *)
type t

(** A torrent is an collection of file(s).  Each file has a length and a
    path. *)
module File : sig
  type t

  (** The length of the file.  [int64] because it could be quite large. *)
  val length : t -> int64

  (** The path, relative to the download directory, of the file. *)
  val path : t -> string
end

(** Create a torrent out of a parsed bencoded value. *)
val of_bencode : Labbittorrent_bencode.t -> t

(** Get the set of announce URLs (trackers). *)
val announce_urls : t -> Uri_set.t

(** Get the info hash.

    {b Note} that invalid info hash's in a torrent are {b not} supported.  This
    module makes no attempt to perform error checking for this case.  *)
val info_hash : t -> Sha1.t

(** Get the piece length, this is exposed as an int because each piece should be
    small. *)
val piece_length : t -> int

(** Get the files to be downloaded.  The list returned is a singleton if there
    is only one file to download.  The order of the list matches the order
    specified in the torrent. *)
val files : t -> File.t list

(** List of the hashes of each piece.  These match the order given in the pieces
    string in the torrent. *)
val piece_hashes : t -> string list
