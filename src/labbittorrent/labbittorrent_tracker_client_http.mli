module Make (Abb : Abb_intf.S with type Native.t = Unix.file_descr) : sig
  val announce : Uri.t Labbittorrent_tracker_client.Make(Abb.Future).announce
end
