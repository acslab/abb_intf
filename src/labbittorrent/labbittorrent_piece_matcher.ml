module List = ListLabels

module Offset = struct
  type t = int

  external of_int : int -> t = "%identity"
  external to_int : t -> int = "%identity"
end

module Length = struct
  type t = int

  external of_int : int -> t = "%identity"
  external to_int : t -> int = "%identity"
end

type 'a t = { id_map : (Offset.t, 'a) Hashtbl.t
            ; offsets :  Offset.t array
            }

let create ls =
  assert (ls <> []);
  { id_map = CCHashtbl.of_list ls
  ; offsets =  Array.of_list (List.sort ~cmp:compare (List.map ~f:fst ls))
  }

let last_file offset len t =
  let file_offset = t.offsets.(Array.length t.offsets - 1) in
  let id = Hashtbl.find t.id_map file_offset in
  [ (id, offset - file_offset, len) ]

let rec mtch (offset, len) t =
  assert (offset >= 0);
  assert (len > 0);
  let s = CCArray.bsearch offset t.offsets in
  match s with
    | `All_bigger ->
      (* The file offsets must always start at zero and the given offset must be
         positive so we can never input an offset less than zero. *)
      assert false
    | `All_lower ->
      (* In the last file *)
      last_file offset len t
    | `At idx when idx = Array.length t.offsets - 1 ->
      (* In the last file *)
      last_file offset len t
    | `Just_after idx
    | `At idx ->
      let file_offset = t.offsets.(idx) in
      let piece_offset = offset - file_offset in
      let id = Hashtbl.find t.id_map file_offset in
      let next_file_offset = t.offsets.(idx + 1) in
      if offset + len <= next_file_offset then
        [ (id, piece_offset, len) ]
      else
        let len' = next_file_offset - offset in
        (id, piece_offset, len')::mtch (next_file_offset, len - len') t
    | `Empty ->
      []
