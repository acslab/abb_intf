module Bytes_stupid = Bytes
module Bytes = BytesLabels

module Frame = struct
  module Request = struct
    type t = { index : int
             ; start : int
             ; length : int
             }
  end

  type t = [
    | `Keepalive
    | `Choke
    | `Unchoke
    | `Interested
    | `Uninterested
    | `Have of int
    | `Bitfield of Bytes.t
    | `Have_all
    | `Have_none
    | `Request of Request.t
    | `Piece of (Request.t * Bytes.t)
    | `Cancel of Request.t
    | `Suggest_piece of int
    | `Reject_request of Request.t
    | `Allowed_fast of int
  ]
end

let protocol_version = "BitTorrent protocol"

module Reader = struct
  type error = [ `Malformed ]

  (* 1 for length, 19 for "BitTorrent protocol", 8 for the reserved bytes, and
     20 for the info hash and 20 for the peer id. *)
  let handshake_length = 1 + 19 + 8 + 20 + 20

  let decode_handshake ~pos ~len b =
    assert (len = handshake_length);
    let len_prefix_cond = Char.code (Bytes.get b 0) = 19 in
    let protocol_version_cond = Bytes.sub_string b 1 19 = "BitTorrent protocol" in
    let fast_extension = (Char.code (Bytes.get b (1 + 19 + 7)) land 0x04) = 0x04 in
    if len_prefix_cond && protocol_version_cond && fast_extension then begin
      let info_hash = Bytes.sub_string b (1 + 19 + 8) 20 in
      let peer_id = Bytes.sub_string b (1 + 19 + 8 + 20) 20 in
      Ok (info_hash, peer_id)
    end
    else
      Error `Malformed

  let prepend_frame f = function
    | Ok (fs, pos) -> Ok (f::fs, pos)
    | Error _ as err -> err

  let decode_frame frame_len ~pos ~len b k =
    let frame_beginning = pos - 4 in
    let pos' = pos + frame_len in
    let len' = len - frame_len in
    match Char.code (Bytes.get b pos) with
      | 0 ->
        prepend_frame `Choke (k ~pos:pos' ~len:len' b)
      | 1 ->
        prepend_frame `Unchoke (k ~pos:pos' ~len:len' b)
      | 2 ->
        prepend_frame `Interested (k ~pos:pos' ~len:len' b)
      | 3 ->
        prepend_frame `Uninterested (k ~pos:pos' ~len:len' b)
      | 4 when len >= frame_len ->
        let idx = Int32.to_int (EndianBytes.BigEndian.get_int32 b (pos + 1)) in
        prepend_frame (`Have idx) (k ~pos:pos' ~len:len' b)
      | 4 ->
        Ok ([], frame_beginning)
      | 5 when len >= frame_len ->
        let bf = Bytes.sub b ~pos:(pos + 1) ~len:(frame_len - 1) in
        prepend_frame (`Bitfield bf) (k ~pos:pos' ~len:len' b)
      | 5 ->
        Ok ([], frame_beginning)
      | 0x0E ->
        prepend_frame `Have_all (k ~pos:pos' ~len:len' b)
      | 0x0F ->
        prepend_frame `Have_none (k ~pos:pos' ~len:len' b)
      | 6 when len >= frame_len ->
        let index = Int32.to_int (EndianBytes.BigEndian.get_int32 b (pos + 1)) in
        let start = Int32.to_int (EndianBytes.BigEndian.get_int32 b (pos + 1 + 4)) in
        let length = Int32.to_int (EndianBytes.BigEndian.get_int32 b (pos + 1 + 4 + 4)) in
        let req = Frame.Request.({ index; start; length }) in
        prepend_frame (`Request req) (k ~pos:pos' ~len:len' b)
      | 6 ->
        Ok ([], frame_beginning)
      | 7 when len >= frame_len ->
        let index = Int32.to_int (EndianBytes.BigEndian.get_int32 b (pos + 1)) in
        let start = Int32.to_int (EndianBytes.BigEndian.get_int32 b (pos + 1 + 4)) in
        let piece = Bytes.sub b ~pos:(pos + 1 + 4 + 4) ~len:(frame_len - 1 - 4 - 4) in
        let req = Frame.Request.({ index; start; length = Bytes.length piece }) in
        prepend_frame (`Piece (req, piece)) (k ~pos:pos' ~len:len' b)
      | 7 ->
        Ok ([], frame_beginning)
      | 8 when len >= frame_len ->
        let index = Int32.to_int (EndianBytes.BigEndian.get_int32 b (pos + 1)) in
        let start = Int32.to_int (EndianBytes.BigEndian.get_int32 b (pos + 1 + 4)) in
        let length = Int32.to_int (EndianBytes.BigEndian.get_int32 b (pos + 1 + 4 + 4)) in
        let req = Frame.Request.({ index; start; length }) in
        prepend_frame (`Cancel req) (k ~pos:pos' ~len:len' b)
      | 8 ->
        Ok ([], frame_beginning)
      | 0x0D when len >= frame_len ->
        let idx = Int32.to_int (EndianBytes.BigEndian.get_int32 b (pos + 1)) in
        prepend_frame (`Suggest_piece idx) (k ~pos:pos' ~len:len' b)
      | 0x0D ->
        Ok ([], frame_beginning)
      | 0x10 when len >= frame_len ->
        let index = Int32.to_int (EndianBytes.BigEndian.get_int32 b (pos + 1)) in
        let start = Int32.to_int (EndianBytes.BigEndian.get_int32 b (pos + 1 + 4)) in
        let length = Int32.to_int (EndianBytes.BigEndian.get_int32 b (pos + 1 + 4 + 4)) in
        let req = Frame.Request.({ index; start; length }) in
        prepend_frame (`Reject_request req) (k ~pos:pos' ~len:len' b)
      | 0x10 ->
        Ok ([], frame_beginning)
      | 0x11 when len >= frame_len ->
        let idx = Int32.to_int (EndianBytes.BigEndian.get_int32 b (pos + 1)) in
        prepend_frame (`Allowed_fast idx) (k ~pos:pos' ~len:len' b)
      | 0x11 ->
        Ok ([], frame_beginning)
      | _ ->
        Error `Malformed

  let rec decode_connected ~pos ~len b : ((Frame.t list * int), [> error ]) result =
    if len < 4 then
      Ok ([], pos)
    else begin
      let length = Int32.to_int (EndianBytes.BigEndian.get_int32 b pos) in
      match length with
        | 0 ->
          prepend_frame `Keepalive (decode_connected ~pos:(pos + 4) ~len:(len - 4) b)
        | n when len >= n ->
          decode_frame n ~pos:(pos + 4) ~len:(len - 4) b decode_connected
        | _ ->
          Ok ([], pos)
    end
end

module Writer = struct
  let int32 buf n =
    let len = 4 in
    let b = Bytes.create len in
    EndianBytes.BigEndian.set_int32 b 0 n;
    Buffer.add_bytes buf b

  let int8 buf n =
    Buffer.add_char buf (Char.chr n)

  let handshake_header_length = 1 + 19 + 8

  let handshake_header_bytes =
    let b = Bytes.create handshake_header_length in
    Bytes.set b 0 (Char.chr 19);
    Bytes_stupid.blit_string protocol_version 0 b 1 (String.length protocol_version);
    Bytes.fill b ~pos:(1 + 19) ~len:8 (Char.chr 0);
    Bytes.set b (1 + 19 + 7) (Char.chr 0x04);
    b

  let handshake_header b ~pos =
    Bytes.blit
      ~src:handshake_header_bytes
      ~src_pos:0
      ~dst:b
      ~dst_pos:pos
      ~len:handshake_header_length

  let info_hash_length = 20

  let info_hash_header b ~pos hash =
    Bytes_stupid.blit_string
      hash
      0
      b
      pos
      info_hash_length

  let peer_id_length = 20

  let peer_id_header b ~pos hash =
    Bytes_stupid.blit_string
      hash
      0
      b
      pos
      peer_id_length

  let encode buf = function
    | `Keepalive ->
      int32 buf 0l
    | `Choke ->
      int32 buf 1l;
      int8 buf 0
    | `Unchoke ->
      int32 buf 1l;
      int8 buf 1
    | `Interested ->
      int32 buf 1l;
      int8 buf 2
    | `Uninterested ->
      int32 buf 1l;
      int8 buf 3
    | `Have idx ->
      int32 buf 5l;
      int8 buf 4;
      int32 buf (Int32.of_int idx)
    | `Bitfield bf ->
      int32 buf (Int32.of_int (1 + Bytes.length bf));
      int8 buf 5;
      Buffer.add_bytes buf bf
    | `Have_all ->
      int32 buf 1l;
      int8 buf 0x0E
    | `Have_none ->
      int32 buf 1l;
      int8 buf 0x0F
    | `Request req ->
      int32 buf 0x0Dl;
      int8 buf 6;
      int32 buf (Int32.of_int req.Frame.Request.index);
      int32 buf (Int32.of_int req.Frame.Request.start);
      int32 buf (Int32.of_int req.Frame.Request.length)
    | `Piece (req, piece) ->
      assert (req.Frame.Request.length = Bytes.length piece);
      int32 buf (Int32.of_int (1 + 4 + 4 + Bytes.length piece));
      int8 buf 7;
      int32 buf (Int32.of_int req.Frame.Request.index);
      int32 buf (Int32.of_int req.Frame.Request.start);
      Buffer.add_bytes buf piece
    | `Cancel req ->
      int32 buf 0x0Dl;
      int8 buf 8;
      int32 buf (Int32.of_int req.Frame.Request.index);
      int32 buf (Int32.of_int req.Frame.Request.start);
      int32 buf (Int32.of_int req.Frame.Request.length)
    | `Suggest_piece idx ->
      int32 buf 5l;
      int8 buf 0x0D;
      int32 buf (Int32.of_int idx)
    | `Reject_request req ->
      int32 buf 0x0Dl;
      int8 buf 0x10;
      int32 buf (Int32.of_int req.Frame.Request.index);
      int32 buf (Int32.of_int req.Frame.Request.start);
      int32 buf (Int32.of_int req.Frame.Request.length)
    | `Allowed_fast idx ->
      int32 buf 5l;
      int8 buf 0x11;
      int32 buf (Int32.of_int idx)
end

