(** Peer connection handles the post-handshake portion of a peer connection.
    This module provides an interface to a single peer.  The peer has little
    intelligence in it other than respecting the peer's choke/unchoke and
    interested/uninterested state.  It also processes requests with a maximum
    outstanding request depth.  Each request will only be processed once and
    become a success or failed and must be explicitly retried if it fails. *)

type request = Labbittorrent_peer_protocol.Frame.Request.t

module Request_set : Set.S with type elt := request

module Make (Abb : Abb_intf.S) : sig
  type serializer = Abb.File.t Abb_service_serializer.Make(Abb.Future).Mutex.t
  type matcher = serializer Labbittorrent_piece_matcher.t

  (** The callback type for when a peer has a new piece.  All of the pieces the
      peer has are included in each call. *)
  type peer_has_cb = (Set.Make(CCInt).t -> unit Abb.Future.t)

  type msg

  type t = (Abb_channel.Make(Abb.Future).writer, msg) Abb_channel.Make(Abb.Future).t

  type init_args = { conn : Abb.Socket.tcp Abb.Socket.t (** Peer connection socket *)
                   ; name : string (** Name of this connection, for logging *)
                   ; num_pieces : int (** Number of pieces in the torrent *)
                   ; piece_length : int (** The length of each piece *)
                   ; have : Set.Make(CCInt).t (** The pieces this peer already has *)
                   ; peer_has_cb : peer_has_cb (** Callback when the peer has a new piece *)
                   ; matcher : matcher (** Matcher for the file writes *)
                   }

  val create : init_args -> t Abb.Future.t

  (** Inform the peer connection that we have a piece.  This was probably
      downloaded through another peer. *)
  val have : t -> int -> unit Abb_channel_intf.channel_ret Abb.Future.t

  (** The set of pieces a peer has. *)
  val peer_has : t -> Set.Make(CCInt).t Abb_channel_intf.channel_ret Abb.Future.t

  (** Tell the peer to attempt to download the set of requests. A request set is
      unordered but order of downloads between calls to [request] are
      guaranteed.  That is, if [request] is called twice, the requests in the
      first call will be completely handled prior to those in the second call.
      The function is called with the result of each attempted request.  The
      function is not called if the request is canceled. A request can fail if
      the other side rejects it. If the peer is choking when this is called, the
      callback will be immediately called with an error on the request. *)
  val request :
    t ->
    f:((request, request) result -> unit Abb.Future.t) ->
    Request_set.t ->
    unit Abb_channel_intf.channel_ret Abb.Future.t

  (** Cancel previously requested requests.  This does not result in the
      requests callback being executed.  On going requests will be canceled
      however they could already be in the process of being transferred so there
      is no guarantee of truly being canceled. *)
  val cancel : t -> Request_set.t -> unit Abb_channel_intf.channel_ret Abb.Future.t

  (** Set that this side of the peer connection is choking.  The system will
      wait 10 seconds before it begins rejecting requests. *)
  val set_choking : t -> bool -> bool Abb_channel_intf.channel_ret Abb.Future.t

  val set_interested : t -> bool -> bool Abb_channel_intf.channel_ret Abb.Future.t

  val peer_choking : t -> bool Abb_channel_intf.channel_ret Abb.Future.t

  val peer_interested : t -> bool Abb_channel_intf.channel_ret Abb.Future.t

  (** Number of bytes in the torrent that have been downloaded. *)
  val downloaded : t -> int Abb_channel_intf.channel_ret Abb.Future.t

  (** Number of bytes in the torrent that have been uploaded. *)
  val uploaded : t -> int Abb_channel_intf.channel_ret Abb.Future.t
end
