(** Service for the client of a tracker.  This provides the police around
    updating information from a tracker.  The service is agnostic to the
    underlying tracker protocol, which the [updater] function handles. *)

(** A key is associated with a peer so only it can update its status with the
    tracker. *)
module Key : sig
    type t
    val to_int32 : t -> int32
  end

module Make (Fut : Abb_intf.Future.S) : sig
  type msg

  type t = (Abb_channel.Make(Fut).writer, msg) Abb_channel.Make(Fut).t

  (** Init arguments.  These are scheduling functions the service will need. *)
  type 'a init_args = { tracker : 'a (** The tracker. *)
                      ; name : string (** Name of the tracker *)
                      ; sleep : (float -> unit Fut.t) (** Sleep, in seconds *)
                      ; now : (unit -> float Fut.t) (** The current time *)
                      }

  (** Client state *)
  type client_s = { info_hash : Sha1.t
                  ; peer_id : string
                  ; ip : Unix.inet_addr option
                  ; port : int
                  ; uploaded : int
                  ; downloaded : int
                  ; left : int
                  }

  (** Announce response *)
  type announce_resp = { leechers : int
                       ; seeders : int
                       ; peers: Labbittorrent_peer_set.t
                       ; interval : int
                       }

  type 'a announce =
      ('a ->
       Key.t ->
       client_s ->
       (announce_resp, [ `Timedout ]) result Fut.t)

  (** Create a tracker client.  The [client_s] is current client state and can
      be updated with the various setters.  The [announce] function takes a
      client state and returns the current peer information. *)
  val create : 'a init_args -> client_s -> 'a announce -> t Fut.t

  (** Force the client to refresh the tracker. The future is determined when the
      tracker has been updated. *)
  val refresh : t -> unit Abb_channel_intf.channel_ret Fut.t

  (** Set the amount downloaded so far.  This is not the amount of the torrent
      downloaded but the total number of bytes that have been downloaded. *)
  val set_downloaded : t -> int -> unit Abb_channel_intf.channel_ret Fut.t

  (** Set how many bytes have been uploaded. *)
  val set_uploaded : t -> int -> unit Abb_channel_intf.channel_ret Fut.t

  (** Set how much of the torrent is left to download. *)
  val set_left : t -> int -> unit Abb_channel_intf.channel_ret Fut.t

  (** Get the current state of the tracker, if it is in an Ok state or has failed.
      If it ha failed, for how many seconds has it failed. *)
  val get_tracker_state : t -> [ `Ok | `Failed of int ] Abb_channel_intf.channel_ret Fut.t

  (** Get the number of known leechers. Leechers are peers that have not
      completely downloaded the torrent. *)
  val get_leechers : t -> int Abb_channel_intf.channel_ret Fut.t

  (** Get the number of known seeders. Seeders are peers that have completely
      downloaded the torrent. *)
  val get_seeders : t -> int Abb_channel_intf.channel_ret Fut.t

  (** Get the known list of peers. This will be the latest set of peers given
      from the tracker. *)
  val get_peers : t -> Labbittorrent_peer_set.t Abb_channel_intf.channel_ret Fut.t
end
