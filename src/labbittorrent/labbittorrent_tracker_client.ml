module Peer_set = Labbittorrent_peer_set

module Key = struct
  type t = int32

  let create () = Random.int32 Int32.max_int

  let to_int32 t = t
end

module Make (Fut : Abb_intf.Future.S) = struct
  open Fut.Infix_monad

  module Channel = Abb_channel.Make(Fut)
  module Service_local = Abb_service_local.Make(Fut)
  module Fut_comb = Abb_future_combinators.Make(Fut)

  type msg =
    | Refresh of unit Fut.Promise.t
    | Set_downloaded of int
    | Set_uploaded of int
    | Set_left of int
    | Get_tracker_state of [ `Ok | `Failed of int ] Fut.Promise.t
    | Get_leechers of int Fut.Promise.t
    | Get_seeders of int Fut.Promise.t
    | Get_peers of Peer_set.t Fut.Promise.t

  type t = (Abb_channel.Make(Fut).writer, msg) Abb_channel.Make(Fut).t

  type 'a init_args = { tracker : 'a
                      ; name : string
                      ; sleep : (float -> unit Fut.t)
                      ; now : (unit -> float Fut.t)
                      }

  type client_s = { info_hash : Sha1.t
                  ; peer_id : string
                  ; ip : Unix.inet_addr option
                  ; port : int
                  ; uploaded : int
                  ; downloaded : int
                  ; left : int
                  }

  type announce_resp = { leechers : int
                       ; seeders : int
                       ; peers : Peer_set.t
                       ; interval : int
                       }

  type 'a announce =
      ('a ->
       Key.t ->
       client_s ->
       (announce_resp, [ `Timedout ]) result Fut.t)

  module Server = struct
    type 'a t = { s : client_s
                ; key : Key.t
                ; tracker : 'a
                ; name : string
                ; leechers : int
                ; seeders : int
                ; peers : Peer_set.t
                ; announce : 'a announce
                ; last_update : [ ` Ok | `Failed of float ]
                ; update_timer : unit Fut.t
                ; writer : (Channel.writer, msg) Channel.t
                ; reader : (Channel.reader, msg) Channel.t
                ; sleep : (float -> unit Fut.t)
                ; now : (unit -> float Fut.t)
                }

    let do_announce client_s key tracker announce sleep =
      Fut.await
        (Fut_comb.first
           (announce tracker key client_s)
           (sleep 60.0 >>| fun () -> Error `Timedout))
      >>= function
      | `Det (v, fut) ->
        Fut.abort fut
        >>| fun () ->
        v
      | `Aborted | `Exn _ ->
        Fut.return (Error `Timedout)

    let do_update t =
      Fut.abort t.update_timer
      >>= fun () ->
      do_announce t.s t.key t.tracker t.announce t.sleep
      >>= function
      | Ok resp ->
        Logs.debug
          (fun m -> m "%s: Updated successfully, retry in %d" t.name resp.interval);
        let timer = t.sleep (float resp.interval) in
        Fut.return
          { t with
              leechers = resp.leechers;
              seeders = resp.seeders;
              peers = resp.peers;
              last_update = `Ok;
              update_timer = timer
          }
      | Error `Timedout ->
        Logs.debug (fun m -> m "%s: Refresh failed" t.name);
        let timer = t.sleep 60.0 in
        t.now ()
        >>| fun now ->
        begin match t.last_update with
          | `Ok ->
            { t with
                update_timer = timer;
                last_update = `Failed now
            }
          | `Failed _ ->
            { t with update_timer = timer }
        end

    let loop t = function
      | Refresh p ->
        do_update t
        >>= fun t ->
        Fut.fork
          (t.update_timer
           >>| fun () ->
           Channel.send t.writer (Refresh (Fut.Promise.create ())))
        >>= fun () ->
        Fut.Promise.set p ()
        >>| fun () ->
        t
      | Set_downloaded bytes ->
        Fut.return {t with s = {t.s with downloaded = bytes}}
      | Set_uploaded bytes ->
        Fut.return {t with s = {t.s with uploaded = bytes}}
      | Set_left bytes ->
        Fut.return {t with s = {t.s with left = bytes}}
      | Get_tracker_state p ->
        t.now ()
        >>= fun now ->
        begin match t.last_update with
          | `Ok -> Fut.Promise.set p `Ok
          | `Failed since -> Fut.Promise.set p (`Failed (int_of_float (now -. since)))
        end
        >>| fun () ->
        t
      | Get_leechers p ->
        Fut.Promise.set p t.leechers
        >>| fun () ->
        t
      | Get_seeders p ->
        Fut.Promise.set p t.seeders
        >>| fun () ->
        t
      | Get_peers p ->
        Fut.Promise.set p t.peers
        >>| fun () ->
        t

    let init (init_args : 'a init_args) client_s announce writer reader =
      ignore (Channel.send writer (Refresh (Fut.Promise.create ())));
      let t = { s = client_s
              ; key = Key.create ()
              ; tracker = init_args.tracker
              ; name = init_args.name
              ; leechers = 0
              ; seeders = 0
              ; peers = Peer_set.empty
              ; announce = announce
              ; last_update = `Ok
              ; update_timer = Fut.Promise.future (Fut.Promise.create ())
              ; writer = writer
              ; reader = reader
              ; sleep = init_args.sleep
              ; now = init_args.now
              }
      in
      Channel.Combinators.fold ~f:loop ~init:t reader
      >>| fun _ ->
      ()
  end

  let create init_args client_s announce =
    Service_local.create (Server.init init_args client_s announce)

  let refresh t =
    let p = Fut.Promise.create () in
    Channel.send t (Refresh p)
    >>= function
    | `Closed ->
      Fut.return `Closed
    | `Ok () ->
      Fut.Promise.future p
      >>| fun v ->
      `Ok v

  let set_downloaded t bytes = Channel.send t (Set_downloaded bytes)

  let set_uploaded t bytes = Channel.send t (Set_uploaded bytes)

  let set_left t bytes = Channel.send t (Set_left bytes)

  let get_tracker_state t =
    let p = Fut.Promise.create () in
    Channel.send t (Get_tracker_state p)
    >>= function
    | `Closed ->
      Fut.return `Closed
    | `Ok () ->
      Fut.Promise.future p
      >>| fun v ->
      `Ok v

  let get_leechers t =
    let p = Fut.Promise.create () in
    Channel.Combinators.send_promise t (Get_leechers p) p

  let get_seeders t =
    let p = Fut.Promise.create () in
    Channel.Combinators.send_promise t (Get_seeders p) p

  let get_peers t =
    let p = Fut.Promise.create () in
    Channel.Combinators.send_promise t (Get_peers p) p
end
