module Make (Abb : Abb_intf.S) : sig
  val announce : Abb_intf.Socket.Sockaddr.t Labbittorrent_tracker_client.Make(Abb.Future).announce
end
