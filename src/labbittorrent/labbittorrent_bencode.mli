type t

module View : sig
  type t =
    | String of string
    | Int of Num.num
    | Dict of t Map.Make(String).t
    | List of t list
end

(** Lens interface for the {! View.t}. *)
module Lens : sig
  (** {2 Type conversions}

      In all cases [Invalid_argument] is raised if the type does not match, where the
      argument to [Invalid_argument] is the name of the conversion function
      being applied. *)

  (** Convert a view to a dict. *)
  val dict : (View.t, View.t Map.Make(String).t) Lens.t

  (** Convert a view to a list. *)
  val list : (View.t, View.t list) Lens.t

  (** Convert a view to an int. *)
  val int : (View.t, Num.num) Lens.t

  (** Convert a view to a string. *)
  val string : (View.t, string) Lens.t

  (** Convert a view that is a list and apply the lens to each element of the
      lens returning a list of the the elements. *)
  val list_of : (View.t, 'a) Lens.t -> (View.t, 'a list) Lens.t


  (** {2 Accessors} *)

  (** Access the key of a dict. Throws [Not_found] if the key does not exist. *)
  val key : string -> (View.t Map.Make(String).t, View.t) Lens.t

  (** Access the key of a dict and wrap it in an option. *)
  val key_opt : string -> (View.t Map.Make(String).t, View.t option) Lens.t
end


val of_bytes : ?pos:int -> bytes -> (int * t)
val to_bytes : t -> bytes

val to_view : t -> View.t
val of_view : View.t -> t

