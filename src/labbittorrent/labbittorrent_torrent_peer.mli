module Make (Abb : Abb_intf.S) : sig
  module Tracker_client : module type of Labbittorrent_tracker_client.Make(Abb.Future)

  type msg

  type t = (Abb_channel.Make(Abb.Future).writer, msg) Abb_channel.Make(Abb.Future).t

  type init_args = { tracker : Tracker_client.t
                   ; torrent : Labbittorrent_torrent.t
                   ; download_dir : string
                   ; peer_id : string
                   }

  val create : init_args -> t Abb.Future.t
end
