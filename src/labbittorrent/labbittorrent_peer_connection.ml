module List = ListLabels

module Int_set = Set.Make(CCInt)

module Peer_protocol = Labbittorrent_peer_protocol
module Piece_matcher = Labbittorrent_piece_matcher
module Request = Labbittorrent_peer_protocol.Frame.Request

type request = Labbittorrent_peer_protocol.Frame.Request.t

module Request_set = Set.Make(struct
    type t = request
    let compare = compare
  end)

module Request_map = CCMap.Make(struct
    type t = request
    let compare = compare
  end)

module Make (Abb : Abb_intf.S) = struct
  open Abb.Future.Infix_monad

  module Channel = Abb_channel.Make(Abb.Future)
  module Service_local = Abb_service_local.Make(Abb.Future)
  module Serializer = Abb_service_serializer.Make(Abb.Future)
  module Fut_comb = Abb_future_combinators.Make(Abb.Future)

  type serializer = Abb.File.t Serializer.Mutex.t
  type matcher = serializer Piece_matcher.t

  type peer_has_cb = (Set.Make(CCInt).t -> unit Abb.Future.t)

  type request_cb = ((request, request) result -> unit Abb.Future.t)

  type msg =
    | Have of int
    | Got_bytes of (Bytes.t * int)
    | Peer_keepalive
    | Keepalive
    | Peer_has of Int_set.t Abb.Future.Promise.t
    | Request of (request_cb * Request_set.t)
    | Cancel of Request_set.t
    | Set_choking of (bool * bool Abb.Future.Promise.t)
    | Set_interested of (bool * bool Abb.Future.Promise.t)
    | Peer_choking of bool Abb.Future.Promise.t
    | Peer_interested of bool Abb.Future.Promise.t
    | Downloaded of int Abb.Future.Promise.t
    | Uploaded of int Abb.Future.Promise.t
    | Cancel_requests
    | Send_next_peer_request

  type t = (Channel.writer, msg) Channel.t

  type init_args = { conn : Abb.Socket.tcp Abb.Socket.t
                   ; name : string
                   ; num_pieces : int
                   ; piece_length : int
                   ; have : Int_set.t
                   ; peer_has_cb : (Set.Make(CCInt).t -> unit Abb.Future.t)
                   ; matcher : matcher
                   }

  module Server = struct
    type t = { conn : Abb.Socket.tcp Abb.Socket.t
             ; name : string
             ; num_pieces : int
             ; piece_length : int
             ; allowed_fast : Int_set.t
             ; have : Int_set.t
             ; peer_has : Int_set.t
             ; peer_requests : request CCFQueue.t
             ; peer_choking : bool
             ; peer_interested : bool
             ; peer_has_cb : peer_has_cb
             ; requests: (request * request_cb) CCFQueue.t
             ; ongoing : request_cb Request_map.t
             ; choking : bool
             ; interested : bool
             ; matcher : matcher
             ; writer : (Channel.writer, msg) Channel.t
             ; buffer : Buffer.t
             ; peer_keepalive_timer : unit Abb.Future.t
             ; keepalive_timer : unit Abb.Future.t
             ; downloaded : int
             ; uploaded : int
             ; cancel_request_timer : unit Abb.Future.t
             }

    let max_queue_length = 10
    let default_len = 4096

    let rec read_bytes writer conn =
      let buf = Bytes.create default_len in
      Abb.Socket.Tcp.recv conn ~buf ~pos:0 ~len:default_len
      >>= function
      | Ok n ->
        (Channel.send writer (Got_bytes (buf, n))
         >>= function
         | `Closed ->
           Abb.Future.return ()
         | `Ok () ->
           read_bytes writer conn)
      | Error _ ->
        Channel.close writer

    let rec send_all conn ~buf ~pos ~len =
      let bufs = Abb_intf.Write_buf.([ { buf; pos; len } ]) in
      Abb.Socket.Tcp.send conn ~bufs
      >>= function
      | Ok n when n < len ->
        send_all conn ~buf ~pos:(pos + n) ~len:(len - n)
      | Ok _ ->
        Abb.Future.return ()
      | Error _ ->
        failwith "send_all failed"

    let request_callback req v t =
      match Request_map.get req t.ongoing with
        | Some f ->
          let t = { t with ongoing = Request_map.remove req t.ongoing } in
          f v
          >>| fun () ->
          t
        | None ->
          assert false

    let maybe_do_requests t =
      match max_queue_length - Request_map.cardinal t.ongoing  with
        | 0 ->
          Abb.Future.return t
        | n when n > 0 ->
          begin match CCFQueue.take_front_l n t.requests with
            | ([], _) ->
              Abb.Future.return t
            | (l, requests) ->
              let buf = Buffer.create 10 in
              List.iter
                ~f:(fun (req, _) ->
                    let frame = `Request req in
                    Peer_protocol.Writer.encode buf frame)
                l;
              let b = Buffer.to_bytes buf in
              send_all t.conn ~buf:b ~pos:0 ~len:(Bytes.length b)
              >>| fun () ->
              let ongoing =
                List.fold_left
                  ~f:(fun acc (req, f) -> Request_map.add req f acc)
                  ~init:t.ongoing
                  l
              in
              { t with requests; ongoing }
          end
        | _ ->
          assert false

    let update_peer_has peer_has t =
      t.peer_has_cb peer_has
      >>| fun () ->
      { t with peer_has }

    let peer_keepalive_timer writer =
      Abb.Sys.sleep (5.0 *. 60.0)
      >>| fun () ->
      ignore (Channel.send writer Peer_keepalive)

    let keepalive_timer writer =
      Abb.Sys.sleep (4.0 *. 60.0)
      >>| fun () ->
      ignore (Channel.send writer Keepalive)

    let process_keepalive t =
      Abb.Future.abort t.peer_keepalive_timer
      >>= fun () ->
      Abb.Future.return { t with peer_keepalive_timer = peer_keepalive_timer t.writer }

    let process_choke t =
      Abb.Future.return { t with peer_choking = true }

    let process_unchoke t =
      Abb.Future.return { t with peer_choking = false }

    let process_interested t =
      Abb.Future.return { t with peer_interested = true }

    let process_uninterested t =
      Abb.Future.return { t with peer_interested = false }

    let process_have t idx =
      assert (idx >= 0);
      assert (idx < t.num_pieces);
      update_peer_has (Int_set.add idx t.peer_has) t

    let process_bitfield t bf =
      let rec f acc = function
        | idx when idx < Bytes.length bf ->
          let offset = idx * 8 in
          let n = Char.code (Bytes.get bf idx) in
          let acc =
            List.fold_left
              ~f:(fun acc bit ->
                  if n land (1 lsl bit) > 0 then
                    Int_set.add (offset + bit) acc
                  else
                    acc)
              ~init:acc
              [0; 1; 2; 3; 4; 5; 6; 7]
          in
          f acc (idx + 1)
        | _ ->
          acc
      in
      let peer_has = f Int_set.empty 0 in
      assert (not (Int_set.is_empty peer_has) && Int_set.min_elt peer_has >= 0);
      assert (not (Int_set.is_empty peer_has) && Int_set.max_elt peer_has < t.num_pieces);
      update_peer_has peer_has t

    let process_have_all t =
      let rec f acc = function
        | 0 -> Int_set.add 0 acc
        | n -> f (Int_set.add n acc) (n - 1)
      in
      update_peer_has (f Int_set.empty (t.num_pieces - 1)) t

    let process_have_none t =
      update_peer_has Int_set.empty t

    let process_request t req =
      ignore (Channel.send t.writer Send_next_peer_request);
      Abb.Future.return { t with peer_requests = CCFQueue.snoc t.peer_requests req }

    let process_piece t req piece =
      Logs.debug
        (fun m ->
           m "%s: Got request: index=%d start=%d length=%d"
             t.name
             req.Request.index
             req.Request.start
             req.Request.length);
      assert (req.Request.length = Bytes.length piece);
      let offset =
        Piece_matcher.Offset.of_int (req.Request.index * t.piece_length + req.Request.start)
      in
      let length = Piece_matcher.Length.of_int req.Request.length in
      let mtches = Piece_matcher.mtch (offset, length) t.matcher in
      Fut_comb.ignore
        (Fut_comb.List.fold_left
           ~f:(fun pos (s, offset, length) ->
               let bufs =
                 Abb_intf.Write_buf.([ { buf = piece
                                       ; pos = pos
                                       ; len = Piece_matcher.Length.to_int length
                                       }
                                     ])
               in
               Serializer.Mutex.run
                 s
                 ~f:(fun file ->
                     Abb.File.pwrite
                       file
                       ~offset:(Piece_matcher.Offset.to_int offset)
                       bufs)
               >>| function
               | `Ok _ ->
                 pos + Piece_matcher.Length.to_int length
               | `Closed ->
                 assert false)
           ~init:0
           mtches)
      >>= fun () ->
      let t = { t with downloaded = t.downloaded + req.Request.length } in
      request_callback req (Ok req) t
      >>= fun t ->
      maybe_do_requests t

    let process_cancel t req =
      let peer_requests =
        t.peer_requests
        |> CCFQueue.to_list
        |> List.filter ~f:(fun r -> r <> req)
        |> CCFQueue.of_list
      in
      Abb.Future.return { t with peer_requests }

    let process_reject_request t req =
      request_callback req (Error req) t

    let process_allowed_fast t idx =
      Abb.Future.return { t with allowed_fast = Int_set.add idx t.allowed_fast }

    let process_frame t = function
      | `Keepalive -> process_keepalive t
      | `Choke -> process_choke t
      | `Unchoke -> process_unchoke t
      | `Interested -> process_interested t
      | `Uninterested -> process_uninterested t
      | `Have idx -> process_have t idx
      | `Bitfield bf -> process_bitfield t bf
      | `Have_all -> process_have_all t
      | `Have_none -> process_have_none t
      | `Request req -> process_request t req
      | `Piece (req, piece) -> process_piece t req piece
      | `Cancel req -> process_cancel t req
      | `Suggest_piece _ -> Abb.Future.return t
      | `Reject_request req -> process_reject_request t req
      | `Allowed_fast idx -> process_allowed_fast t idx

    let process_frames t frames =
      Fut_comb.List.fold_left ~f:process_frame ~init:t frames

    let send_frames conn frames =
      let buf = Buffer.create 10 in
      List.iter
        ~f:(Peer_protocol.Writer.encode buf)
        frames;
      let b = Buffer.to_bytes buf in
      send_all conn ~buf:b ~pos:0 ~len:(Bytes.length b)

    let send_frame conn frame = send_frames conn [frame]

    let send_choking conn toggle =
      let frame =
        match toggle with
          | true -> `Choke
          | false -> `Unchoke
      in
      send_frame conn frame

    let send_interested conn toggle =
      let frame =
        match toggle with
          | true -> `Interested
          | false -> `Uninterested
      in
      send_frame conn frame

    let handle_have idx t =
      let have = Int_set.add idx t.have in
      let frame = `Have idx in
      send_frame t.conn frame
      >>| fun () ->
      { t with have = have }

    let handle_got_bytes b len t =
      Buffer.add_subbytes t.buffer b 0 len;
      let bytes = Buffer.to_bytes t.buffer in
      let decoded =
        Peer_protocol.Reader.decode_connected
          ~pos:0
          ~len:(Bytes.length bytes)
          bytes
      in
      begin match decoded with
        | Ok (frames, pos) ->
          Buffer.clear t.buffer;
          if Bytes.length bytes - pos > 0 then
            Buffer.add_subbytes t.buffer bytes pos (Bytes.length bytes - pos);
          process_frames t frames
        | Error _ ->
          failwith "got_bytes failed"
      end

    let handle_peer_keepalive t =
      (* Have not received a keepalive from this peer, kill it. *)
      ignore (Channel.close t.writer);
      Abb.Future.return t

    let handle_keepalive t =
      Logs.debug (fun m -> m "%s: Sending keepalive" t.name);
      let buf = Buffer.create 4 in
      Peer_protocol.Writer.encode buf `Keepalive;
      let b = Buffer.to_bytes buf in
      send_all t.conn ~buf:b ~pos:0 ~len:(Bytes.length b)
      >>| fun () ->
      { t with keepalive_timer = keepalive_timer t.writer }

    let handle_peer_has p t =
      Abb.Future.Promise.set p t.peer_has
      >>= fun () ->
      Abb.Future.return t

    let handle_request f rs t =
      let requests =
        Request_set.fold
          (fun req acc -> CCFQueue.snoc acc (req, f))
          rs
          t.requests
      in
      maybe_do_requests { t with requests }

    let handle_cancel rs t =
      let requests =
        t.requests
        |> CCFQueue.to_list
        |> List.filter ~f:(fun (req, _) -> not (Request_set.mem req rs))
        |> CCFQueue.of_list
      in
      let cancel_request_timer =
        Abb.Sys.sleep 10.0
        >>| fun () ->
        ignore (Channel.send t.writer Cancel_requests)
      in
      Abb.Future.abort t.cancel_request_timer
      >>= fun () ->
      Abb.Future.return { t with requests; cancel_request_timer }

    let handle_set_choking toggle p = function
      | t when t.choking = toggle ->
        Abb.Future.Promise.set p t.choking
        >>| fun () ->
        t
      | t ->
        Abb.Future.Promise.set p t.choking
        >>= fun () ->
        send_choking t.conn toggle
        >>| fun () ->
        { t with choking = toggle }

    let handle_set_interested toggle p = function
      | t when t.interested = toggle ->
        Abb.Future.Promise.set p t.interested
        >>| fun () ->
        t
      | t ->
        Abb.Future.Promise.set p t.interested
        >>= fun () ->
        send_interested t.conn toggle
        >>| fun () ->
        { t with interested = toggle }

    let handle_peer_choking p t =
      Abb.Future.Promise.set p t.peer_choking
      >>| fun () ->
      t

    let handle_peer_interested p t =
      Abb.Future.Promise.set p t.peer_interested
      >>| fun () ->
      t

    let handle_downloaded p t =
      Abb.Future.Promise.set p t.downloaded
      >>| fun () ->
      t

    let handle_uploaded p t =
      Abb.Future.Promise.set p t.uploaded
      >>| fun () ->
      t

    let handle_cancel_requests t =
      let reject_frames =
        t.peer_requests
        |> CCFQueue.to_list
        |> List.map ~f:(fun req -> `Reject_request req)
      in
      send_frames t.conn reject_frames
      >>| fun () ->
      t

    let handle_send_next_peer_request t =
      match CCFQueue.take_front t.peer_requests with
        | Some (req, peer_requests) ->
          Logs.debug
            (fun m ->
               m "%s: Sending request: index=%d start=%d length=%d"
                 t.name
                 req.Request.index
                 req.Request.start
                 req.Request.length);
          let offset =
            Piece_matcher.Offset.of_int (req.Request.index * t.piece_length + req.Request.start)
          in
          let length = Piece_matcher.Length.of_int req.Request.length in
          let mtches = Piece_matcher.mtch (offset, length) t.matcher in
          let b = Bytes.create req.Request.length in
          Fut_comb.ignore
            (Fut_comb.List.fold_left
               ~f:(fun pos (s, offset, length) ->
                   Serializer.Mutex.run
                     s
                     ~f:(fun file ->
                         Abb.File.pread
                           file
                           ~offset:(Piece_matcher.Offset.to_int offset)
                           ~buf:b
                           ~pos
                           ~len:(Piece_matcher.Length.to_int length))
                   >>| function
                   | `Ok _ ->
                     pos + Piece_matcher.Length.to_int length
                   | `Closed ->
                     assert false)
               ~init:0
               mtches)
          >>= fun () ->
          let frame = `Piece (req, b) in
          send_frame t.conn frame
          >>| fun () ->
          ignore (Channel.send t.writer Send_next_peer_request);
          { t with peer_requests }
        | None ->
          Abb.Future.return t

    let loop t = function
      | Have idx -> handle_have idx t
      | Got_bytes (b, len) -> handle_got_bytes b len t
      | Peer_keepalive -> handle_peer_keepalive t
      | Keepalive -> handle_keepalive t
      | Peer_has p -> handle_peer_has p t
      | Request (f, rs) -> handle_request f rs t
      | Cancel rs -> handle_cancel rs t
      | Set_choking (toggle, p) -> handle_set_choking toggle p t
      | Set_interested (toggle, p) -> handle_set_interested toggle p t
      | Peer_choking p -> handle_peer_choking p t
      | Peer_interested p -> handle_peer_interested p t
      | Downloaded p -> handle_downloaded p t
      | Uploaded p -> handle_uploaded p t
      | Cancel_requests -> handle_cancel_requests t
      | Send_next_peer_request -> handle_send_next_peer_request t

    let init (init_args : init_args) writer reader =
      let t = { conn = init_args.conn
              ; name = init_args.name
              ; num_pieces = init_args.num_pieces
              ; piece_length = init_args.piece_length
              ; allowed_fast = Int_set.empty
              ; peer_has = Int_set.empty
              ; peer_requests = CCFQueue.empty
              ; peer_choking = true
              ; peer_interested = false
              ; peer_has_cb = init_args.peer_has_cb
              ; requests = CCFQueue.empty
              ; ongoing = Request_map.empty
              ; choking = true
              ; interested = false
              ; have = init_args.have
              ; matcher = init_args.matcher
              ; writer = writer
              ; buffer = Buffer.create default_len
              ; peer_keepalive_timer = peer_keepalive_timer writer
              ; keepalive_timer = keepalive_timer writer
              ; downloaded = 0
              ; uploaded = 0
              ; cancel_request_timer = Fut_comb.unit
              }
      in
      (* Should be a bitfield but this is a bit simpler *)
      let have_frames =
        t.have
        |> Int_set.elements
        |> List.map ~f:(fun idx -> `Have idx)
      in
      send_frames t.conn have_frames
      >>= fun () ->
      let read_bytes_fut = read_bytes writer init_args.conn in
      let loop_fut =
        Channel.Combinators.fold
          ~f:loop
          ~init:t
          reader
      in
      (* If one fails they both fail *)
      Fut_comb.link read_bytes_fut loop_fut;
      loop_fut
      >>= fun _ ->
      (* Make sure the read_bytes future stops. *)
      Abb.Future.abort read_bytes_fut
  end

  let create (init_args : init_args) =
    Service_local.create (Server.init init_args)

  let have t idx =
    Channel.send t (Have idx)

  let peer_has t =
    let p = Abb.Future.Promise.create () in
    Channel.Combinators.send_promise t (Peer_has p) p

  let request t ~f rs =
    Channel.send t (Request (f, rs))

  let cancel t rs =
    Channel.send t (Cancel rs)

  let set_choking t toggle =
    let p = Abb.Future.Promise.create () in
    Channel.Combinators.send_promise t (Set_choking (toggle, p)) p

  let set_interested t toggle =
    let p = Abb.Future.Promise.create () in
    Channel.Combinators.send_promise t (Set_interested (toggle, p)) p

  let peer_choking t =
    let p = Abb.Future.Promise.create () in
    Channel.Combinators.send_promise t (Peer_choking p) p

  let peer_interested t =
    let p = Abb.Future.Promise.create () in
    Channel.Combinators.send_promise t (Peer_interested p) p

  let downloaded t =
    let p = Abb.Future.Promise.create () in
    Channel.Combinators.send_promise t (Downloaded p) p

  let uploaded t =
    let p = Abb.Future.Promise.create () in
    Channel.Combinators.send_promise t (Uploaded p) p
end
