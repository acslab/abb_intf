(** Stateless decoder and encoder for the peer protocol.  The decoder is broken
    up into handshake and a connected decoders.  The handshake decoder requires
    being given a byte buffer with [handshake_length] bytes filled in.  The
    connected decoder consumes as many frames as it can and returns the position
    in the buffer last read. *)

module Frame : sig
  module Request : sig
    type t = { index : int
             ; start : int
             ; length : int
             }
  end

  type t = [
    | `Keepalive
    | `Choke
    | `Unchoke
    | `Interested
    | `Uninterested
    | `Have of int
    | `Bitfield of Bytes.t
    | `Have_all
    | `Have_none
    | `Request of Request.t
    | `Piece of (Request.t * Bytes.t)
    | `Cancel of Request.t
    | `Suggest_piece of int
    | `Reject_request of Request.t
    | `Allowed_fast of int
  ]
end

module Reader : sig
  (** The input was malformed. *)
  type error = [ `Malformed ]

  (** The size of a whole handshake.  This includes the protocol version,
      reserved bytes, and info_hash. *)
  val handshake_length : int

  (** Decode a handshake, the input buffer must be of length [handshake_length].
      On success, a tuple of the [info_hash] and [peer_id] is returned. If the
      handshake does not have the fast extension or is invalid in any other way,
      [`Malformed] is returned. *)
  val decode_handshake :
    pos:int ->
    len:int ->
    Bytes.t ->
    ((string * string), [> error ]) result

  (** Decode connected frames.  It decodes as many frames as possible and
      returns them in order along with the last position it tried to read.  Any
      unconsumed bytes are due to an incomplete frame, and more bytes are
      required.  If any frames in the input are malformed, then [`Malformed] is
      returned. *)
  val decode_connected :
    pos:int ->
    len:int ->
    Bytes.t ->
    ((Frame.t list * int), [> error ]) result
end

module Writer : sig
  (** The length of a handshake header.  This is not the whole handshake, just
      the version and reserved bytes. *)
  val handshake_header_length : int

  (** Put the handshake header into the byte buffer.  The buffer must have
      [handshake_header_length] bytes left in it. *)
  val handshake_header : Bytes.t -> pos:int -> unit

  (** Length of an info_hash *)
  val info_hash_length : int

  (** Copy an info hash, the destination buffer must have [info_hash_length]
      bytes free.  *)
  val info_hash_header : Bytes.t -> pos:int -> string -> unit

  (** Length of a peer_id *)
  val peer_id_length : int

  (** Copy a peer id, the destination buffer must have [peer_id_length] bytes
      free.  *)
  val peer_id_header : Bytes.t -> pos:int -> string -> unit

  (** Encode a frame into a buffer. *)
  val encode : Buffer.t -> Frame.t -> unit
end

