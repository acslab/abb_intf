module Kr = struct
  type t = { id : string
           ; desc : string
           ; score : float option
           } [@@deriving yojson]
end

module Okr = struct
  type t = { id : string
           ; desc : string
           ; time_type : string
           ; krs : Kr.t list
           } [@@deriving yojson]
end

module Okrs = struct
  type t = Okr.t list [@@deriving yojson]
end

module Org = struct
  type t = { id : string
           ; name : string
           } [@@deriving yojson]
end

module Orgs = struct
  type t = Org.t list [@@deriving yojson]
end
