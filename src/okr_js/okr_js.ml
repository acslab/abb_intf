let min_score = 0.6

let alert msg = Js.Unsafe.(fun_call (js_expr "alert") [| inject (Js.string msg) |])

let format_score = function
  | Snabela.Kv.F f -> Snabela.Kv.S (Printf.sprintf "%0.2f" f)
  | any -> any

let okr_list_tmpl =
  "okr_list.html"
  |> Okr_js_files.read
  |> CCOpt.get_exn
  |> Snabela.Template.of_utf8_string
  |> CCResult.get_exn
  |> CCFun.flip Snabela.of_template [("score", format_score)]

let add_objective_tmpl =
  "add_okr_dialog.html"
  |> Okr_js_files.read
  |> CCOpt.get_exn
  |> Snabela.Template.of_utf8_string
  |> CCResult.get_exn
  |> CCFun.flip Snabela.of_template []

let add_kr_tmpl =
  "add_kr_dialog.html"
  |> Okr_js_files.read
  |> CCOpt.get_exn
  |> Snabela.Template.of_utf8_string
  |> CCResult.get_exn
  |> CCFun.flip Snabela.of_template []


let set_quarterly_okrs quarterly_elm all_okrs =
  let quarterly_okrs =
    List.filter
      (fun okr -> okr.Okr_data.Okr.time_type = "quarterly")
      all_okrs
  in
  let okrs =
    List.map
      (fun okr ->
         let (score_good, score_bad, has_score, score) =
           let krs = okr.Okr_data.Okr.krs in
           match CCList.all_some (List.map (fun kr -> kr.Okr_data.Kr.score) krs) with
             | None
             | Some [] ->
               (false, false, false, 0.0)
             | Some scores ->
               let avg_score = List.fold_left (+.) 0.0 scores /. float (List.length scores) in
               if avg_score < min_score then
                 (false, true, true, avg_score)
               else
                 (true, false, true, avg_score)
         in
         let objective_title = okr.Okr_data.Okr.desc in
         let krs =
           Snabela.Kv.(List.map
                         (fun kr ->
                            let score = CCOpt.get_or ~default:0.0 kr.Okr_data.Kr.score in
                            Map.of_list
                              [ ("kr_id", string kr.Okr_data.Kr.id)
                              ; ("kr_text", string kr.Okr_data.Kr.desc)
                              ; ("has_score", bool (CCOpt.is_some kr.Okr_data.Kr.score))
                              ; ("score", float score)
                              ; ("score_bad", bool (score < 0.6))
                              ; ("score_good", bool (score >= 0.6))
                              ])
                         okr.Okr_data.Okr.krs)
         in
         Snabela.Kv.(Map.of_list
                       [ ("objective_id", string okr.Okr_data.Okr.id)
                       ; ("objective_title", string objective_title)
                       ; ("krs", list krs)
                       ; ("score_good", bool score_good)
                       ; ("score_bad", bool score_bad)
                       ; ("has_score", bool has_score)
                       ; ("score", float score)
                       ]))
      quarterly_okrs
  in
  let kv = Snabela.Kv.(Map.of_list [ ("okrs", list okrs) ]) in
  match Snabela.apply okr_list_tmpl kv with
    | Ok res ->
      quarterly_elm##.innerHTML := Js.string res
    | Error _ ->
      alert "nyi"

let set_current_okrs req =
  let results =
    CCOpt.of_result
      (Okr_data.Okrs.of_yojson
         (Yojson.Safe.from_string (Js.to_string req##.responseText)))
  in
  let quarterly =
    Js.Opt.to_option
      (Dom_html.(CoerceTo.div (getElementById "okr_quarterly")))
  in
  ignore CCOpt.(set_quarterly_okrs <$> quarterly <*> results);
  Js._true

let fetch_current_okrs () =
  let req = XmlHttpRequest.create () in
  req##.onload := Dom.handler (fun _ -> set_current_okrs req);
  req##_open (Js.string "GET") (Js.string "/v0/current_okrs") Js._true;
  req##send Js.null

let remove_kr obj_id kr_id =
  let req = XmlHttpRequest.create () in
  req##.onload := Dom.handler (fun _ -> fetch_current_okrs (); Js._true);
  let url = Printf.sprintf "/v0/okr/%s/delete/%s" obj_id kr_id in
  req##_open (Js.string "POST") (Js.string url) Js._true;
  req##send Js.null

let show_add_objective_dialog timeframe orgs elm =
  let orgs =
    List.map
      (fun org -> Snabela.Kv.(Map.of_list
                                [ ("org_id", string org.Okr_data.Org.id)
                                ; ("org_name", string org.Okr_data.Org.name)
                                ; ("selected", bool false)
                                ]))
      orgs
  in
  let kv =
    Snabela.Kv.(Map.of_list [ ("timeframe", string timeframe)
                            ; ("organizations", list orgs)
                            ])
  in
  match Snabela.apply add_objective_tmpl kv with
    | Ok res -> begin
        elm##.innerHTML := Js.string res;
        Js.Unsafe.meth_call elm##.firstChild "showModal" [| |]
      end
    | Error _ ->
      alert "nyi"

let show_add_kr_dialog obj_id elm =
  let kv = Snabela.Kv.(Map.of_list [ ("objective_id", string obj_id) ]) in
  match Snabela.apply add_kr_tmpl kv with
    | Ok res -> begin
        elm##.innerHTML := Js.string res;
        Js.Unsafe.meth_call elm##.firstChild "showModal" [| |]
      end
    | Error _ ->
      alert "nyi"

let get_organizations timeframe elm =
  let req = XmlHttpRequest.create () in
  req##.onload := Dom.handler
      (fun _ ->
         let orgs =
           CCOpt.of_result
             (Okr_data.Orgs.of_yojson
                (Yojson.Safe.from_string (Js.to_string req##.responseText)))
         in
         ignore CCOpt.(show_add_objective_dialog timeframe <$> orgs <*> elm);
         Js._true);
  req##_open (Js.string "GET") (Js.string "/v0/account/organizations") Js._true;
  req##send Js.null

let add_objective timeframe =
  let dialog = Js.Opt.to_option (Dom_html.(CoerceTo.div (getElementById "dialog"))) in
  ignore (get_organizations timeframe dialog)

let add_kr obj_id =
  let dialog = Js.Opt.to_option (Dom_html.(CoerceTo.div (getElementById "dialog"))) in
  ignore CCOpt.(show_add_kr_dialog obj_id <$> dialog)

let close_dialog () =
  let dialog = Js.Opt.to_option (Dom_html.(CoerceTo.div (getElementById "dialog"))) in
  ignore CCOpt.((fun elm -> Js.Unsafe.meth_call elm##.firstChild "close" [| |]) <$> dialog)

let homepage_onload () =
  fetch_current_okrs ()

let _ =
  Js.export_all
    (object%js
      method homepageOnload = homepage_onload ()
      method removeKr obj_id kr_id = remove_kr (Js.to_string obj_id) (Js.to_string kr_id)
      method addObjective timeframe = add_objective (Js.to_string timeframe)
      method addKr obj_id = add_kr (Js.to_string obj_id)
      method closeDialog = close_dialog ()
    end)
