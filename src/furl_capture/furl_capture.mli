module Path : sig
  (** Stop at / *)
  val string : string Tyre.t

  (** -?[0-9]+ *)
  val int : int Tyre.t

  (** -?[0-9]+( .[0-9]* )? *)
  val float : float Tyre.t

  (** true|false *)
  val bool : bool Tyre.t
end

module Query : sig
  (** Stop at &;+, *)
  val string : string -> (string * string Tyre.t)

  (** -?[0-9]+ *)
  val int : string -> (string * int Tyre.t)

  (** -?[0-9]+( .[0-9]* )? *)
  val float : string -> (string * float Tyre.t)

  (** true|false *)
  val bool : string -> (string * bool Tyre.t)
end
