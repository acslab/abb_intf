module Path = struct
  let string = Tyre.regex Re.(rep1 @@ compl [char '/'])
  let float = Tyre.float
  let int = Tyre.int
  let bool = Tyre.bool
end

module Query = struct
  let string n = (n, Tyre.regex Re.(rep1 @@ compl [set "&;+,"]))
  let float n = (n, Path.float)
  let int n = (n, Path.int)
  let bool n = (n, Path.bool)
end
