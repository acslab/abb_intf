[global]
selector = ["opam", "config", "var", "os"]

[global.release]
extra_compiler_opts = "-bin-annot -strict-sequence -strict-formats -safe-string -noassert"

[global.debug]
extra_compiler_opts = "-g -bin-annot -strict-sequence -strict-formats -safe-string -w '@d@f@p@u@s@40'"

[global.profile]
extra_compiler_opts = "-safe-string"

[global.test-release]
extra_compiler_opts = "-safe-string"

[global.test-debug]
extra_compiler_opts = "-safe-string"

[global.test-profile]
extra_compiler_opts = "-safe-string"

[src.bencode_pp]
type = "exec"
install = true
deps = [ "labbittorrent" ]
install_cmd = "cp -vf abbittorrent.native $(PREFIX)/bin/bencode_pp"
remove_cmd = "rm -vf $(PREFIX)/bin/bencode_pp"
extra_compiler_opts = "-noassert"

[src.abbittorrent]
type = "exec"
install = true
deps = [ "logs", "logs.fmt", "abb_scheduler_select", "labbittorrent" ]
install_cmd = "cp -vf abbittorrent.native $(PREFIX)/bin/abbittorrent"
remove_cmd = "rm -vf $(PREFIX)/bin/abbittorrent"

[src.labbittorrent]
install = true
deps = [ "num",
         "containers",
         "sha",
         "ocplib-endian",
         "lens",
         "uri",
         "ipaddr",
         "ipaddr.unix",
         "logs",
         "abb_channel",
         "abb_future_combinators",
         "abb_service_local",
         "abb_service_serializer",
         "abb_scheduler_select" ]

[src.abb_intf]
install = true
deps = [ "ppx_deriving", "ppx_deriving.show", "ppx_deriving.eq" ]
extra_makefile_lines = ["DOC_FILES:=$(wildcard $(SRC_DIR)/*_intf.ml)"]

[src.abb]
install = true
deps = ["abb_intf", "abb_scheduler_select"]
extra_makefile_lines = ["include $(SRC_DIR)/abb_other.mk"]

[src.abb.selector.freebsd]
deps = ["abb_intf", "abb_scheduler_kqueue"]
extra_makefile_lines = ["include $(SRC_DIR)/abb_kqueue.mk"]

[src.abb_io]
install = true
deps = [ "containers", "abb_intf", "abb_future_combinators" ]

[src.abb_service_local]
install = true
deps = [ "abb_channel", "abb_channel_queue" ]

[src.abb_service_serializer]
install = true
deps = [ "abb_channel", "abb_service_local" ]

[src.abb_channel]
install = true
deps = [ "abb_intf", "abb_future_combinators" ]
extra_makefile_lines = ["DOC_FILES:=$(wildcard *_intf.ml) $(wildcard *.mli)"]

[src.abb_channel_queue]
install = true
deps = [ "abb_intf", "abb_channel", "abb_future_combinators" ]

[src.abb_fut]
install = true
deps = [ "abb_intf" ]
extra_makefile_lines = ["DOC_FILES:=$(wildcard $(SRC_DIR)/*.ml)"]

[src.abb_future_combinators]
install = true
deps = [ "abb_intf" ]

[src.abb_scheduler_select]
install = true
deps = ["unix", "mtime.clock.os", "containers", "abb_intf", "abb_fut", "abb_thread_pool" ]

[src.abb_scheduler_kqueue]
install = true
deps = ["unix", "mtime.clock.os", "containers", "kqueue", "abb_intf", "abb_fut", "abb_thread_pool" ]

[src.abb_scheduler_nyi]
install = false
deps = ["abb_intf", "abb_fut" ]

[src.abb_tcp_server]
install = true
deps = [ "abb_intf", "abb_channel", "abb_channel_queue" ]

[src.abb_thread_pool]
install = true

[src.abb_tls]
install = true
deps = [ "unix", "otls", "abb_intf", "abb_future_combinators", "abb_io" ]

[src.abb_time]
install = true
deps = [ "containers" ]

[src.abb_test]
install = true
deps = ["containers", "oth", "oth_abb", "abb_intf", "abb_future_combinators"]
extra_compiler_opts = "-noassert"
debug = { extra_compiler_opts = "-noassert" }
profile = { extra_compiler_opts = "-noassert" }

[src.cohttp_abb]
install = true
deps = [ "uri",
         "ppx_deriving",
         "ppx_deriving.show",
         "ppx_deriving.eq",
         "containers",
         "duration",
         "cohttp",
         "oth",
         "oth_abb",
         "abb_intf",
         "abb_future_combinators",
         "abb_io",
         "abb_channel",
         "abb_channel_queue",
         "abb_tls"
       ]

[src.cohttp_abb_curl]
install = false
type = "exec"
deps = [ "uri", "otls", "abb_intf", "abb_scheduler_select", "abb_io", "cohttp_abb" ]

[src.prober]
install = false
type = "exec"
deps = [ "cmdliner",
         "ISO8601",
         "uri",
         "logs",
         "logs.fmt",
         "abb_intf",
         "abb_scheduler_kqueue",
         "abb_io",
         "abb_channel",
         "abb_time",
         "cohttp_abb" ]

[src.stsops]
install = true
type = "exec"
deps = [ "ppx_deriving",
         "ppx_deriving.show",
         "ppx_deriving.eq",
         "cmdliner",
         "ISO8601",
         "toml",
         "containers",
         "logs",
         "logs.fmt",
         "process",
         "snabela" ]
install_cmd = "cp -cv stsops.native $(PREFIX)/bin/stsops"
remove_cmd = "rm -vf $(PREFIX)/bin/stsops"

[src.oth_abb]
install = true
deps = [ "containers", "duration", "oth", "abb_intf", "abb_future_combinators" ]

[src.cohttp_abb_server_hello_world]
install = false
type = "exec"
deps = [ "cmdliner",
         "uri",
         "containers",
         "furl",
         "furl_capture",
         "otls",
         "abb_intf",
         "abb_scheduler_select",
         "abb_future_combinators",
         "abb_io",
         "cohttp_abb",
         "cohttp_abb_router_handler" ]

[src.irc_server]
install = false
type = "exec"
deps = [ "abb_channel",
         "abb_channel_queue",
         "abb_future_combinators",
         "abb_intf",
         "abb_io",
         "abb_scheduler_select",
         "abb_tcp_server",
         "cmdliner",
         "containers",
         "ISO8601",
         "logs",
         "logs.fmt" ]

[src.cohttp_abb_router_handler]
install = true
deps = [ "uri", "cohttp_abb" ]

[src.furl_capture]
install = true
deps = [ "re", "tyre" ]

[src.brtl]
install = true
deps = [ "abb_intf",
         "abb_future_combinators",
         "cohttp_abb",
         "furl",
         "furl_capture",
         "hmap",
         "logs",
         "snabela",
         "uri"
       ]

[src.brtl_mw_log]
install = true
deps = [ "abb_intf", "brtl", "cohttp", "logs", "uri" ]

[src.brtl_mw_session]
install = true
deps = [ "abb_intf", "brtl" ]

[src.brtl_hello_world]
install = false
type = "exec"
deps = [ "abb_scheduler_select", "brtl", "brtl_mw_log", "containers", "logs", "logs.fmt" ]

[src.longform]
install = true
type = "exec"
install_cmd = "cp -cv longform.native $(PREFIX)/bin/longform"
remove_cmd = "rm -vf $(PREFIX)/bin/longform"
deps = [ "abb",
         "abb_future_combinators",
         "abb_service_serializer",
         "brtl",
         "brtl_mw_log",
         "cmdliner",
         "containers",
         "ISO8601",
         "logs",
         "logs.fmt",
         "longform_data",
         "longform_tmpl",
         "lua_pattern",
         "magic-mime",
         "otls",
         "sqlite3",
         "yojson" ]

[src.longform_js]
install = false
type = "exec"
deps = [ "js_of_ocaml",
         "js_of_ocaml.ppx",
         "longform_data",
         "yojson" ]
extra_makefile_lines = [ "OCAMLC_LINK_OPTS=",
                         "OCAMLOPT_LINK_OPTS=",
                         "all: ../../../longform_files/longform_js.js",
                         "../../../longform_files/longform_js.js: $(BYTE_TARGET)",
                         "\tjs_of_ocaml longform_js.byte",
                         "\tmv longform_js.js ../../../longform_files" ]

debug = { extra_makefile_lines = [ "OCAMLC_LINK_OPTS=",
                                   "OCAMLOPT_LINK_OPTS=",
                                   "all: ../../../longform_files/longform_js.js",
                                   "../../../longform_files/longform_js.js: $(BYTE_TARGET)",
                                   "\tjs_of_ocaml --pretty --source-map-inline longform_js.byte",
                                   "\tmv longform_js.js ../../../longform_files" ]}


[src.longform_data]
install = true
deps = [ "ppx_deriving_yojson", "yojson" ]

[src.longform_tmpl]
install = false
compile_deps = [ "longform_js" ]
extra_makefile_lines = ["include $(SRC_DIR)/longform_tmpl.mk"]

[src.okr]
install = true
type = "exec"
install_cmd = "cp -cv longform.native $(PREFIX)/bin/okr"
remove_cmd = "rm -vf $(PREFIX)/bin/okr"
deps = [ "abb",
         "abb_future_combinators",
         "abb_service_serializer",
         "brtl",
         "brtl_mw_log",
         "brtl_mw_session",
         "ISO8601",
         "cmdliner",
         "containers",
         "logs",
         "logs.fmt",
         "magic-mime",
         "okr_data",
         "okr_files",
         "scrypt",
         "sqlite3",
         "uuidm",
         "yojson" ]

[src.okr_data]
install = true
deps = [ "ppx_deriving_yojson", "yojson" ]

[src.okr_js]
install = false
type = "exec"
deps = [ "containers",
         "js_of_ocaml",
         "js_of_ocaml.ppx",
         "okr_data",
         "okr_js_files",
         "snabela",
         "yojson" ]
extra_makefile_lines = [ "OCAMLC_LINK_OPTS=",
                         "OCAMLOPT_LINK_OPTS=",
                         "all: ../../../okr_files/okr_js.js",
                         "../../../okr_files/okr_js.js: $(BYTE_TARGET)",
                         "\tjs_of_ocaml okr_js.byte",
                         "\tmv okr_js.js ../../../okr_files" ]

debug = { extra_makefile_lines = [ "OCAMLC_LINK_OPTS=",
                                   "OCAMLOPT_LINK_OPTS=",
                                   "all: ../../../okr_files/okr_js.js",
                                   "../../../okr_files/okr_js.js: $(BYTE_TARGET)",
                                   "\tjs_of_ocaml --pretty --source-map-inline okr_js.byte",
                                   "\tmv okr_js.js ../../../okr_files" ] }


[src.okr_files]
install = false
compile_deps = [ "okr_js" ]
extra_makefile_lines = ["include $(SRC_DIR)/okr_files.mk"]

[src.okr_js_files]
install = false
extra_makefile_lines = ["include $(SRC_DIR)/okr_js_files.mk"]

[tests.abb_fut]
deps = ["oth", "abb_intf", "abb_fut"]

[tests.abb_scheduler_select]
deps = ["abb_intf", "abb_fut", "abb_test", "abb_scheduler_select"]

[tests.abb_scheduler_kqueue]
deps = ["abb_intf", "abb_fut", "abb_test", "abb_scheduler_kqueue"]

[tests.abb_channel_queue_fut]
deps = ["oth", "abb_fut", "abb_channel", "abb_channel_queue"]

[tests.abb_service_local]
deps = ["oth", "abb_fut", "abb_channel", "abb_service_local"]

[tests.abb_future_combinators]
deps = [ "oth", "abb_intf", "abb_fut", "abb_future_combinators" ]

[tests.labbittorrent]
deps = [ "qcheck", "oth", "labbittorrent" ]

[tests.abb_io]
deps = [ "oth_abb", "abb_scheduler_select", "abb_io" ]

[tests.cohttp_abb]
deps = [ "uri", "oth", "oth_abb", "abb_scheduler_select", "cohttp_abb" ]

[tests.abb_thread_pool]
deps = [ "unix", "oth", "abb_thread_pool" ]

[tests.abb_time]
deps = [ "unix", "mtime.clock.os", "oth", "abb_time" ]

[tests.abb_tls]
deps = [ "containers", "uri", "abb_scheduler_select", "abb_tls", "cohttp_abb" ]

[tests.furl]
deps = [ "tyre", "uri", "furl", "oth" ]
